#!/bin/bash
for filename in input_sw_folder/*; do

    xbase=${filename##*/};
    xpref=${xbase%.*}
    echo "Processing ${xbase}";

    ./lpcnet_demo "-features" ${filename} output_lpcnet_feat_folder/${xpref}.bin;

done