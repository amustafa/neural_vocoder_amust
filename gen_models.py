from audioop import bias
from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
import math
from adaptive_conv1d import *

which_norm = weight_norm
kernel_size = 9

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        #just 1 look-ahead frame
       
        self.padding_left = (kernel_size - 5) * dilation
        self.padding_right = 4 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()

        #self.cont_fc = nn.Sequential(which_norm(nn.Linear(64, in_ch, bias=False)),
        #                nn.Tanh())
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, init_state=0):

        #init_state = self.cont_fc(init_state).unsqueeze(1)
        #init_state = torch.repeat_interleave(init_state, self.padding_left, dim=1)
        #init_state = init_state.permute(0,2,1).contiguous() 

        x = F.pad(x,(self.padding_left, self.padding_right))
        #x[:,:,:self.padding_left] = init_state[:,:,:]
        conv_out = self.nl(self.conv(x))
        return conv_out

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, externel_padding= None):

        #if externel_padding is not None:
        #    x = torch.cat((externel_padding, x), dim=-1)
        #else:
        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out

class ConvComb(nn.Module):
    def __init__(self, lag=400):
        super(ConvComb, self).__init__()
        torch.manual_seed(5)
       
        self.padding = lag
        
        self.conv = which_norm(nn.Conv1d(1,1,lag,dilation=1, bias= False))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        #if externel_padding is not None:
        #    x = torch.cat((externel_padding, x), dim=-1)
        #else:
        x_padded = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x_padded)[:,:,:-1]
        return conv_out + x

class NonCausalConv(nn.Module):
    def __init__(self, in_ch=1, out_ch=1, kernel_size=9, dilation=1, groups=1, bias= False):
        super(NonCausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1)//2 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , self.padding))
        conv_out = self.conv(x)
        return conv_out

class Upsample(nn.Module):
    
    def __init__(self,ch1,ch2, rate1=0, rate2=0):
        
        super().__init__()
        torch.manual_seed(5)
        self.rate1 = rate1
        self.rate2 = rate2
        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x):
        
        x = self.nl(self.conv(F.interpolate(x, scale_factor= self.rate2 / self.rate1)))
        return x
          
class EnergyConv(nn.Module):
    
    def __init__(self,ch1,ch2):
        
        super().__init__()
        torch.manual_seed(5)

        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, energy):
        x = self.nl(self.conv(energy))
        
        return x
        
class TADE(nn.Module):
    
    def __init__(self,num_channels, k_siz, dilation_factor, learn_beta = True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.learn_beta = learn_beta
        self.mlp_shared = nn.Sequential(CausalConv(80,64,kernel_size=k_siz,dilation=dilation_factor),
                          nn.LeakyReLU(0.2))
       
        self.gamma_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
        if self.learn_beta:
            self.beta_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
     
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, cond, scale_factor):
           
        cond_interp = F.interpolate(cond,scale_factor= scale_factor)
        actv = self.mlp_shared(cond_interp)

        gamma = self.gamma_conv(actv)
        if self.learn_beta:
            beta = self.beta_conv(actv)
        else:
            beta = None
        
        return gamma, beta 
        
class Modulation(nn.Module):
    
    def __init__(self,num_channels, apply_norm= True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.apply_norm = apply_norm
        self.eps = 1e-05

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x, gamma, beta):
         
        if self.apply_norm:

            mu, var = torch.mean(x, dim=1, keepdim=True), torch.var(x, dim=1, keepdim=True)
            x_norm = (x - mu) * torch.rsqrt(var + self.eps)
            
        else:
            x_norm = x
            
        if beta is not None:
            out = (x_norm * gamma) + beta
        else:
            out = (x_norm * gamma)
        return out

class TADEResBlock(nn.Module):
    
    def __init__(self,in_channels, out_channels, k_siz=kernel_size, learn_beta = True):
        
        super().__init__()
        torch.manual_seed(5)
        self.in_channels= in_channels
        self.out_channels= out_channels
        dil = 1
        
        self.tade = TADE(in_channels,k_siz,1, learn_beta)
        
        self.mod1 = Modulation(self.out_channels)
        self.conv1_tanh = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1)
        self.conv1_gate = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1)

        self.mod2 = Modulation(self.out_channels)
        self.conv2_tanh = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2)
        self.conv2_gate = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2) 
                                    
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x, cond):
        
        temp_res = x.shape[2]
        mel_res = cond.shape[2]
        scale_factor  = temp_res/mel_res
        
        gamma, beta = self.tade(cond, scale_factor)
        
        mod_out1 = self.mod1(x,gamma,beta)
        out1 = torch.tanh(self.conv1_tanh(mod_out1)) * F.softmax(self.conv1_gate(mod_out1),dim=1)
        
        mod_out2 = self.mod2(out1,gamma,beta) 
        out2 = torch.tanh(self.conv2_tanh(mod_out2)) * F.softmax(self.conv2_gate(mod_out2),dim=1)

        out = out2 + x
        return out

class SpadeGen(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.latent_dim = 64

        self.energy_conv = EnergyConv(320,self.latent_dim)
        
        self.act0 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler0 = Upsample(self.latent_dim,self.latent_dim,100,200)
        
        self.act1 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler1 = Upsample(self.latent_dim,self.latent_dim,200,500)

        self.act2 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler2 = Upsample(self.latent_dim,self.latent_dim,500,1000)

        self.act3 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler3 = Upsample(self.latent_dim,self.latent_dim,1000,2000)

        self.act4 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler4 = Upsample(self.latent_dim,self.latent_dim,2000,4000)

        self.act5 = TADEResBlock(self.latent_dim,self.latent_dim)

        self.act6 = TADEResBlock(self.latent_dim,self.latent_dim)

        self.act7 = TADEResBlock(self.latent_dim,self.latent_dim)
   
        self.anti_alias = TADEResBlock(self.latent_dim,self.latent_dim)

        self.dec_conv = CausalConv(self.latent_dim,4,kernel_size=kernel_size,dilation=1)
        self.dec_tanh = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, energy):
         
        latent = self.energy_conv(energy)
        latent  = self.upsampler0(self.act0(latent,x)) 
        stage125_250 = self.upsampler1(self.act1(latent, x))
        stage250_500 = self.upsampler2(self.act2(stage125_250 ,x))
        stage500_1000 = self.upsampler3(self.act3(stage250_500 ,x))
        stage1000_2000 = self.upsampler4(self.act4(stage500_1000 ,x))
        stage2000_4000 = self.act5(stage1000_2000 ,x)
        stage4000_8000 = self.act6(stage2000_4000 ,x)
        stage8000_16000 = self.act7(stage4000_8000 ,x)
        ant_alias = self.anti_alias(stage8000_16000 ,x)          
        d_conv= self.dec_conv(ant_alias)
        out = self.dec_tanh(d_conv)
   
        return out
        
class Generator(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.mel_conv = CausalConv(80,80,kernel_size=kernel_size,dilation=1)
        
        self.spade_generator = SpadeGen()    

        self.pqmf = PQMF()
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
                              
    def forward(self, mel):

        energy = torch.sum(mel, 1, keepdim=True)
    
        x = self.spade_generator(self.mel_conv(mel), energy)   
        x = self.pqmf.synthesis(x)
        return x.squeeze(1)

class GeneratorLPCNet(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_conv = CausalConv(19,80,kernel_size=kernel_size,dilation=1)
        
        self.spade_generator = SpadeGen()    

        self.pqmf = PQMF()
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)

            chunk = torch.cat((torch.sin(f  * progression + phase0), torch.cos(f  * progression + phase0)), dim = -1)

            phase0 = phase0 + 160 * f

            chunks.append(chunk)

        phase_signals = torch.cat(chunks, dim=1)

        return phase_signals.reshape(batch_size, -1, 320)
                              
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        envelope= bfcc_with_corr.permute(0,2,1).contiguous() 
    
        x = self.spade_generator(self.bfcc_conv(envelope), p_embed)   
        x = self.pqmf.synthesis(x)
        return x.squeeze(1)

############################################################# RNN Helper Layers #############################################################

class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return self.nl(output)

class Snake_old(nn.Module):
    def __init__(self, feat_size, alpha=1):
        super(Snake_old, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * torch.sigmoid(self.gate(x))

        return out 
    
class Snake(nn.Module):
    def __init__(self, feat_size):
        super(Snake, self).__init__()
        
        torch.manual_seed(5)

        self.alpha_weight = nn.Linear(feat_size, 1, bias=False)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.ones_(m.weight.data)

    def forward(self, x):
        
        alpha = self.alpha_weight(x)
        
        x = x + (alpha + 1e-9).reciprocal() * torch.sin(alpha * x).pow(2)
        
        return x

class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        out = x * torch.sigmoid(self.gate(x)) 
    
        return out

class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )
        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out  
 
class CondFramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=2, apply_act=True):
        
        super(CondFramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        
        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim

        if apply_act:
        
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False)),
                                    GLU(self.fc_out_dim) #Snake(self.fc_out_dim)
                                   )

        else:
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False))
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, cond):


        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()

        
        cond_x_flat_padded_unfolded = torch.cat([cond, x_flat_padded_unfolded], dim=2)
        out = self.fc(cond_x_flat_padded_unfolded)

        return out

############################################################ FrameWise Model Definitions ############################################################

class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNet(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=9)
        
        self.feat_in_conv = CausalConv(256,320,kernel_size=9)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(320,320)
        
        self.rnn1 = ForwardGRU(320,320)
             
        self.rnn2 = ForwardGRU(320,320)
        
        self.rnn3 = ForwardGRU(320,320)
                 
        self.rnn4 = ForwardGRU(320,320)
        
        self.fc = which_norm(nn.Linear(1920, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2, apply_act=False)]
                                            )
        self.out = nn.Tanh()

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)

        final = self.out(signal)
                               
        waveform = final.reshape(final.size(0),1,-1).squeeze(1)

        return waveform

class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=3)
        
        self.feat_in_conv = CausalConv(256,256,kernel_size=3)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(256,256)
        
        self.rnn1 = ForwardGRU(256,256)
             
        self.rnn2 = ForwardGRU(256,256)
        
        self.rnn3 = ForwardGRU(256,256)
                 
        self.rnn4 = ForwardGRU(256,256)
        
        self.fc = which_norm(nn.Linear(1536, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2)]
                                            )
        self.out = nn.Sequential(CausalConv(1,1,kernel_size= 320),
                                 nn.Tanh())

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)
                 
        waveform = self.out(signal.reshape(signal.size(0),1,-1)).squeeze(1)

        return waveform

class FWGAN500(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = nn.Sequential(nn.ConvTranspose1d(19,64,kernel_size=5,stride=5,padding=0,\
                                                      bias=False),
                                                      nn.Tanh())

        self.feat_in_conv = ConvLookahead(128,256,kernel_size=5) 
        self.feat_in_nl = GLU(256)
        
        self.rnn = ForwardGRU(256,256)
        
        self.fwc1 = FramewiseConv(256, 256)
        self.fwc2 = FramewiseConv(256, 128)
        self.fwc3 = FramewiseConv(128, 128)
        self.fwc4 = FramewiseConv(128, 64)
        self.fwc5 = FramewiseConv(64, 64)
        self.fwc6 = FramewiseConv(64, 32)
        self.fwc7 = FramewiseConv(32, 32, act='tanh')
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,32)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,32)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals#.reshape(batch_size, -1, 64)


    def gain_multiply(self, x, c0):

        gain = 10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        wav_latent = self.feat_in_nl(self.feat_in_conv(feat_in).permute(0,2,1).contiguous())
        
        
        rnn_out = self.rnn(wav_latent)
        
        fwc1_out = self.fwc1(rnn_out)
        fwc2_out = self.fwc2(fwc1_out)
        fwc3_out = self.fwc3(fwc2_out)
        fwc4_out = self.fwc4(fwc3_out)
        fwc5_out = self.fwc5(fwc4_out)
        fwc6_out = self.fwc6(fwc5_out)
        fwc7_out = self.fwc7(fwc6_out)
        
        waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1).squeeze(1)

        waveform = self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform

############################################################ FrameWise With Continuation 500Hz Baseline ############################################################

class ContForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ContForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.cont_fc = nn.Sequential(which_norm(nn.Linear(64, self.hidden_size, bias=False)),
                        nn.Tanh())

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, x0):

        self.gru.flatten_parameters()

        h0 = self.cont_fc(x0).unsqueeze(0)

        output, h0 = self.gru(x, h0)
        
        return self.nl(output) 

class ContFramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True):
        
        super(ContFramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

            self.cont_fc = nn.Sequential(which_norm(nn.Linear(64, self.required_pad_left, bias=False)),
                                    nn.Tanh()
                                   )

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )
        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, x0):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        pad = self.cont_fc(x0).view(x0.size(0),1,-1)
        x_flat_padded = torch.cat((pad, x_flat), dim=-1).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out 

class FWGAN500Cont(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = nn.Sequential(nn.ConvTranspose1d(19,64,kernel_size=5,stride=5,padding=0,\
                                                      bias=False),
                                                      nn.Tanh())

        self.feat_in_conv = ConvLookahead(128,256,kernel_size=5) 
        self.feat_in_nl = GLU(256)
        
        self.rnn = ContForwardGRU(256,256)
        
        self.fwc1 = ContFramewiseConv(256, 256)
        self.fwc2 = ContFramewiseConv(256, 128)
        self.fwc3 = ContFramewiseConv(128, 128)
        self.fwc4 = ContFramewiseConv(128, 64)
        self.fwc5 = ContFramewiseConv(64, 64)
        self.fwc6 = ContFramewiseConv(64, 32)
        self.fwc7 = ContFramewiseConv(32, 32, act='tanh')
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,32)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,32)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals#.reshape(batch_size, -1, 64)

    def create_phase_signals_old(self, periods, frame_length=160, num_harmonics=1, phase_at_frame_k=None, k=0):

        """ creates a reference phase signal from a sequence of periods
        
        Parameters:
        -----------
        periods: torch.Tensor
            sequence of periods of shape (batch_size, num_frames)
            
        frame_length: int
            frame length in samples
            
        num_harmonics: int
            number of harmonics to produce (fundamental counted as harmonic)
            
        phase_at_frame_k: torch.FloatTensor or None
            absolute desired phases at beginning of frame k, shape: (batch_size,). If None,
            absolute phases at index 0 will be 0.
            
        k: int
            index for phase_at_frame_k argument. Ignored when phase_at_frame_k is None

        Returns:
        --------
        ref_phase: torch.FloatTensor
            reference phase signal of shape (batch_size, frame_length * periods.size(1), 2 * num_harmonics).
            The real-part of harmonic k is in ref_phase[..., ]

        next_phase0: torch.FloatTensor
            absolute phases at beginning of frame periods.size(1). For smooth continuation of reference phase signal,
            use phase_at_frame_k=next_phase0 and k=0 in next call to create_phase_signal

        """
        
        if len(periods.shape) != 2:
            raise ValueError("periods must be a rank 2 tensor")
        
        batch_size = periods.size(0)
        progression = torch.arange(1, frame_length + 1, dtype=torch.float32, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)
        

        
        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for frame in range(periods.size(1)):
            f = (2 * torch.pi / periods[:, frame]).unsqueeze(-1)
            
            chunk =f * progression + phase0
            
            phase0 = 2 * torch.pi * torch.frac((phase0 + frame_length * f) / (2 * torch.pi))
            
            chunks.append(chunk)
        
        phase_angles = torch.cat(chunks, dim=1)
        next_phase0 = phase0
        
        if phase_at_frame_k is not None:
            # apply phase shift to match absolute phase at frame k
            phase_shift = phase_at_frame_k - phase_angles[:, k * frame_length]
            phase_angles = phase_angles + phase_shift.unsqueeze(-1)
            next_phase0 = next_phase0 + phase_shift
        
        ref_phase = torch.cat(
            [torch.cos((k + 1) * phase_angles).unsqueeze(-1) for k in range(num_harmonics)] +
            [torch.sin((k + 1) * phase_angles).unsqueeze(-1) for k in range(num_harmonics)],
            dim=-1).permute(0,2,1).contiguous()

        l = torch.split(ref_phase, 1, 1)
        ll = [t.reshape(t.size(0), -1, 32) for t in l]
        pitch_embed = torch.cat(ll, dim=-1)
        return pitch_embed
        
        #return ref_phase.permute(0,2,1).contiguous()#, next_phase0

    def gain_multiply(self, x, c0):

        gain = 10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain
       
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()

        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        noise = (0.003)*torch.randn_like(feat_in)#((2/256) * torch.rand_like(feat_in)) - (1/256)
        
        feat_in = feat_in + noise
        
        wav_latent = self.feat_in_nl(self.feat_in_conv(feat_in).permute(0,2,1).contiguous())
        
        noise = (0.003)*torch.randn_like(wav_latent) #((2/256) * torch.rand_like(wav_latent)) - (1/256)
        
        wav_latent = wav_latent + noise
        
        rnn_out = self.rnn(wav_latent, x0)
        
        noise = (0.003)*torch.randn_like(rnn_out)#((2/256) * torch.rand_like(rnn_out)) - (1/256)
        
        rnn_out = rnn_out + noise
        
        fwc1_out = self.fwc1(rnn_out, x0)
        noise = (0.003)*torch.randn_like(fwc1_out) #((2/256) * torch.rand_like(fwc1_out)) - (1/256)
        
        fwc1_out = fwc1_out + noise
        
        fwc2_out = self.fwc2(fwc1_out, x0)
        noise = (0.003)*torch.randn_like(fwc2_out) #((2/256) * torch.rand_like(fwc2_out)) - (1/256)
        
        fwc2_out = fwc2_out + noise
        fwc3_out = self.fwc3(fwc2_out, x0)
        noise = (0.003)*torch.randn_like(fwc3_out) #((2/256) * torch.rand_like(fwc3_out)) - (1/256)
        
        fwc3_out = fwc3_out + noise
        fwc4_out = self.fwc4(fwc3_out, x0)
        noise = (0.003)*torch.randn_like(fwc4_out) #((2/256) * torch.rand_like(fwc4_out)) - (1/256)
        
        fwc4_out = fwc4_out + noise
        
        fwc5_out = self.fwc5(fwc4_out, x0)
        #noise = (0.003)*torch.randn_like(fwc5_out)#((2/256) * torch.rand_like(fwc5_out)) - (1/256)
        
        #fwc5_out = fwc5_out + noise
        
        fwc6_out = self.fwc6(fwc5_out, x0)
        #noise = (0.003)*torch.randn_like(fwc6_out) #((2/256) * torch.rand_like(fwc6_out)) - (1/256)
        
        #fwc6_out = fwc6_out + noise
        
        fwc7_out = self.fwc7(fwc6_out, x0)
        
        waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1).squeeze(1)

        waveform = self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform


########################################## Adaptive Convolutional Vocoder ######################################################

class AdaptiveConv1d(nn.Module):
    COUNTER = 1
    
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 feature_dim,
                 frame_size=160,
                 overlap_size=40,
                 use_bias=False,
                 use_gain=False,
                 padding=None,
                 name=None):
        """
        
        Parameters:
        -----------
        
        in_channels : int
            number of input channels
        
        out_channels : int
            number of output channels
        
        feature_dim : int
            dimension of features from which kernels, biases and gains are computed
        
        frame_size : int
            frame size
    
        overlap_size : int
            overlap size for filter cross-fade. Cross-fade is done on the first overlap_size samples of every frame

        use_bias : bool
            if true, biases will be added to output channels

        use_gain : bool
            if true, a learned gain is applied to 

        padding : List[int, int]
        
        """
        
        super(AdaptiveConv1d, self).__init__()
        
        self.in_channels   = in_channels
        self.out_channels  = out_channels
        self.feature_dim   = feature_dim
        self.kernel_size   = kernel_size
        self.frame_size    = frame_size
        self.overlap_size  = overlap_size
        self.use_bias      = use_bias
        self.use_gain      = use_gain
        
        if name is None:
            self.name = "adaptive_conv1d_" + str(AdaptiveConv1d.COUNTER)
            AdaptiveConv1d.COUNTER += 1
        else:
            self.name = name
        
        # network for generating convolution weights
        self.conv_kernel = which_norm(nn.Linear(feature_dim, in_channels * out_channels * kernel_size))
        
        if self.use_bias:
            self.conv_bias = which_norm(nn.Linear(feature_dim, out_channels))

        if self.use_gain:
            self.filter_gain = which_norm(nn.Linear(feature_dim, out_channels))
            
        if type(padding) == type(None):
            self.padding = [kernel_size // 2, kernel_size - 1 - kernel_size // 2]
        else:
            self.padding = padding
            
        
        self.overlap_win = nn.Parameter(.5 + .5 * torch.cos((torch.arange(self.overlap_size) + 0.5) * torch.pi / overlap_size), requires_grad=False)
        
    
    def flop_count(self, rate):
        frame_rate = rate / self.frame_size
        overlap = self.overlap_size
        overhead = overlap / self.frame_size
        
        count = 0
        
        # kernel computation and filtering
        count += 2 * (frame_rate * self.feature_dim * self.kernel_size)
        count += 2 * (self.in_channels * self.out_channels * self.kernel_size * (1 + overhead) * rate)
        
        # bias computation
        if self.use_bias:
            count += 2 * (frame_rate * self.feature_dim) + rate * (1 + overhead)
            
        # gain computation
        if self.use_gain:
            count += 2 * (frame_rate * self.feature_dim * self.out_channels) + rate * (1 + overhead) * self.out_channels
            
        # windowing
        count += 3 * overlap * frame_rate * self.out_channels
        
        return count
    
    def forward(self, x, features, conv_gains=None, debug=False):
        """ adaptive 1d convolution
        
        
        Parameters:
        -----------
        x : torch.tensor
            input signal of shape (batch_size, in_channels, num_samples)
            
        feathres : torch.tensor
            frame-wise features of shape (batch_size, num_frames, feature_dim)
        
        """
        
        batch_size = x.size(0)
        num_frames = features.size(1)
        num_samples = x.size(2)
        frame_size = self.frame_size
        overlap_size = self.overlap_size
        kernel_size = self.kernel_size
        win1 = torch.flip(self.overlap_win, [0])
        win2 = self.overlap_win
        
        if num_samples // self.frame_size != num_frames:
            raise ValueError('non matching sizes in AdaptiveConv1d.forward')
        
        conv_kernels = self.conv_kernel(features).reshape((batch_size, num_frames, self.out_channels, self.in_channels, self.kernel_size))
        
        if self.use_gain and conv_gains is None:
            # normalize kernels
            conv_kernels = conv_kernels / torch.sqrt(1e-6 + torch.sum(conv_kernels ** 2, dim=-1, keepdim=True))
        
        if self.use_bias:
            conv_biases  = self.conv_bias(features).permute(0, 2, 1)
        
        if self.use_gain:
            conv_gains   = torch.exp(self.filter_gain(features).permute(0, 2, 1))
            if debug and batch_size == 1:
                key = self.name + "_gains"
                write_data(key, conv_gains.detach().squeeze().cpu().numpy(), 16000 // self.frame_size)

        # frame-wise convolution with overlap-add
        output_frames = []
        overlap_mem = torch.zeros((batch_size, self.out_channels, self.overlap_size), device=x.device)
        x = F.pad(x, self.padding)
        x = F.pad(x, [0, self.overlap_size])
        
        for i in range(num_frames):
            xx = x[:, :, i * frame_size : (i + 1) * frame_size + kernel_size - 1 + overlap_size].reshape((1, batch_size * self.in_channels, -1))
            new_chunk = torch.conv1d(xx, conv_kernels[:, i, ...].reshape((batch_size * self.out_channels, self.in_channels, self.kernel_size)), groups=batch_size).reshape(batch_size, self.out_channels, -1)
            
            if self.use_bias:
                new_chunk = new_chunk + conv_biases[:, :, i : i + 1]
                
            if self.use_gain:
                new_chunk = new_chunk * conv_gains[:, :, i : i + 1]
            
            # overlapping part
            output_frames.append(new_chunk[:, :, : overlap_size] * win1 + overlap_mem * win2)
            
            # non-overlapping part
            output_frames.append(new_chunk[:, :, overlap_size : frame_size])
            
            # mem for next frame
            overlap_mem = new_chunk[:, :, frame_size :]
        
        # concatenate chunks
        output = torch.cat(output_frames, dim=-1)

        return output
    
class AdaConvFWGAN(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = nn.Sequential(nn.ConvTranspose1d(19,1,kernel_size=160,stride=160,padding=0,\
                                                      bias=False))
        
        self.initial_signal_act = nn.Sequential(CausalConv(3,1,kernel_size=9), 
                                                nn.LeakyReLU(0.2))

        self.feat_in_conv = ConvLookahead(339,256,kernel_size=5) 
        self.feat_in_nl = GLU(256)
        
        self.rnn = ForwardGRU(256,256)
        
        self.fwc1 = FramewiseConv(256, 256)
        self.adaconv1 = AdaptiveConv1d(2,2,16,256)
        self.nl1 = nn.LeakyReLU(0.2)
        
        self.fwc2 = FramewiseConv(256, 256)
        self.adaconv2 = AdaptiveConv1d(2,2,16,256)
        self.nl2 = nn.LeakyReLU(0.2)
        
        self.fwc3 = FramewiseConv(256, 256)
        self.adaconv3 = AdaptiveConv1d(2,2,16,256)
        self.nl3 = nn.LeakyReLU(0.2)
        
        self.fwc4 = FramewiseConv(256, 256)
        self.adaconv4 = AdaptiveConv1d(2,2,16,256)
        self.nl4 = nn.LeakyReLU(0.2)
        
        self.fwc5 = FramewiseConv(256, 256)
        self.adaconv5 = AdaptiveConv1d(2,1,16,256)
        self.nl5 = nn.Tanh()

        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks160 = []
        chunks1 = []
        
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin160 = chunk_sin.reshape(chunk_sin.size(0),-1,160)
            chunk_sin1 = chunk_sin.reshape(chunk_sin.size(0),-1,1)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos160 = chunk_cos.reshape(chunk_cos.size(0),-1,160)
            chunk_cos1 = chunk_cos.reshape(chunk_cos.size(0),-1,1)
            
            chunk160 = torch.cat((chunk_sin160, chunk_cos160), dim = -1)
            chunk1 = torch.cat((chunk_sin1, chunk_cos1), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks160.append(chunk160)
            chunks1.append(chunk1)
            
        phase_signals160 = torch.cat(chunks160, dim=1).permute(0, 2, 1).contiguous()
        phase_signals1 = torch.cat(chunks1, dim=1).permute(0, 2, 1).contiguous()
        
        return phase_signals160, phase_signals1


    def gain_multiply(self, x, c0):

        gain = 10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain
       
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed160, p_embed1 = self.create_phase_signals(pitch_period)
        
        envelope_upsampled = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        sig0 = self.initial_signal_act(torch.cat((p_embed1, envelope_upsampled), dim=1))
        
        feat_in = torch.cat((p_embed160 , bfcc_with_corr.permute(0,2,1).contiguous()), dim=1)
        
        wav_latent = self.feat_in_nl(self.feat_in_conv(feat_in).permute(0,2,1).contiguous())
        
        pre_net = self.rnn(wav_latent)
        
        feat_act1 =  self.fwc1(pre_net)
        sig1 = self.adaconv1(torch.cat((sig0, torch.randn_like(sig0)), dim=1), feat_act1)
        sig1 = torch.cat((sig1[:,0,:].unsqueeze(1), self.nl1((sig1[:,1,:].unsqueeze(1)))), dim=1)
             
        feat_act2 =  self.fwc2(feat_act1)
        sig2 = self.adaconv2(sig1, feat_act2)
        sig2 = torch.cat((sig2[:,0,:].unsqueeze(1), self.nl2((sig2[:,1,:].unsqueeze(1)))), dim=1)
        
        feat_act3 =  self.fwc3(feat_act2)
        sig3 = self.adaconv3(sig2, feat_act3)
        sig3 = torch.cat((sig3[:,0,:].unsqueeze(1), self.nl3((sig3[:,1,:].unsqueeze(1)))), dim=1)
        
        feat_act4 =  self.fwc4(feat_act3)
        sig4 = self.adaconv4(sig3, feat_act4)
        sig4 = torch.cat((sig4[:,0,:].unsqueeze(1), self.nl4((sig4[:,1,:].unsqueeze(1)))), dim=1)
        
        feat_act5 =  self.fwc5(feat_act4)
        sig5 = self.nl5(self.adaconv5(sig4, feat_act5)).squeeze(1)

        waveform = self.gain_multiply(sig5,bfcc_with_corr[:,:,:1])

        return  waveform

################################################### FrameWise With deeep Continuation 400Hz Baseline ###################################################

class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = nn.Linear(in_ch, out_ch * upsample_factor, bias=False)
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        '''x = x.permute(0,2,1).contiguous()

        x = self.nl(self.fc(x))
        
        x_split = torch.split(x, self.out_ch, dim=-1)
        
        out = torch.cat([chunk.permute(0,2,1).contiguous() for chunk in x_split], dim=-1)
        
        return out'''
        
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
    
class FWGAN400ContLarge(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,4) 
        
        self.feat_in_conv1 = ConvLookahead(160,256,kernel_size=5) 
        self.feat_in_nl1 = GLU(256)
        

        self.cont_net = nn.Sequential(which_norm(nn.Linear(321, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 64, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(64, 64, bias=False)),
                                      nn.Tanh())
        
        self.rnn = ContForwardGRU(256,256)
        
        self.fwc1 = ContFramewiseConv(256, 256)
        self.fwc2 = ContFramewiseConv(256, 128)
        self.fwc3 = ContFramewiseConv(128, 128)
        self.fwc4 = ContFramewiseConv(128, 64)
        self.fwc5 = ContFramewiseConv(64, 64)
        self.fwc6 = ContFramewiseConv(64, 40)
        self.fwc7 = ContFramewiseConv(40, 40)
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals


    def gain_multiply(self, x, c0):

        gain = 10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

    def forward(self, pitch_period, bfcc_with_corr, x0):

        norm_x0 = torch.norm(x0,2, dim=-1, keepdim=True)
        x0 = x0 / torch.sqrt((1e-8) +  norm_x0**2)
        x0 = torch.cat((torch.log(norm_x0 + 1e-7), x0), dim=-1)

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        #noise = (0.003)*torch.randn_like(feat_in)#((2/256) * torch.rand_like(feat_in)) - (1/256)
        
        #feat_in = feat_in + noise
        
        wav_latent1 = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())#.\
                      #permute(0,2,1).contiguous()

        cont_latent = self.cont_net(x0)
        
        
        #noise = (0.003)*torch.randn_like(wav_latent) #((2/256) * torch.rand_like(wav_latent)) - (1/256)
        
        #wav_latent = wav_latent + noise
        
        #wav_latent2 = self.feat_in_nl2(self.feat_in_conv2(wav_latent1).permute(0,2,1).contiguous()).\
        #              permute(0,2,1).contiguous()
            
        
        #wav_latent3 = self.feat_in_nl3(self.feat_in_conv3(wav_latent2).permute(0,2,1).contiguous())
        
        rnn_out = self.rnn(wav_latent1, cont_latent)
        
        #noise = (0.003)*torch.randn_like(rnn_out)#((2/256) * torch.rand_like(rnn_out)) - (1/256)
        
        #rnn_out = rnn_out + noise
        
        fwc1_out = self.fwc1(rnn_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc1_out) #((2/256) * torch.rand_like(fwc1_out)) - (1/256)
        
        #fwc1_out = fwc1_out + noise
        
        fwc2_out = self.fwc2(fwc1_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc2_out) #((2/256) * torch.rand_like(fwc2_out)) - (1/256)
        
        #fwc2_out = fwc2_out + noise
        fwc3_out = self.fwc3(fwc2_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc3_out) #((2/256) * torch.rand_like(fwc3_out)) - (1/256)
        
        #fwc3_out = fwc3_out + noise
        fwc4_out = self.fwc4(fwc3_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc4_out) #((2/256) * torch.rand_like(fwc4_out)) - (1/256)
        
        #fwc4_out = fwc4_out + noise
        
        fwc5_out = self.fwc5(fwc4_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc5_out)#((2/256) * torch.rand_like(fwc5_out)) - (1/256)
        
        #fwc5_out = fwc5_out + noise
        
        fwc6_out = self.fwc6(fwc5_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc6_out) #((2/256) * torch.rand_like(fwc6_out)) - (1/256)
        
        #fwc6_out = fwc6_out + noise
        
        fwc7_out = self.fwc7(fwc6_out, cont_latent)
        
        waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1).squeeze(1)

        waveform = self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform

class FWGAN400ContLargeNew(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,4) 
        
        self.feat_in_conv1 = ConvLookahead(160,256,kernel_size=9) 
        self.feat_in_nl1 = GLU(256)
        
        self.cont_net = nn.Sequential(which_norm(nn.Linear(321, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 64, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(64, 64, bias=False)),
                                      nn.Tanh())
        
        self.rnn = ContForwardGRU(256,256)
        
        self.fwc1 = ContFramewiseConv(256, 256)
        self.fwc2 = ContFramewiseConv(256, 128)
        self.fwc3 = ContFramewiseConv(128, 128)
        self.fwc4 = ContFramewiseConv(128, 64)
        self.fwc5 = ContFramewiseConv(64, 64)
        self.fwc6 = ContFramewiseConv(64, 40)
        self.fwc7 = ContFramewiseConv(40, 40)
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals


    def gain_multiply(self, x, c0):

        gain = 0.3*10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

    def forward(self, pitch_period, bfcc_with_corr, x0):

        norm_x0 = torch.norm(x0,2, dim=-1, keepdim=True)
        x0 = x0 / torch.sqrt((1e-8) +  norm_x0**2)
        x0 = torch.cat((torch.log(norm_x0 + 1e-7), x0), dim=-1)

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        #noise = (0.003)*torch.randn_like(feat_in)#((2/256) * torch.rand_like(feat_in)) - (1/256)
        
        #feat_in = feat_in + noise
        
        wav_latent1 = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())#.\
                      #permute(0,2,1).contiguous()

        cont_latent = self.cont_net(x0)
        
        
        #noise = (0.003)*torch.randn_like(wav_latent) #((2/256) * torch.rand_like(wav_latent)) - (1/256)
        
        #wav_latent = wav_latent + noise
        
        #wav_latent2 = self.feat_in_nl2(self.feat_in_conv2(wav_latent1).permute(0,2,1).contiguous()).\
        #              permute(0,2,1).contiguous()
            
        
        #wav_latent3 = self.feat_in_nl3(self.feat_in_conv3(wav_latent2).permute(0,2,1).contiguous())
        
        rnn_out = self.rnn(wav_latent1, cont_latent)
        
        #noise = (0.003)*torch.randn_like(rnn_out)#((2/256) * torch.rand_like(rnn_out)) - (1/256)
        
        #rnn_out = rnn_out + noise
        
        fwc1_out = self.fwc1(rnn_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc1_out) #((2/256) * torch.rand_like(fwc1_out)) - (1/256)
        
        #fwc1_out = fwc1_out + noise
        
        fwc2_out = self.fwc2(fwc1_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc2_out) #((2/256) * torch.rand_like(fwc2_out)) - (1/256)
        
        #fwc2_out = fwc2_out + noise
        fwc3_out = self.fwc3(fwc2_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc3_out) #((2/256) * torch.rand_like(fwc3_out)) - (1/256)
        
        #fwc3_out = fwc3_out + noise
        fwc4_out = self.fwc4(fwc3_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc4_out) #((2/256) * torch.rand_like(fwc4_out)) - (1/256)
        
        #fwc4_out = fwc4_out + noise
        
        fwc5_out = self.fwc5(fwc4_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc5_out)#((2/256) * torch.rand_like(fwc5_out)) - (1/256)
        
        #fwc5_out = fwc5_out + noise
        
        fwc6_out = self.fwc6(fwc5_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc6_out) #((2/256) * torch.rand_like(fwc6_out)) - (1/256)
        
        #fwc6_out = fwc6_out + noise
        
        fwc7_out = self.fwc7(fwc6_out, cont_latent)
        
        waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1).squeeze(1)

        waveform = self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform

class FWGAN400Shaper(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,4) 
        
        self.feat_in_conv1 = ConvLookahead(120,256,kernel_size=9) 
        self.feat_in_nl1 = GLU(256)
        
        self.ref_phase_conv = nn.Sequential(CausalConv(2,16,33),
                                            nn.LeakyReLU(0.2),
                                            CausalConv(16,16,9),
                                            nn.LeakyReLU(0.2),
                                            CausalConv(16,1,9),
                                            nn.Tanh())
        
        self.cond_gain_dense = which_norm(nn.Linear(256, 1, bias=False))
        
        self.rnn = ForwardGRU(256,256)
        
        self.fwc1 = FramewiseConv(256, 256)
        self.fwc2 = FramewiseConv(256, 128)
        self.fwc3 = FramewiseConv(128, 128)
        self.fwc4 = FramewiseConv(128, 80)
        self.fwc5 = FramewiseConv(80, 80)
        self.fwc6 = FramewiseConv(80, 40)
        self.fwc7 = FramewiseConv(40, 40)

        self.framing_filter = ConvComb()

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)

        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,1)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,1)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def gain_multiply_old(self, x, c0):

        gain = 0.03*10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)
        
        return x * gain

    def gain_multiply(self, x, gain):

        gain = torch.repeat_interleave(gain, 40, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)
        
        return x * gain

    def forward(self, pitch_period, bfcc_with_corr, x0):


        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()

        voiced = self.ref_phase_conv(p_embed)
        #unvoiced = torch.randn_like(voiced)
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((voiced.reshape(voiced.size(0),-1,40).permute(0,2,1).contiguous(), envelope), dim=1)
                
        feat_latent1 = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())

        gain = torch.exp(self.cond_gain_dense(feat_latent1))

        rnn_out = self.rnn(feat_latent1)#self.rnn(wave_feat_latent)
        
        #noise = (0.003)*torch.randn_like(rnn_out)#((2/256) * torch.rand_like(rnn_out)) - (1/256)
        
        #rnn_out = rnn_out + noise
        
        fwc1_out = self.fwc1(rnn_out)
        #noise = (0.003)*torch.randn_like(fwc1_out) #((2/256) * torch.rand_like(fwc1_out)) - (1/256)
        
        #fwc1_out = fwc1_out + noise
        
        fwc2_out = self.fwc2(fwc1_out)
        #noise = (0.003)*torch.randn_like(fwc2_out) #((2/256) * torch.rand_like(fwc2_out)) - (1/256)
        
        #fwc2_out = fwc2_out + noise
        fwc3_out = self.fwc3(fwc2_out)
        #noise = (0.003)*torch.randn_like(fwc3_out) #((2/256) * torch.rand_like(fwc3_out)) - (1/256)
        
        #fwc3_out = fwc3_out + noise
        fwc4_out = self.fwc4(fwc3_out)
        #noise = (0.003)*torch.randn_like(fwc4_out) #((2/256) * torch.rand_like(fwc4_out)) - (1/256)
        
        #fwc4_out = fwc4_out + noise
        
        fwc5_out = self.fwc5(fwc4_out)

        fwc6_out = self.fwc6(fwc5_out)

        fwc7_out = self.fwc7(fwc6_out)

        waveform = torch.tanh(self.framing_filter(fwc7_out.reshape(fwc7_out.size(0),1,-1))).squeeze(1)

        waveform = self.gain_multiply(waveform,gain) #self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform.squeeze(1)

class FWGAN400ContLargeAdaConvCond(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,4) 
        
        self.feat_in_conv1 = ConvLookahead(160,256,kernel_size=5) 
        self.feat_in_nl1 = GLU(256)
        
        self.filter_pitch_embed = nn.Embedding(257, 64)
        self.filter_bfcc_corr_fc = which_norm(nn.Linear(19, 64, bias=False))
        self.filter_conv = ConvLookahead(128,128,kernel_size=5) 
        self.filter_glu = GLU(128)
        self.filter_net = nn.Sequential(which_norm(nn.Linear(128, 128, bias=False)),
                                        GLU(128),
                                        which_norm(nn.Linear(128, 64, bias=False)),
                                        GLU(64),
                                        which_norm(nn.Linear(64, 64, bias=False)),
                                        GLU(64),
                                        which_norm(nn.Linear(64, 33, bias=False)),
                                        GLU(33))

        self.cont_net = nn.Sequential(which_norm(nn.Linear(321, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 160, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(160, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 80, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(80, 64, bias=False)),
                                      nn.Tanh(),
                                      which_norm(nn.Linear(64, 64, bias=False)),
                                      nn.Tanh())
        
        self.rnn = ContForwardGRU(256,256)
        
        self.fwc1 = ContFramewiseConv(256, 256)
        self.fwc2 = ContFramewiseConv(256, 128)
        self.fwc3 = ContFramewiseConv(128, 128)
        self.fwc4 = ContFramewiseConv(128, 64)
        self.fwc5 = ContFramewiseConv(64, 64)
        self.fwc6 = ContFramewiseConv(64, 40)
        self.fwc7 = ContFramewiseConv(40, 40)
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals
    
    
    def adaptive_conv_fir(self, frames, filters, upsample_factor=4):

        print(frames.shape, filters.shape)

        subframe_length = frames.size(-1)
        frame_length = frames.size(-1) * upsample_factor
        filter_length = filters.size(-1)

        flattened_frames = frames.reshape(frames.size(0),1,-1)
        flattened_frames = F.pad(flattened_frames, (filter_length-1, 0))
        flattened_frames = flattened_frames.unsqueeze(3).permute(0,1,3,2).contiguous() 


        overlapped_frames = F.unfold(flattened_frames, kernel_size=(1,filter_length + frame_length - 1),\
                                     stride=(1, frame_length)).permute(0,2,1)

        overlapped_frames_splitted = torch.split(overlapped_frames, 1, dim=0)

        chunks = torch.cat(overlapped_frames_splitted, dim=1)

        filters_splitted = torch.split(filters, 1, dim=0)

        kernel = torch.cat(filters_splitted, dim=1).permute(1,0,2)

        out = F.conv1d(chunks, kernel, groups=kernel.size(0))

        out_splitted = torch.split(out, overlapped_frames.size(1), dim=1)

        out = torch.cat(out_splitted, dim=0)

        out = out.reshape(out.size(0),-1, subframe_length)

        return out 

    def gain_multiply(self, x, c0):

        gain = 10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

    def forward(self, pitch_period, bfcc_with_corr, x0):

        norm_x0 = torch.norm(x0,2, dim=-1, keepdim=True)
        x0 = x0 / torch.sqrt((1e-8) +  norm_x0**2)
        x0 = torch.cat((torch.log(norm_x0 + 1e-7), x0), dim=-1)
        cont_latent = self.cont_net(x0)

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        #noise = (0.003)*torch.randn_like(feat_in)#((2/256) * torch.rand_like(feat_in)) - (1/256)
        
        #feat_in = feat_in + noise
        
        wav_latent1 = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())

        
        f_p_embed = self.filter_pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        f_envelope_embed = self.filter_bfcc_corr_fc(bfcc_with_corr).permute(0, 2, 1).contiguous()

        f_latent = self.filter_glu(self.filter_conv(torch.cat((f_p_embed , f_envelope_embed), dim=1))\
                                    .permute(0,2,1).contiguous())
        f_kernel = self.filter_net(f_latent)
        

        #noise = (0.003)*torch.randn_like(wav_latent) #((2/256) * torch.rand_like(wav_latent)) - (1/256)
        
        #wav_latent = wav_latent + noise
        
        #wav_latent2 = self.feat_in_nl2(self.feat_in_conv2(wav_latent1).permute(0,2,1).contiguous()).\
        #              permute(0,2,1).contiguous()
            
        
        #wav_latent3 = self.feat_in_nl3(self.feat_in_conv3(wav_latent2).permute(0,2,1).contiguous())
        
        rnn_out = self.rnn(wav_latent1, cont_latent)
        
        #noise = (0.003)*torch.randn_like(rnn_out)#((2/256) * torch.rand_like(rnn_out)) - (1/256)
        
        #rnn_out = rnn_out + noise
        
        fwc1_out = self.fwc1(rnn_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc1_out) #((2/256) * torch.rand_like(fwc1_out)) - (1/256)
        
        #fwc1_out = fwc1_out + noise
        
        fwc2_out = self.fwc2(fwc1_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc2_out) #((2/256) * torch.rand_like(fwc2_out)) - (1/256)
        
        #fwc2_out = fwc2_out + noise
        fwc3_out = self.fwc3(fwc2_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc3_out) #((2/256) * torch.rand_like(fwc3_out)) - (1/256)
        
        #fwc3_out = fwc3_out + noise
        fwc4_out = self.fwc4(fwc3_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc4_out) #((2/256) * torch.rand_like(fwc4_out)) - (1/256)
        
        #fwc4_out = fwc4_out + noise
        
        fwc5_out = self.fwc5(fwc4_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc5_out)#((2/256) * torch.rand_like(fwc5_out)) - (1/256)
        
        #fwc5_out = fwc5_out + noise
        
        fwc6_out = self.fwc6(fwc5_out, cont_latent)
        #noise = (0.003)*torch.randn_like(fwc6_out) #((2/256) * torch.rand_like(fwc6_out)) - (1/256)
        
        #fwc6_out = fwc6_out + noise
        
        fwc7_out = torch.tanh(self.adaptive_conv_fir(self.fwc7(fwc6_out, cont_latent), f_kernel))

        waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1).squeeze(1)
        
        waveform = self.gain_multiply(waveform,bfcc_with_corr[:,:,:1])

        return  waveform

class FWGAN400ContLargeNewARR(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,5) 
        
        self.feat_in_conv1 = ConvLookahead(144,256,kernel_size=9) 
        self.feat_in_nl1 = GLU(256)

        self.rnn = ForwardGRU(320,256)
        
        self.fwc1 = FramewiseConv(256 + 64, 256)
        self.fwc2 = FramewiseConv(256 + 64, 128)
        self.fwc3 = FramewiseConv(128 + 64, 128)
        self.fwc4 = FramewiseConv(128 + 64, 64)
        self.fwc5 = FramewiseConv(64 + 64, 64)
        self.fwc6 = FramewiseConv(64 + 64, 32)
        self.fwc7 = FramewiseConv(32 + 64, 32)
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,32)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,32)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def gain_multiply(self, x, c0):

        gain = 0.3*10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

    def run_first_stage(self, feat_latent, x0):

        pred_wav = torch.cat((x0, torch.zeros((x0.size(0),7680), device=x0.device)), dim=-1)
        pred_wav = pred_wav.reshape(pred_wav.size(0),-1,32)

        ar_wav = torch.cat((x0, torch.zeros((x0.size(0),7680), device=x0.device)), dim=-1)
        ar_wav = pred_wav.reshape(ar_wav.size(0),-1,32)

        latent = torch.cat((feat_latent, pred_wav, ar_wav), dim=-1) 
        rnn_out = self.rnn(latent)

        fwc1_out = self.fwc1(torch.cat((rnn_out, pred_wav, ar_wav), dim=-1))
        fwc2_out = self.fwc2(torch.cat((fwc1_out, pred_wav, ar_wav), dim=-1))
        fwc3_out = self.fwc3(torch.cat((fwc2_out, pred_wav, ar_wav), dim=-1))
        fwc4_out = self.fwc4(torch.cat((fwc3_out, pred_wav, ar_wav), dim=-1))
        fwc5_out = self.fwc5(torch.cat((fwc4_out, pred_wav, ar_wav), dim=-1))    
        fwc6_out = self.fwc6(torch.cat((fwc5_out, pred_wav, ar_wav), dim=-1))
        fwc7_out = self.fwc7(torch.cat((fwc6_out, pred_wav, ar_wav), dim=-1))

        out_waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1)

        return  out_waveform.squeeze(1)#[:,320:]

    def get_pitch_prediction(self, first_signal, pitch_period):

        pitch_prediction = first_signal[:,:320]
        start_ind = 320

        for i in range(2,pitch_period.size(-1)):

            T = pitch_period[:, i:i+1]
            ind = start_ind - T
            ind = ind + torch.arange(160, device=ind.device)
            pred_wav = torch.gather(first_signal, dim=-1, index=ind)#.unsqueeze(1) 
            pitch_prediction = torch.cat((pitch_prediction, pred_wav), dim=-1)
            start_ind = start_ind + 160

        return pitch_prediction

    def run_second_stage(self, first_signal, feat_latent, x0, pitch_period):

        first_signal[:,:320] = x0[:,:]

        ar_wav = torch.cat((torch.zeros((x0.size(0),32), device=x0.device), first_signal[:,:-32]), dim=-1)
        ar_wav = ar_wav.reshape(ar_wav.size(0),-1,32)

        pred_wav = self.get_pitch_prediction(first_signal, pitch_period)

        pred_wav = pred_wav.reshape(pred_wav.size(0),-1,32)

        latent = torch.cat((feat_latent, pred_wav, ar_wav), dim=-1) 
        rnn_out = self.rnn(latent)

        fwc1_out = self.fwc1(torch.cat((rnn_out, pred_wav, ar_wav), dim=-1))
        fwc2_out = self.fwc2(torch.cat((fwc1_out, pred_wav, ar_wav), dim=-1))
        fwc3_out = self.fwc3(torch.cat((fwc2_out, pred_wav, ar_wav), dim=-1))
        fwc4_out = self.fwc4(torch.cat((fwc3_out, pred_wav, ar_wav), dim=-1))
        fwc5_out = self.fwc5(torch.cat((fwc4_out, pred_wav, ar_wav), dim=-1))    
        fwc6_out = self.fwc6(torch.cat((fwc5_out, pred_wav, ar_wav), dim=-1))
        fwc7_out = self.fwc7(torch.cat((fwc6_out, pred_wav, ar_wav), dim=-1))

        out_waveform = fwc7_out.reshape(fwc7_out.size(0),1,-1)

        return  out_waveform.squeeze(1)#[:,320:]
    
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        feat_latent = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())

        first_signal = self.run_first_stage(feat_latent, x0)
        second_signal = self.run_second_stage(first_signal, feat_latent, x0, pitch_period)

        return first_signal, second_signal #
 
        
