from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np

class AdaptiveConv1d(nn.Module):
    def __init__(self, device, in_channels, out_channels, kernel_size, feature_dim, frame_size=160, overlap_size=160,\
                 use_bias=False, use_gain=False, padding=None):
        """
        
        Parameters:
        -----------
        
        in_channels : int
            number of input channels
        
        out_channels : int
            number of output channels
        
        feature_dim : int
            dimension of features from which kernels, biases and gains are computed
        
        frame_size : int
            frame size
    
        overlap_size : int
            overlap size for filter cross-fade. Cross-fade is done on the first overlap_size samples of every frame

        use_bias : bool
            if true, biases will be added to output channels

        use_gain : bool
            if true, a learned gain is applied to 

        padding : List[int, int]
        
        """
        
        super(AdaptiveConv1d, self).__init__()
        
        self.device = device
        self.in_channels   = in_channels
        self.out_channels  = out_channels
        self.feature_dim   = feature_dim
        self.kernel_size   = kernel_size
        self.frame_size    = frame_size
        self.overlap_size  = overlap_size
        self.use_bias      = use_bias
        self.use_gain      = use_gain
        
        # network for generating convolution weights
        self.conv_kernel = nn.Linear(feature_dim, in_channels * out_channels * kernel_size)
        #with torch.no_grad():
            # initialize to low pass
        #    self.conv_kernel.bias[:] = 1 / kernel_size
        
        if self.use_bias:
            self.conv_bias = nn.Linear(feature_dim, out_channels)

        
        if self.use_gain:
            self.filter_gain = nn.Linear(feature_dim, out_channels)
            
        if type(padding) == type(None):
            self.padding = [kernel_size // 2, kernel_size - 1 - kernel_size // 2]
        else:
            self.padding = padding
            
        
        self.overlap_win = .5 + .5 * torch.cos((torch.arange(self.overlap_size) + 0.5) * torch.pi / overlap_size)
        #self.overlap_win = self.overlap_win.to(device)
        
    def forward(self, x, features):
        """ adaptive 1d convolution
        
        
        Parameters:
        -----------
        x : torch.tensor
            input signal of shape (batch_size, in_channels, num_samples)
            
        feathres : torch.tensor
            frame-wise features of shape (batch_size, num_frames, feature_dim)
        
        """

        batch_size = x.size(0)
        num_frames = features.size(1)
        num_samples = x.size(2)
        frame_size = self.frame_size
        overlap_size = self.overlap_size
        kernel_size = self.kernel_size
        win1 = torch.flip(self.overlap_win, [0]).to(self.device)
        win2 = self.overlap_win.to(self.device)
        
        if num_samples // self.frame_size != num_frames:
            raise ValueError('non matching sizes in AdaptiveConv1d.forward')
            
        #print(self.conv_kernel(features).shape)
        
        conv_kernels = torch.tanh(self.conv_kernel(features).reshape((batch_size, num_frames,\
                                        self.out_channels, self.in_channels, self.kernel_size)))
        
        #print(conv_kernels.shape)
        if self.use_bias:
            conv_biases  = torch.tanh(self.conv_bias(features)).permute(0, 2, 1)
        
        if self.use_gain:
            conv_gains   = self.filter_gain(features).permute(0, 2, 1)

        # frame-wise convolution with overlap-add
        output_frames = []
        overlap_mem = torch.zeros((batch_size, self.out_channels, self.overlap_size)).to(self.device)
        x = F.pad(x, self.padding)
        
        for i in range(num_frames):
            new_chunk = torch.cat(
                [F.conv1d(x[b : b + 1, :, i * frame_size : (i + 1) * frame_size + kernel_size - 1 + overlap_size],\
                          conv_kernels[b, i, :, :, :]) for b in range(batch_size)],
                dim=0
            )
            
            if self.use_bias:
                new_chunk = new_chunk + conv_biases[:, :, i : i + 1]
                
            if self.use_gain:
                new_chunk = new_chunk * conv_gains[:, :, i : i + 1]
            
            # overlapping part
            output_frames.append(new_chunk[:, :, : overlap_size] * win1 + overlap_mem * win2)
            
            # non-overlapping part
            output_frames.append(new_chunk[:, :, overlap_size : frame_size])
            
            overlap_mem = new_chunk[:, :, frame_size :]
            
        output = torch.cat(output_frames, dim=-1)
        return output

class Dehummer(nn.Module):
    def __init__(self,device,  kernel_size, feature_dim, frame_size=160, overlap_size=40):
        super(Dehummer, self).__init__()


        self.kernel_size = kernel_size
        self.frame_size = frame_size
        self.feature_dim = feature_dim
        self.overlap_size = overlap_size

        self.adaptive_conv = AdaptiveConv1d(device, 1, 1, kernel_size, feature_dim, frame_size, overlap_size,\
                                            use_gain=False, use_bias=False)

    def forward(self, x, features):
        """ Dehummer forward pass
        
        Paramters:
        ----------
        x : torch.tensor of shape (batch_size, num_samples)
        features : torch.tensor of shape (batch_size, num_frames, feature_dim)
        """
        
        batch_size = x.size(0)

        x_filtered = self.adaptive_conv(x, features)#.squeeze(1)
        #x_filtered = torch.concat(
        #    (torch.zeros(batch_size, self.frame_size), x_filtered[:, self.frame_size :]),
        #    dim=1)
        
        x_dehummed = x+ x_filtered# x - x_filtered

        return x_dehummed