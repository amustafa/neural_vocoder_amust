import time
import torch
import os
import gc
from gen_models_sparsify import *
from disc_models import *
from sparsification import *
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator_lpcnet import AudioSampleGenerator
import soundfile as sf
import random
from feature_extractors import *
from stft_loss import *

def fetch_data(batch, batch_size, device):
    
    wb_speech = batch['audio'].type(torch.FloatTensor).view(batch_size,16000).to(device)
    #lpc_feat = batch['lpc_feat'].type(torch.FloatTensor).to(device)
    pw_speech = batch['pw_audio'].type(torch.FloatTensor).view(batch_size,16000).to(device)
    periods = batch['periods'].type(torch.long).view(batch_size,100).to(device)
    bfcc_with_corr = batch['bfcc_with_corr'].type(torch.FloatTensor).to(device)

    return wb_speech, pw_speech, periods, bfcc_with_corr

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True

def feature_loss(fmap_r, fmap_g, detach_first=False):
    loss = 0
    i = 0
    if detach_first:

        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                loss += torch.mean(torch.abs(rl.detach() - gl))
                i = i+1

    else:
        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                loss += torch.mean(torch.abs(rl - gl))
                i = i+1

    return loss/i


def discriminator_loss(disc_real_outputs, disc_generated_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dr, dg in zip(disc_real_outputs, disc_generated_outputs):
        r_loss =  torch.mean((1-dr)**2) #(torch.nn.ReLU()(1.0 - dr)).mean() #
        g_loss =  torch.mean(dg**2) #(torch.nn.ReLU()(1.0 + dg)).mean() #
        loss += (r_loss + g_loss)
        hing_loss += ((torch.nn.ReLU()(1.0 - dr)).mean() + (torch.nn.ReLU()(1.0 + dg)).mean()).item() #(torch.mean((1-dr)**2) + torch.mean(dg**2)).item()
        i = i+1
    return loss/i, (hing_loss/i)


def generator_loss(disc_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dg in disc_outputs:
        l = torch.mean((1-dg)**2) #-1.0 * dg.mean() #
        loss += l
        hing_loss += (-1.0 * dg.mean()).item()
        i = i+1

    return loss/i, (hing_loss/i)

def get_sparsification_list_dense(model):

    to_sparsify = []
    for module in model.modules():
        if isinstance(module, CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNet):
            if module.density < 1:
                to_sparsify.append((module.fc, module.density, [4, 8], False))
            else:
                pass

        if isinstance(module, CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite):
            if module.density < 1:
                to_sparsify.append((module.fc, module.density, [4, 8], False))
            else:
                pass
        
        if isinstance(module, Snake):
            if module.density < 1:
                to_sparsify.append((module.gate, module.density, [4, 8], False))    
            else:
                pass  

        if isinstance(module, FramewiseConv):
            if module.density < 1:
                to_sparsify.append((module.fc[0], module.density, [4, 8], False))  
            else:
                pass  

        if isinstance(module, CondFramewiseConv):
            if module.density < 1:
                to_sparsify.append((module.fc[0], module.density, [4, 8], False)) 
            else:
                pass 

    return to_sparsify

def get_sparsification_list_gru(model):

    to_sparsify = []
    for module in model.modules():

        if isinstance(module, ForwardGRU):
            if module.density < 1:

                sparsify_dict = {'W_ir' : (module.density, [4, 8], False),
                                 'W_iz' : (module.density, [4, 8], False),
                                 'W_in' : (module.density, [4, 8], False),
                                 'W_hr' : (module.density, [4, 8], True),
                                 'W_hz' : (module.density, [4, 8], True),
                                 'W_hn' : (module.density, [4, 8], True)}

                to_sparsify.append((module.gru, sparsify_dict))    
            else:
                pass  

    return to_sparsify

if __name__ == "__main__":

    torch.cuda.init()
    seed_everything(5)
    out_path = 'trained_models_distill' 
    audio_data_file = '../datasets/data_gan1.s16' #'../datasets/total_audio_dataset_ms_challenge.sw' #
    feat_data_file_dont_care = '../datasets/features_gan1.f32' #'../datasets/total_features_lpcnet_with_burg_ms_challenge.f32' #
    feat_data_file_actual = '../datasets/data_gan1.s16'
    voicing_flag_file = '../datasets/data_gan1.s16'

    model_fdr = 'models'  
    opt_fdr='optimizers'
    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 16

    sample_generator = AudioSampleGenerator(audio_data_file, feat_data_file_dont_care, feat_data_file_actual, voicing_flag_file, batch_size)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0001/2
    d_learning_rate = 0.0004/2
    
    device = torch.device('cuda:1')
    generator = CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite() #CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLiteIIR() #

    generator.to(device)
    window = torch.hann_window(window_length=320).view(1,320,1).to(device)

    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])

    msd = TFDMultiResolutionDiscriminator().to(device)
    mpd = TFDRealImagMultiResolutionDiscriminator().to(device)#MultiPeriodDiscriminator().to(device)

    msd_optimizer = optim.AdamW(msd.parameters(), d_learning_rate, betas=[0.8, 0.99])
    mpd_optimizer = optim.AdamW(mpd.parameters(), d_learning_rate, betas=[0.8, 0.99])
    
    saved_model_path= 'trained_models_distill/20221021_0643/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models_distill/20221021_0643/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    '''saved_model_path= 'trained_models_distill/20221021_0643/models/mpd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    mpd.load_state_dict(saved_model_generator)
 
    saved_opt_path= 'trained_models_distill/20221021_0643/optimizers/mpd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    mpd_optimizer.load_state_dict(saved_model_opt)
    for param_group in mpd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay'''

    saved_model_path= 'trained_models_distill/20221021_0643/models/msd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    msd.load_state_dict(saved_model_generator)
 
    saved_opt_path= 'trained_models_distill/20221021_0643/optimizers/msd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    msd_optimizer.load_state_dict(saved_model_opt)
    for param_group in msd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    #generator = nn.DataParallel(generator)'''

    spect_loss =  MultiResolutionSTFTLoss(device).to(device)

    start = 0
    stop = 0#1000
    interval = 25
    
    sparsification_list_dense = get_sparsification_list_dense(generator)
    print('Setting up dense layers sparsification ...\n')
    for item in sparsification_list_dense:
        print(item)
    sparsifyer_dense = DenseSparsifier(sparsification_list_dense, start, stop, interval)

    sparsification_list_gru = get_sparsification_list_gru(generator)
    print('Setting up gru layers sparsification ...\n')
    for item in sparsification_list_gru:
        print(item)
    sparsifyer_gru = GRUSparsifier(sparsification_list_gru, start, stop, interval)
    print('===============================================================================\n')

    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')
    
    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):

            gc.collect()
            torch.cuda.empty_cache()
            
            wb_speech, pw_speech, periods, bfcc_with_corr = fetch_data(sample_batch, batch_size, device)
            
            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            mpd.zero_grad()
            mpd_optimizer.zero_grad()

            #generate fake speech

            speech_generated = generator(periods.detach(), bfcc_with_corr.detach())  

            ###################### Train D ######################

            #disc_ms_real, disc_ms_gen, _, _ = msd(pw_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            #loss_ms_disc, loss_ms_disc_hinge = discriminator_loss(disc_ms_real, disc_ms_gen)

            disc_mp_real, disc_mp_gen, _, _ = mpd(pw_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            loss_mp_disc, loss_mp_disc_hinge = discriminator_loss(disc_mp_real, disc_mp_gen)

            total_d_loss = loss_mp_disc #+(loss_mp_disc + loss_ms_disc)/2.0 #

            total_d_loss_hinge = loss_mp_disc_hinge # (loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0 #

            total_d_loss.backward()

            #msd_optimizer.step()
            mpd_optimizer.step()

            ###################### Train G ######################
            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            mpd.zero_grad()
            mpd_optimizer.zero_grad()

            speech_generated = generator(periods.detach(), bfcc_with_corr.detach())  

            ########################### adv training without distillation ###########################
            
            disc_ms_real, disc_ms_gen, fm_ms_real, fm_ms_gen = msd(pw_speech.unsqueeze(1), speech_generated.unsqueeze(1))  
            loss_ms_disc, loss_ms_disc_hinge = generator_loss(disc_ms_gen)   

            disc_mp_real, disc_mp_gen, fm_mp_real, fm_mp_gen = mpd(pw_speech.unsqueeze(1), speech_generated.unsqueeze(1))  
            loss_mp_disc, loss_mp_disc_hinge = generator_loss(disc_mp_gen)   

            g_adv_loss = (loss_mp_disc + loss_ms_disc)/2.0 #loss_ms_disc#

            g_adv_loss_hinge = (loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0 #loss_ms_disc_hinge #

            #fm_loss_ms = feature_loss(fm_ms_real, fm_ms_gen)

            #g_fm_loss = fm_loss_ms #'''

            tf_loss = spect_loss(speech_generated, pw_speech.detach()) 

            '''frame_bias_g = torch.mean(speech_generated.reshape(speech_generated.size(0),100,160), dim=(0,1))
            frame_bias_r = torch.mean(pw_speech.reshape(pw_speech.size(0),100,160), dim=(0,1)).detach()#torch.zeros_like(frame_bias_g).detach()#

            frame_bias_g_point = torch.mean(speech_generated.reshape(speech_generated.size(0),100,160), dim=(0,1,2))
            frame_bias_r_point = torch.mean(pw_speech.reshape(pw_speech.size(0),100,160), dim=(0,1,2)).detach()

            bias_loss = F.l1_loss(frame_bias_g, frame_bias_r) + F.l1_loss(frame_bias_g_point, frame_bias_r_point)
            
            corr_r_80, corr_r_160, corr_r_320 = correlogram(pw_speech.detach(), 80), correlogram(pw_speech.detach(), 160), correlogram(pw_speech.detach(), 320)
            corr_g_80, corr_g_160, corr_g_320 = correlogram(speech_generated, 80), correlogram(speech_generated, 160), correlogram(speech_generated, 320)

            correlogram_loss = (F.l1_loss(corr_g_80, corr_r_80) + F.l1_loss(corr_g_160, corr_r_160)  + F.l1_loss(corr_g_320, corr_r_320))/3.0'''

            total_loss = g_adv_loss + tf_loss #+ 2.0*g_fm_loss #

            total_loss.backward()
            
            g_optimizer.step()

            if (epoch > 0) or sparsifyer_dense.step_counter > stop:
                sparsifyer_dense.step()
                sparsifyer_gru.step()
            else:
                sparsifyer_dense.step(verbose=True)
                sparsifyer_gru.step(verbose=True)

            if (i + 1) % 1 == 0:
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, bias_loss {bias_loss.item()}\n')
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, corr_loss {correlogram_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}, bias_loss {bias_loss.item()} \
                #    , corr_loss {correlogram_loss.item()}\n')  
                #print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()} , fm_loss {g_fm_loss.item()}\n')
                print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}\n')
                
            if (epoch == 0):
                if ((sparsifyer_dense.step_counter % sparsifyer_dense.interval == 0)):

                    model_path = os.path.join(models_path, 'generator.pkl')
                    torch.save(generator.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                    torch.save(g_optimizer.state_dict(), model_opt_path) 


                    model_path = os.path.join(models_path, 'mpd.pkl')
                    torch.save(mpd.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'mpd_opt.pkl')
                    torch.save(mpd_optimizer.state_dict(), model_opt_path) 


                    model_path = os.path.join(models_path, 'msd.pkl')
                    torch.save(msd.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'msd_opt.pkl')
                    torch.save(msd_optimizer.state_dict(), model_opt_path) 

            else:
                if ((epoch % 1 == 0) and (i % 100 == 0)):

                    model_path = os.path.join(models_path, 'generator.pkl')
                    torch.save(generator.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                    torch.save(g_optimizer.state_dict(), model_opt_path) 

                    model_path = os.path.join(models_path, 'mpd.pkl')
                    torch.save(mpd.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'mpd_opt.pkl')
                    torch.save(mpd_optimizer.state_dict(), model_opt_path) 

                    model_path = os.path.join(models_path, 'msd.pkl')
                    torch.save(msd.state_dict(), model_path)

                    model_opt_path = os.path.join(optimizer_path, 'msd_opt.pkl')
                    torch.save(msd_optimizer.state_dict(), model_opt_path) 

    print('Finished Training!')

