import os
import math
import tqdm
import numpy as np
import amfm_decompy.pYAAPT as pYAAPT
import amfm_decompy.basic_tools as basic

def get_voicing_info(x, sr=16000, upsample_rate=160):

    signal = basic.SignalObj(x, sr)
    pitch = pYAAPT.yaapt(signal, **{'frame_length' : 20.0, 'tda_frame_length' : 20.0} )

    voiced_flags = pitch.vuv.astype('int')
    unvoiced_flags = np.invert(pitch.vuv).astype('int')
    #voiced_flags = np.repeat(pitch.vuv.astype('int'), upsample_rate)
    #unvoiced_flags = np.repeat(np.invert(pitch.vuv).astype('int'), upsample_rate)
    
    return voiced_flags, unvoiced_flags

file_path = file_pth = "../datasets/total_audio_dataset_ms_challenge.sw"
samples = np.memmap(file_pth, dtype=np.short, mode='r') 
sr=16000

'''v_total = np.array([])
uv_total = np.array([])

print('Start voice extraction ...')

for i in tqdm.tqdm(range(0,len(samples),9600000)):
    
    if i == 0:
    
        chunck = (np.copy(samples[i:i+9600000])/32768.0).astype('float32')
    
    else:
        chunck = (np.copy(samples[i-320:i+9600000])/32768.0).astype('float32')
   
    v,uv = get_voicing_info(chunck)
    v_total = np.append(v_total, v)
    uv_total = np.append(uv_total, uv)

np.save('../voicing.npy', v_total)
np.save('../unvoicing.npy', uv_total)
print('Finished')'''


f_voiced =  open('../voicing.txt', 'a') 
#f_unvoiced =  open('../unvoicing.txt', 'a') 

print('Start voice extraction ...')

for i in tqdm.tqdm(range(0,len(samples),9600000)):
    
    if i == 0:
    
        chunck = (np.copy(samples[i:i+9600000])/32768.0).astype('float32')
    
    else:
        chunck = (np.copy(samples[i-320:i+9600000])/32768.0).astype('float32')
   
    v,uv = get_voicing_info(chunck)
    np.savetxt(f_voiced, v, fmt="%d")
    #np.savetxt(f_unvoiced, uv, fmt="%d")

f_voiced.close()
#f_unvoiced.close()
print('Finished')