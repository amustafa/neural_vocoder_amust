from audioop import bias
from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
import math
from adaptive_conv1d import *

which_norm = weight_norm

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        #just 1 look-ahead frame
       
        self.padding_left = (kernel_size - 2) * dilation
        self.padding_right = 1 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        #self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding_left, self.padding_right))
        conv_out = self.conv(x) #self.nl(self.conv(x))
        return conv_out
    
class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0))
        conv_out = self.conv(x)
        return conv_out
    
class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = which_norm(nn.Linear(in_ch, out_ch * upsample_factor, bias=False))
        #self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.fc(x) #self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
    
class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        out = x * torch.sigmoid(self.gate(x)) 
    
        return out
    
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return self.nl(output)
    
class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    #nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )
        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out  
    
class ReNormLayer(nn.Module):
    def __init__(self, input_size, hidden_size, frame_len=160, act='tanh', res=False):
        super(ReNormLayer, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size
        self.num_channels = hidden_size//2
        self.frame_len = frame_len

        self.mean_var_layer = FramewiseConv(input_size, hidden_size)
        self.sum_fc = nn.Sequential(which_norm(nn.Linear(self.num_channels, 1, bias=False)), nn.Tanh())
        self.res = res

        
        if act=='lr':
            self.nl = nn.LeakyReLU(0.2)

        if act=='tanh': 
            self.nl = nn.Tanh()
            
        self.init_weights()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, renorm_feat, signal):
        
        renormalization_frames = self.mean_var_layer(renorm_feat).permute(0,2,1).contiguous()
        
        alpha, beta = torch.split(renormalization_frames, self.num_channels, dim=1)
        
        renormalized_signal = (torch.repeat_interleave(alpha, self.frame_len, dim=-1) * signal) + \
                               torch.repeat_interleave(beta, self.frame_len, dim=-1)
        
        if self.res:
        
            renormalized_signal = self.nl(renormalized_signal) + signal

        else:

            renormalized_signal = self.nl(renormalized_signal)
        
        renormalized_signal_embed = self.sum_fc(renormalized_signal.permute(0,2,1).contiguous())
        renormalized_signal_embed = renormalized_signal_embed.reshape(renormalized_signal_embed.size(0), -1, self.frame_len)
        
        #renormalized_signal_sum = torch.sum(torch.tanh(renormalized_signal),dim=1, keepdim=True)/self.num_channels
        
        #output_signal_flat = self.nl(renormalized_signal_sum + signal)
        #output_signal_frames = output_signal_flat.reshape(output_signal_flat.size(0), -1, self.frame_len)

        return renormalization_frames.permute(0,2,1).contiguous(), renormalized_signal, renormalized_signal_embed
    
class FWReNormGAN(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.frame_len = 160
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,1) 
        
        self.initial_signal_conv = nn.Sequential(CausalConv(4,4,9),
                                                nn.Tanh(),
                                                CausalConv(4,4,9, dilation=2),
                                                nn.Tanh(),
                                                CausalConv(4,32,9),
                                                nn.Tanh())
        
        self.sum_fc = nn.Sequential(which_norm(nn.Linear(32, 1, bias=False)), nn.Tanh())
        
        self.feat_in_conv1 = ConvLookahead(240,256,kernel_size=5) 
        self.feat_in_nl1 = GLU(256)
                
        self.rnn = ForwardGRU(256,256)
        
        self.renormalization_layers = nn.ModuleList([ReNormLayer(256,64, self.frame_len)] + \
                                                    [ReNormLayer(224,64, self.frame_len, res=True) for i in range(18)] + \
                                                    [ReNormLayer(224,64, self.frame_len, act='tanh')])
        

        self.final_signal_conv = nn.Sequential(CausalConv(32,1,9),
                                            nn.Tanh())
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)

        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,1)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,1)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def gain_multiply(self, x, c0):

        gain = 0.3*10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain

    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        corr = torch.repeat_interleave(bfcc_with_corr[:,:,-1:].permute(0, 2, 1).contiguous(), self.frame_len, dim=-1)
        noise = torch.randn((p_embed.size(0), 1, p_embed.size(-1)), device=p_embed.device)

        signal = self.initial_signal_conv(torch.cat((p_embed, corr, noise), dim=1)) 
        signal_embed = self.sum_fc(signal.permute(0,2,1).contiguous())
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())

        feat_in = torch.cat((signal_embed.reshape(signal_embed.size(0),-1,self.frame_len).permute(0,2,1).contiguous(), envelope), dim=1)
                
        feat_latent1 = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())
        
        renorm_feat = self.rnn(feat_latent1)
        
        for renorm_layer in self.renormalization_layers:
            
            out_feat, signal, signal_as_feat_frames = renorm_layer(renorm_feat, signal)
            renorm_feat = torch.cat((signal_as_feat_frames, out_feat), dim=-1)

        return  self.final_signal_conv(signal).squeeze(1)