import time
import torch
import os
import gc
from silk_models import *
#from disc_models import *
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator_silk_enhancement import SilkEnhancementSet
import random
from stft_loss_phase import *
from stft_loss_jan import *


def fetch_data(batch, batch_size, device):
    
    silk_features = batch['features'].type(torch.FloatTensor).to(device)

    pitch_periods = batch['periods'].type(torch.long).to(device)
    
    num_bits = batch['numbits'].type(torch.FloatTensor).to(device)

    target_speech = batch['target_orig'].type(torch.FloatTensor).view(batch_size,8000).to(device)

    coded_speech = batch['signals'].type(torch.FloatTensor).view(batch_size,8000).to(device)

    return silk_features, pitch_periods, num_bits, target_speech, coded_speech
    
def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True

def feature_loss(fmap_r, fmap_g, detach_first=False):
    loss = 0
    i = 0
    if detach_first:

        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                loss += torch.mean(torch.abs(rl.detach() - gl))
                i = i+1

    else:
        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                loss += torch.mean(torch.abs(rl - gl))
                i = i+1

    return loss/i

def discriminator_loss(disc_real_outputs, disc_generated_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dr, dg in zip(disc_real_outputs, disc_generated_outputs):
        r_loss =  torch.mean((1-dr)**2) #(torch.nn.ReLU()(1.0 - dr)).mean() #
        g_loss =  torch.mean(dg**2) #(torch.nn.ReLU()(1.0 + dg)).mean() #
        loss += (r_loss + g_loss)
        hing_loss += ((torch.nn.ReLU()(1.0 - dr)).mean() + (torch.nn.ReLU()(1.0 + dg)).mean()).item() #(torch.mean((1-dr)**2) + torch.mean(dg**2)).item()
        i = i+1
    return loss/i, (hing_loss/i)

def generator_loss(disc_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dg in disc_outputs:
        l = torch.mean((1-dg)**2) #-1.0 * dg.mean() #
        loss += l
        hing_loss += (-1.0 * dg.mean()).item()
        i = i+1

    return loss/i, (hing_loss/i)

def correlogram(x, frame_size=160, search_size=320):

    temp_dim = x.size(1)
    
    kernel = torch.cat([x[:,i:i+frame_size].clone().view(-1, 1, frame_size)\
                        for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
    
    kernel_splitted = torch.split(kernel, 1, dim=0)
    
    kernel = torch.cat(kernel_splitted, dim=1).permute(1,0,2)
    
    segment = torch.cat([x[:,i:i+frame_size+search_size].clone().view(-1, 1, frame_size + search_size) \
                        for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
    
    num_frames = segment.size(1)
    
    segment_splitted = torch.split(segment, 1, dim=0)
    
    segment = torch.cat(segment_splitted, dim=1)
    
    xcorr = F.conv1d(segment, kernel, groups=kernel.size(0))
    
    xcorr_splitted = torch.split(xcorr, num_frames, dim=1)
    
    xcorr = torch.cat(xcorr_splitted, dim=0)
    
    return xcorr.permute(0, 2, 1).contiguous()

def distortion_loss(y_true, y_pred, rate_lambda=None):
    """ custom distortion loss for LPCNet features """

    if y_true.size(-1) != 20:
        raise ValueError('distortion loss is designed to work with 20 features')

    ceps_error   = y_pred[..., :18] - y_true[..., :18]
    pitch_error  = 2 * (y_pred[..., 18:19] - y_true[..., 18:19]) / (2 + y_true[..., 18:19])
    corr_error   = y_pred[..., 19:] - y_true[..., 19:]
    pitch_weight = torch.relu(y_true[..., 19:] + 0.5) ** 2

    loss = torch.mean(ceps_error ** 2 + (10/18) * torch.abs(pitch_error) * pitch_weight + (1/18) * corr_error ** 2, dim=-1)

    if type(rate_lambda) != type(None):
        loss = loss / torch.sqrt(rate_lambda)

    loss = torch.mean(loss)

    return loss

# loss
w_l1 = 0
w_lm = 0
w_slm = 2
w_sc = 0
w_logmel = 0
w_wsc = 0
w_xcorr = 0
w_sxcorr = 1
w_l2 = 10

w_sum = w_l1 + w_lm + w_sc + w_logmel + w_wsc + w_slm + w_xcorr + w_sxcorr + w_l2

def xcorr_loss(y_true, y_pred):
    dims = list(range(1, len(y_true.shape)))

    loss = 1 - torch.sum(y_true * y_pred, dim=dims) / torch.sqrt(torch.sum(y_true ** 2, dim=dims) * torch.sum(y_pred ** 2, dim=dims) + 1e-9)

    return torch.mean(loss)

def td_l2_norm(y_true, y_pred):
    dims = list(range(1, len(y_true.shape)))

    loss = torch.mean((y_true - y_pred) ** 2, dim=dims) / (torch.mean(y_pred ** 2, dim=dims) ** .5 + 1e-6)

    return loss.mean()

def td_l1(y_true, y_pred, pow=0):
    dims = list(range(1, len(y_true.shape)))
    tmp = torch.mean(torch.abs(y_true - y_pred), dim=dims) / ((torch.mean(torch.abs(y_pred), dim=dims) + 1e-9) ** pow)

    return torch.mean(tmp)


def phase_loss(y_pred, y_true):

    return torch.norm(y_true - y_pred, p=1) / torch.norm(y_true, p=1)


if __name__ == "__main__":

    torch.cuda.init()
    seed_everything(5)

    print('Creating dataloader ...\n')

    out_path = 'trained_models_silk_enhancement' 
    silk_dataset_path = '../datasets/silk_codec_dataset/training/' 
    model_fdr = 'models'  
    opt_fdr='optimizers'
    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 1024

    sample_generator = SilkEnhancementSet(silk_dataset_path)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0005 #/2
    d_learning_rate = 0.0005 #/2
    
    device = torch.device('cuda:3')
    generator = SilkEnhancerAR()
    #generator.apply(_remove_weight_norm)
    generator.to(device)

    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])
    lr_decay = 1e-4
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=g_optimizer, lr_lambda=lambda x : 1 / (1 + lr_decay * x))

    seq_length_list = [seq_len for seq_len in range(32,101)]

    window = torch.cat(
            [
                torch.zeros(40),
                torch.flip(nn.Parameter(.5 + .5 * torch.cos((torch.arange(20) + 0.5) * torch.pi / 20), requires_grad=False), [0]),
                torch.ones(40 - 20),
                1 - torch.flip(nn.Parameter(.5 + .5 * torch.cos((torch.arange(20) + 0.5) * torch.pi / 20), requires_grad=False), [0]),
                torch.zeros(128 - 2 * 40 - 20)
            ]).to(device)
    
    #msd = BigTFDMultiResolutionDiscriminator().to(device) #MelGANDiscriminator().to(device) #
    #mpd = TFDRealImagMultiResolutionDiscriminator().to(device)#MultiPeriodDiscriminator().to(device)

    #msd_optimizer = optim.AdamW(msd.parameters(), d_learning_rate, betas=[0.8, 0.99])
    #mpd_optimizer = optim.AdamW(mpd.parameters(), d_learning_rate, betas=[0.8, 0.99])

    '''saved_model_path= 'trained_models_silk_enhancement/20240115_0804/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models_silk_enhancement/20240115_0804/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    saved_model_path= 'trained_models_silk_enhancement/20220626_1437/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator_t.load_state_dict(saved_model_generator)
    
    saved_opt_path= 'trained_models_silk_enhancement/20220626_1437/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_t_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models/20220407_0428/optimizers/mpd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    mpd_optimizer.load_state_dict(saved_model_opt)
    for param_group in mpd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    saved_model_path= 'trained_models_distill/20230521_2129/models/msd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    msd.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models_distill/20230521_2129/optimizers/msd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    msd_optimizer.load_state_dict(saved_model_opt)
    for param_group in msd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    #generator = nn.DataParallel(generator)'''

    stftloss = MRSTFTLoss(sc_weight=w_sc, log_mag_weight=w_lm, wsc_weight=w_wsc, smooth_log_mag_weight=w_slm, sxcorr_weight=w_sxcorr).to(device)
    logmelloss = MRLogMelLoss().to(device)
    spect_loss = MultiResolutionSTFTLoss(device).to(device)
    def criterion(x, y):

        return (w_l1 * td_l1(x, y, pow=1) +  stftloss(x, y) + w_logmel * logmelloss(x, y)
                + w_xcorr * xcorr_loss(x, y) + w_l2 * td_l2_norm(x, y)) / w_sum
   
    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')

    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):

            gc.collect()
            torch.cuda.empty_cache()

            seq_len = seq_length_list[i % len(seq_length_list)]
            
            silk_features, pitch_periods, num_bits, target_speech, coded_speech = fetch_data(sample_batch, batch_size, device)

            silk_features = silk_features[:,:seq_len,:]
            pitch_periods = pitch_periods[:,:seq_len]
            num_bits = num_bits[:,:seq_len,:]
            target_speech = target_speech[:,:seq_len*80]
            coded_speech = coded_speech[:,:seq_len*80]

            target_speech = torch.cat((coded_speech[:,:320], target_speech[:,320:]), dim=-1)

            generator.zero_grad()
            g_optimizer.zero_grad()

            '''msd.zero_grad()
            msd_optimizer.zero_grad()

            #mpd.zero_grad()
            #mpd_optimizer.zero_grad()

            #generate fake speech

            speech_generated = generator(coded_audio.detach(), coded_audio_flat.detach()) 

            ###################### Train D ######################

            disc_ms_real, disc_ms_gen, _, _ = msd(clean_audio.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            loss_ms_disc, loss_ms_disc_hinge = discriminator_loss(disc_ms_real, disc_ms_gen)

            #disc_mp_real, disc_mp_gen, _, _ = mpd(pw_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            #loss_mp_disc, loss_mp_disc_hinge = discriminator_loss(disc_mp_real, disc_mp_gen)

            total_d_loss = loss_ms_disc #(loss_mp_disc + loss_ms_disc)/2.0 #

            total_d_loss_hinge = loss_ms_disc_hinge #(loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0 #

            total_d_loss.backward()

            msd_optimizer.step()
            #mpd_optimizer.step()'''

            ###################### Train G ######################
            #generator.zero_grad()
            #g_optimizer.zero_grad()

            #msd.zero_grad()
            #msd_optimizer.zero_grad()

            speech_generated = generator(pitch_periods.detach(), silk_features.detach(), num_bits.detach(), coded_speech.detach()) 
            
            ########################### adv training without distillation ###########################
            
            '''disc_ms_real, disc_ms_gen, fm_ms_real, fm_ms_gen = msd(clean_audio.unsqueeze(1), speech_generated.unsqueeze(1))  
            loss_ms_disc, loss_ms_disc_hinge = generator_loss(disc_ms_gen)   
 
            g_adv_loss = loss_ms_disc#(loss_mp_disc + loss_ms_disc)/2.0 #

            g_adv_loss_hinge = loss_ms_disc_hinge #(loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0 #

            fm_loss_ms = feature_loss(fm_ms_real, fm_ms_gen)

            g_fm_loss = fm_loss_ms #'''
            
            tf_loss = spect_loss(speech_generated, target_speech.detach()) 
            cos_dist_loss =  torch.mean(1.0 - F.cosine_similarity(speech_generated[:,320:480], target_speech[:,320:480].detach()))
            total_loss = tf_loss + phase_loss(speech_generated, target_speech.detach())#criterion(target_speech.detach(), speech_generated)

            total_loss.backward()
            
            g_optimizer.step()
            scheduler.step()

            if (i + 1) % 1 == 0:
                print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, cos_dist {cos_dist_loss.item()}, seq_len {seq_len}\n') 
             
            if ((epoch % 1 == 0) and (i % 100 == 0)):

                model_path = os.path.join(models_path, 'generator.pkl')
                torch.save(generator.state_dict(), model_path)

                model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                torch.save(g_optimizer.state_dict(), model_opt_path) 


    print('Finished Training!')

