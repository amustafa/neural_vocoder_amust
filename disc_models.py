import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
from feature_extractors import *
from torch.cuda.amp import autocast
import torchaudio

LRELU_SLOPE = 0.2
which_norm = weight_norm

def get_padding(kernel_size, dilation=1):
    return int((kernel_size*dilation - dilation)/2)

def make_positional_embeddings(batch_size=128, posemb_depth=1, nfft=(640//2)+1, num_frames=51):
    orig = np.linspace(0, np.pi, nfft)
    ret = np.stack([2**n * orig for n in range(0, posemb_depth)])
    ret = np.cos(ret)

    ret = np.expand_dims(ret, axis=0)
    ret = np.expand_dims(ret, axis=-1)
    ret = ret + np.zeros([batch_size, 1, 1, num_frames]) # B, depth, nfft, num_frames
    return torch.from_numpy(ret).type(torch.FloatTensor).squeeze(1)

class DConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size=3, dilation=1):
        super(DConv, self).__init__()
        torch.manual_seed(5)
       
        padding = (kernel_size - 1) // 2 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch, out_ch, kernel_size, dilation=dilation, padding=padding))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        conv_out = self.conv(x)
        return conv_out

class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=True))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * torch.sigmoid(self.gate(x)) 
    
        return out
    
class DiscriminatorRealImagFC(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorRealImagFC, self).__init__()
        #norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.dim = (self.resolution[0]//2) + 1
        self.pos_embed = make_positional_embeddings()
        self.pos_embed = self.pos_embed.to(torch.device('cuda:7')) #torch.cat((self.pos_embed, self.pos_embed), dim=1).to(torch.device('cuda:7'))
        #print(self.pos_embed.shape)
        
        self.fc_layers =  nn.ModuleList([which_norm(nn.Linear(self.dim, self.dim, bias=True)),
                                        GLU(self.dim),
                                        which_norm(nn.Linear(self.dim, self.dim, bias=True)),
                                        GLU(self.dim),
                                        which_norm(nn.Linear(self.dim, self.dim, bias=True)),
                                        GLU(self.dim),
                                        which_norm(nn.Linear(self.dim, self.dim, bias=True)), 
                                        GLU(self.dim),
                                        which_norm(nn.Linear(self.dim, self.dim, bias=True)), 
                                        GLU(self.dim),
                                       ])
        
        #self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (5, 9), padding=(2, 4)))

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x) + self.pos_embed
        x = x.permute(0,2,1).contiguous()

        for l in self.fc_layers:
            x = l(x)
            if isinstance(l, GLU):
                fmap.append(x)
        #x = self.conv_post(x)
        #fmap.append(x)
        #x = torch.flatten(x, 1, -1)
        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            
            #x = torch.cat((torch.real(x), torch.imag(x)), dim=1)

            #x = x.permute(0,2,1).contiguous()

        return torchaudio.functional.amplitude_to_DB(torch.abs(x),db_multiplier=0.0, multiplier=20,amin=1e-05,top_db=80)
    
class DiscriminatorP(torch.nn.Module):
    def __init__(self, period, kernel_size=5, stride=3, use_spectral_norm=False):
        super(DiscriminatorP, self).__init__()
        self.period = period
        self.use_spectral_norm = use_spectral_norm
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, 32, (kernel_size, 1), (stride, 1), padding=(get_padding(kernel_size, 1), 0))),
            norm_f(nn.Conv2d(32, 128, (kernel_size, 1), (stride, 1), padding=(get_padding(kernel_size, 1), 0))),
            norm_f(nn.Conv2d(128, 512, (kernel_size, 1), (stride, 1), padding=(get_padding(kernel_size, 1), 0))),
            norm_f(nn.Conv2d(512, 1024, (kernel_size, 1), (stride, 1), padding=(get_padding(kernel_size, 1), 0))),
            norm_f(nn.Conv2d(1024, 1024, (kernel_size, 1), 1, padding=(get_padding(kernel_size, 1), 0))),
        ])
        self.conv_post = norm_f(nn.Conv2d(1024, 1, (3, 1), 1, padding=(1, 0)))

        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        # 1d to 2d
        b, c, t = x.shape
        if t % self.period != 0: # pad first
            n_pad = self.period - (t % self.period)
            x = F.pad(x, (0, n_pad), "reflect")
            t = t + n_pad
        x = x.view(b, c, t // self.period, self.period)

        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, LRELU_SLOPE)
            fmap.append(x)
        x = self.conv_post(x)
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

class MultiPeriodDiscriminator(torch.nn.Module):
    def __init__(self):
        super(MultiPeriodDiscriminator, self).__init__()
        self.discriminators = nn.ModuleList([
            DiscriminatorP(2),
            DiscriminatorP(3),
            DiscriminatorP(5),
            DiscriminatorP(7),
            DiscriminatorP(11),
        ])

    def forward(self, y, y_hat):
        y_d_rs = []
        y_d_gs = []
        fmap_rs = []
        fmap_gs = []
        for i, d in enumerate(self.discriminators):
            y_d_r, fmap_r = d(y)
            y_d_g, fmap_g = d(y_hat)
            y_d_rs.append(y_d_r)
            fmap_rs.append(fmap_r)
            y_d_gs.append(y_d_g)
            fmap_gs.append(fmap_g)

        return y_d_rs, y_d_gs, fmap_rs, fmap_gs

class DiscriminatorC(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorC, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 9), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.correlogram(x.squeeze(1), frame_size=self.resolution) 

        x = x.unsqueeze(1)
    
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def correlogram(self, x, frame_size=160, search_size=320):

        temp_dim = x.size(1)
        
        kernel = torch.cat([x[:,i:i+frame_size].clone().view(-1, 1, frame_size)\
                            for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
        
        kernel_splitted = torch.split(kernel, 1, dim=0)
        
        kernel = torch.cat(kernel_splitted, dim=1).permute(1,0,2)
        
        segment = torch.cat([x[:,i:i+frame_size+search_size].clone().view(-1, 1, frame_size + search_size) \
                            for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
        
        num_frames = segment.size(1)
        
        segment_splitted = torch.split(segment, 1, dim=0)
        
        segment = torch.cat(segment_splitted, dim=1)
        
        xcorr = F.conv1d(segment, kernel, groups=kernel.size(0))
        
        xcorr_splitted = torch.split(xcorr, num_frames, dim=1)
        
        xcorr = torch.cat(xcorr_splitted, dim=0)
        
        return xcorr.permute(0, 2, 1).contiguous()

class DiscriminatorR(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorR, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 9), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.spectrogram(x)
        
        x = x.unsqueeze(1)

        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           center=False, return_complex=False) #[B, F, TT, 2]
            mag = torch.sqrt(torch.norm(x, p=2, dim =-1)) #[B, F, TT]

        return torch.sqrt(mag) #torch.cat((mag, x), dim=1)#

class DiscriminatorRR(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorRR, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(3, self.num_channels, (5, 9), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(1, 2), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(1, 2), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(1, 2), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 3), padding=(2, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (5, 3), padding=(2, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.spectrogram(x)
        
        #x = x.unsqueeze(1)

        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           center=False, return_complex=False) #[B, F, TT, 2]
            mag = torch.sqrt(torch.norm(x, p=2, dim =-1)).unsqueeze(1) #[B, F, TT]

            x = x.permute(0,3,1,2).contiguous()

        return torch.cat((mag, x), dim=1)#x.permute(0,3,1,2).contiguous()#torch.sqrt(mag)

class DiscriminatorRSC(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorRSC, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(3, self.num_channels, (3, 3), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)#torch.tanh(x)#
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'hann_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            mag = torchaudio.functional.amplitude_to_DB(torch.abs(x),db_multiplier=0.0, multiplier=20,amin=1e-05,top_db=80).unsqueeze(1)

            x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3)

            x = x.permute(0,3,1,2).contiguous()

        return torch.cat((mag, x), dim=1)

class DiscriminatorMag(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorMag, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 3), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), dilation=(2,2), padding=(2, 2))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), dilation=(4,4), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (5, 9), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels*2, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*2, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*4, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels*4, self.num_channels*4, (5, 9), padding=(2, 4))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels*4, 1, (5, 9), padding=(2, 4)))'''

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def global_norm(self, x):
        
        N = x.size(1)
        
        return x / torch.sqrt((1.0/N)*(torch.sum(x**2, dim=1, keepdim=True)) + 1e-5)

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x)
        
        x = x.unsqueeze(1)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)#x * torch.sigmoid(x)#
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window,  return_complex=True) #[B, F, TT, 1]
            x = torch.abs(x)
            num_bins = x.size(1)
            
            
        return x #torchaudio.functional.amplitude_to_DB(x,db_multiplier=0.0, multiplier=20,amin=1e-05,top_db=80) #torch.real(x) #

class DiscriminatorRealImag(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorRealImag, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (3, 3), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))'''


        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (5, 9), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), stride=(2, 1), padding=(2, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 9), padding=(2, 4))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (5, 9), padding=(2, 4)))

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        #x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            
            x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3)

            x = x.permute(0,3,1,2).contiguous()

        return x

class DiscriminatorSS(torch.nn.Module):
    def __init__(self, res, use_spectral_norm=False):
        super(DiscriminatorSS, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm

        self.res = res

        self.convs = nn.ModuleList([
            norm_f(nn.Conv1d(1, 16, 15, 1, padding=7)),
            norm_f(nn.Conv1d(16, 64, 41, 4, groups=4, padding=20)),
            norm_f(nn.Conv1d(64, 256, 41, 4, groups=16, padding=20)),
            norm_f(nn.Conv1d(256, 512, 41, 4, groups=64, padding=20)),
            norm_f(nn.Conv1d(512, 512, 41, 1, padding=20)),
        ])

        self.conv_post = norm_f(nn.Conv1d(512, 1, 5, 1, padding=2))

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):

        fmap = []
        x = x.reshape(-1, 1, self.res)
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, LRELU_SLOPE)
            fmap.append(x)
        
        x = self.conv_post(x)
        fmap.append(x)

        return x, fmap

class DiscriminatorPhaseDiff(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorPhaseDiff, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (3, 3), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.spectrogram(x)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution

        x = x.squeeze(1)
        window = getattr(torch, 'hann_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.sgn(torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True)) #[B, F, TT, 1]

            x = x[:,:,:-1] * torch.conj(x)[:,:,1:]
            
            x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3)

            x = x.permute(0,3,1,2).contiguous()

        return x

class DiscriminatorRealImagLRFold(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorRealImagLRFold, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (9, 3), padding=(4, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 3), stride=(2, 1), padding=(4, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 3), stride=(2, 1), dilation=(1,2), padding=(4, 2))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 3), stride=(2, 1), dilation=(1,4), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x)

        #print(x.shape)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)

        #print(x.shape)
       
        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            
            x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3)

            x = x.permute(0,3,1,2).contiguous()

        return x
    
class DiscriminatorRealImagLRF(torch.nn.Module):
    def __init__(self, use_spectral_norm=False):
        super(DiscriminatorRealImagLRF, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm

        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (9, 3), padding=(4, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels*2, (9, 3), stride=(2, 1), padding=(4, 1))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*4, (9, 3), stride=(2, 1), dilation=(1,2), padding=(4, 2))),
            norm_f(nn.Conv2d(self.num_channels*4, self.num_channels*4, (9, 3), stride=(2, 1), dilation=(1,4), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels*4, self.num_channels*8, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels*8, 1, (3, 3), padding=(1, 1)))

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (9, 5), padding=(4, 2))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels*2, (9, 5), stride=(2, 1), padding=(4, 1))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*4, (9, 5), stride=(2, 1), dilation=(1,2), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels*4, self.num_channels*4, (9, 5), stride=(2, 1), dilation=(1,4), padding=(4, 8))),
            norm_f(nn.Conv2d(self.num_channels*4, self.num_channels*8, (9, 5), padding=(4, 2))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels*8, 1, (9, 5), padding=(4, 2)))'''

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (9, 9), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 9), stride=(1, 1), dilation=(2,2), padding=(8, 8))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 9), stride=(1, 1), dilation=(4,4), padding=(16, 16))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 9), stride=(1, 1), dilation=(8,8), padding=(32, 32))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (9, 9), padding=(4, 4))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (9, 9), padding=(4, 4)))'''

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels, (5, 5), padding=(2, 2))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 5), stride=(1, 1), dilation=(2,2), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 5), stride=(1, 1), dilation=(3,3), padding=(6, 6))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 5), stride=(1, 1), dilation=(4,4), padding=(8, 8))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (5, 5), padding=(2, 2))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (5, 5), padding=(2, 2)))'''

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x, resolution, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x, resolution)

        #print(x.shape)
        
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)

        #print(x.shape)
       
        return x, fmap

    def spectrogram(self, x, resolution):
        n_fft, hop_length, win_length = resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            
            x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3)

            x = x.permute(0,3,1,2).contiguous()

        return x
    
class DiscriminatorRealImagLRFPosEnc(torch.nn.Module):
    def __init__(self, use_spectral_norm=False):
        super(DiscriminatorRealImagLRFPosEnc, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm

        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 3), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), padding=(1, 1))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), dilation=(2,2), padding=(2, 2))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), dilation=(4,4), padding=(4, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), stride=(1, 1), dilation=(8,8), padding=(8, 8))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        '''self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(2, self.num_channels*2, (7, 5), padding=(3, 2))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*2, (7, 5), stride=(2, 1), padding=(3, 2))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*2, (7, 5), stride=(2, 1), dilation=(1,2), padding=(3, 4))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*2, (7, 5), stride=(2, 1), dilation=(1,3), padding=(3, 6))),
            norm_f(nn.Conv2d(self.num_channels*2, self.num_channels*2, (7, 5),  dilation=(1,4), padding=(3, 8))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels*2, 1, (3, 3), padding=(1, 1)))'''


        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")


    def freq_pos_embedding(self, x):
        
        N = x.size(2)
        args = torch.arange(0, N, dtype=x.dtype, device=x.device) * torch.pi * 2 / N
        cos = torch.cos(args).reshape(1, 1, -1, 1)
        sin = torch.sin(args).reshape(1, 1, -1, 1)
        zeros = torch.zeros_like(x[:, 0:1, :, :])
        
        y = torch.cat((x, zeros + sin, zeros + cos), dim=1)
        
        return y

    def forward(self, x, resolution, pitch_period, bfcc_with_corr):
        fmap = []

        x = self.spectrogram(x, resolution)

        #print(x.shape)

        #x = self.freq_pos_embedding(x)

        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)

        #print(x.shape)
       
        return x, fmap

    def spectrogram(self, x, resolution):
        n_fft, hop_length, win_length = resolution
        #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        window = getattr(torch, 'bartlett_window')(win_length).to(x.device)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           window=window, return_complex=True) #[B, F, TT, 1]
            
            x = torch.abs(x).unsqueeze(1)
            
            #x = torch.cat((torch.real(x).unsqueeze(3), torch.imag(x).unsqueeze(3)), dim=3) #torch.abs(x).unsqueeze(1) #

            #x = x.permute(0,3,1,2).contiguous()

        return x
    
class BigTFDMultiResolutionDiscriminator(torch.nn.Module):
    def __init__(self, use_spectral_norm=False):
        super(BigTFDMultiResolutionDiscriminator, self).__init__()
        
        self.resolutions = [[640, 160, 640]] #[[2560, 640, 2560], [1280, 320, 1280], [640, 160, 640] , [320, 80, 320], [160, 40, 160], [80, 20, 80]]
        
        discs = [DiscriminatorRealImagFC(self.resolutions[i]) for i in range(len(self.resolutions))] #[DiscriminatorMag(self.resolutions[i]) for i in range(len(self.resolutions))]   
        self.discriminators = nn.ModuleList(discs)

    def forward(self, y, y_hat, pitch_period=0, bfcc_with_corr=0):
        y_d_rs = []
        y_d_gs = []
        fmap_rs = []
        fmap_gs = []
        #res_idx = np.random.choice([0,1,2,3,4,5])
        for i, d in enumerate(self.discriminators):
            y_d_r, fmap_r = d(y, pitch_period, bfcc_with_corr)
            y_d_g, fmap_g = d(y_hat, pitch_period, bfcc_with_corr)
            y_d_rs.append(y_d_r)
            y_d_gs.append(y_d_g)
            fmap_rs.append(fmap_r)
            fmap_gs.append(fmap_g)

        return y_d_rs, y_d_gs, fmap_rs, fmap_gs

class DiscriminatorS(torch.nn.Module):
    def __init__(self, num_ch_in=1, use_spectral_norm=False):
        super(DiscriminatorS, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm

        self.convs = nn.ModuleList([
            norm_f(nn.Conv1d(num_ch_in, 16, 15, 1, padding=7)),
            norm_f(nn.Conv1d(16, 64, 41, 4, groups=4, padding=20)),
            norm_f(nn.Conv1d(64, 256, 41, 4, groups=16, padding=20)),
            norm_f(nn.Conv1d(256, 512, 41, 4, groups=64, padding=20)),
            norm_f(nn.Conv1d(512, 512, 41, 1, padding=20)),
        ])

        self.conv_post = norm_f(nn.Conv1d(512, 1, 5, 1, padding=2))

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, LRELU_SLOPE)
            fmap.append(x)
        
        x = self.conv_post(x)
        fmap.append(x)

        return x, fmap

class WaveUNetDisc(torch.nn.Module):
    def __init__(self, num_ch_in=1, use_spectral_norm=False):
        super(WaveUNetDisc, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        
        self.enc1 = nn.Sequential(norm_f(nn.Conv1d(1, 32, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        self.enc2 = nn.Sequential(norm_f(nn.Conv1d(32, 64, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        self.enc3 = nn.Sequential(norm_f(nn.Conv1d(64, 128, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        self.enc4 = nn.Sequential(norm_f(nn.Conv1d(128, 256, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        self.enc5 = nn.Sequential(norm_f(nn.Conv1d(256, 512, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        
        
        self.dec1 = nn.Sequential(norm_f(nn.ConvTranspose1d(512, 256, 15, 3, padding=6)), nn.LeakyReLU(0.2))
        self.dec2 = nn.Sequential(norm_f(nn.ConvTranspose1d(512, 128, 16, 3, padding=7)), nn.LeakyReLU(0.2))
        self.dec3 = nn.Sequential(norm_f(nn.ConvTranspose1d(256, 64, 16, 3, padding=7)), nn.LeakyReLU(0.2))
        self.dec4 = nn.Sequential(norm_f(nn.ConvTranspose1d(128, 32, 15, 3, padding=6)), nn.LeakyReLU(0.2))
        self.dec5 = nn.Sequential(norm_f(nn.ConvTranspose1d(64, 32, 15, 3, padding=7)), nn.LeakyReLU(0.2))
        
        self.final_layer = norm_f(nn.Conv1d(32, 1, 15, 1, padding=7))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def global_norm(self, x):
        
        N = x.size(1)
        
        return x / torch.sqrt((1.0/N)*(torch.sum(x**2, dim=1, keepdim=True)) + 1e-5)

    def forward(self, x, pitch_period, bfcc_with_corr):
        fmap = []
        
        enc1_out = self.global_norm(self.enc1(x))
        enc2_out = self.global_norm(self.enc2(enc1_out))
        enc3_out = self.global_norm(self.enc3(enc2_out))
        enc4_out = self.global_norm(self.enc4(enc3_out))
        enc5_out = self.global_norm(self.enc5(enc4_out))
                
        dec1_out = self.global_norm(self.dec1(enc5_out))
        dec2_out = self.global_norm(self.dec2(torch.cat((dec1_out,enc4_out), dim=1)))
        dec3_out = self.global_norm(self.dec3(torch.cat((dec2_out,enc3_out), dim=1)))
        dec4_out = self.global_norm(self.dec4(torch.cat((dec3_out,enc2_out), dim=1)))
        dec5_out = self.global_norm(self.dec5(torch.cat((dec4_out,enc1_out), dim=1)))

        out = self.final_layer(dec5_out)
        
        return out, dec5_out#, fmap

class MelGANDiscriminator(torch.nn.Module):
    def __init__(self):
        super(MelGANDiscriminator, self).__init__()
        self.td_discriminators = nn.ModuleList([
            DiscriminatorS(), 
            DiscriminatorS(),
            DiscriminatorS(),

        ])
        self.meanpools = nn.ModuleList([
            nn.AvgPool1d(4, stride=2, padding=1, count_include_pad=False),
            nn.AvgPool1d(4, stride=2, padding=1, count_include_pad=False),
            #nn.AvgPool1d(4, stride=2, padding=1, count_include_pad=False),
            
        ])

    def forward(self,y, y_hat, cond=None, voiced=None, unvoiced=None):
       
        #y_f = y.clone()
        #y_hat_f = y_hat.clone()

        y_d_rs = []
        y_d_gs = []
        fmap_rs = []
        fmap_gs = []

        for i, d in enumerate(self.td_discriminators):
            if i != 0:
                y = self.meanpools[i-1](y) #y.reshape(-1, 1, y.size(2)//2) #
                y_hat = self.meanpools[i-1](y_hat) #y_hat.reshape(-1, 1, y_hat.size(2)//2) #
            y_d_r, fmap_r = d(y)
            y_d_g, fmap_g = d(y_hat)
            y_d_rs.append(y_d_r)
            fmap_rs.append(fmap_r)
            y_d_gs.append(y_d_g)
            fmap_gs.append(fmap_g)

        return y_d_rs, y_d_gs, fmap_rs, fmap_gs


"""if __name__ == "__main__":

    z = torch.randn(32,20,100)
    model = ConvPredictor()
    out = model(z)
    print(out.size())"""

