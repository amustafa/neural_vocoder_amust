#!/bin/bash
for filename in original/*; do

    xbase=${filename##*/};
    xpref=${xbase%.*}
    echo "Processing ${xbase}";

    sox ${filename} -r 16000   wavs/original/${xbase};

done