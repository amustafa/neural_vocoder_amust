import os
import time
import gc
import torch
import math
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.nn.utils import weight_norm, spectral_norm
import scipy.io.wavfile
import librosa
from librosa.util import normalize
import scipy.signal as sig
import soundfile as sf
from scipy import signal as si
import argparse


which_norm = weight_norm

#################################################################### vocoder definition ####################################################################

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, density=1):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        if density < 1:
            self.conv = nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation,bias= False)
        else:
            self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation,bias= False))

        self.density = density
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out
                        
class Snake(nn.Module):
    def __init__(self, feat_size, density=1):
        super(Snake, self).__init__()
        
        torch.manual_seed(5)

        self.density = density

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * (1.0 + torch.sigmoid(self.gate(x)))

        return out 

class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1, density=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size
        self.density= density

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.snake = Snake(self.hidden_size, self.density)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return self.snake(output) #output#

class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, density=1):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.density = density
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                Snake(self.fc_out_dim, self.density)
                               )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out 

class CondFramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=2, apply_act=True, density=1):
        
        super(CondFramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.density = density
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        
        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim

        if apply_act:
        
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False)),
                                    Snake(self.fc_out_dim, self.density)
                                   )

        else:
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False))
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, cond):


        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()

        
        cond_x_flat_padded_unfolded = torch.cat([cond, x_flat_padded_unfolded], dim=2)
        out = self.fc(cond_x_flat_padded_unfolded)

        return out  

class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNet(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=9)
        #self.bfcc_with_corr_snake = nn.LeakyReLU(0.2)#Snake(128, device=device)

        self.feat_in_conv = CausalConv(256,320,kernel_size=9)
        self.feat_in_snake = nn.LeakyReLU(0.2)#Snake(320, device=device)

        self.rnn0 = ForwardGRU(320,320)
        
        self.rnn1 = ForwardGRU(320,320)
             
        self.rnn2 = ForwardGRU(320,320)
        
        self.rnn3 = ForwardGRU(320,320)
                 
        self.rnn4 = ForwardGRU(320,320)
        
        self.fc = which_norm(nn.Linear(1920, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2, apply_act=False)]
                                            )
        self.out = nn.Tanh()
        
        

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)

        final = self.out(signal)
                               
        waveform = final.reshape(final.size(0),1,-1).squeeze(1)

        return waveform      

class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=3)
        
        self.feat_in_conv = CausalConv(256,256,kernel_size=3)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(256,256, density=0.6)
        
        self.rnn1 = ForwardGRU(256,256, density=0.6)
             
        self.rnn2 = ForwardGRU(256,256, density=0.6)
        
        self.rnn3 = ForwardGRU(256,256, density=0.6)
                 
        self.rnn4 = ForwardGRU(256,256, density=0.6)
        
        self.fc = which_norm(nn.Linear(1536, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512, density=0.65)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.65),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2)]
                                            )
        self.out = nn.Sequential(CausalConv(1,1,kernel_size= 320),
                                 nn.Tanh())

        self.density = 0.65

        self.init_weights()
        #self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)
                 
        waveform = self.out(signal.reshape(signal.size(0),1,-1)).squeeze(1)

        return waveform

'''class FramewiseConvIIR(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=2):
        
        super(FramewiseConvIIR, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
        self.required_pad_right = 0 

        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim

        self.iir_conv = nn.RNN(input_size=self.fc_input_dim, hidden_size=self.fc_out_dim, num_layers=1, batch_first=True, bias=False)

        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        self.iir_conv.flatten_parameters()

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out, _ = self.iir_conv(x_flat_padded_unfolded)
        return out 
    
class Snake(nn.Module):
    def __init__(self, feat_size,alpha=1):
        super(Snake, self).__init__()
        
        torch.manual_seed(5)

        self.gate = nn.Sigmoid()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        dim = x.size(2)

        x_split = torch.split(x,dim//2,2)

        out = x_split[0] * self.gate(x_split[1]) 
        
        return out
    
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return output#self.snake(output) 
    
class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLiteIIR(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=3)
        
        self.feat_in_conv = CausalConv(256,256,kernel_size=3)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(256,256)
        
        self.rnn1 = ForwardGRU(256,256)
             
        self.rnn2 = ForwardGRU(256,256)
        
        self.rnn3 = ForwardGRU(256,256)
                 
        self.rnn4 = ForwardGRU(256,256)
        
        self.fc = which_norm(nn.Linear(1536, 160, bias=False))

        self.lookahead_conv = FramewiseConv(160, 320)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(160, 320, frame_kernel_size=2),
                                             CondFramewiseConv(160, 320, frame_kernel_size=2),
                                             CondFramewiseConv(160, 320, frame_kernel_size=2),
                                             CondFramewiseConv(160, 320, frame_kernel_size=2)]
                                            )
        self.out = FramewiseConvIIR(160,160)

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)

        signal = self.out(signal)
                 
        waveform = signal.reshape(signal.size(0),1,-1).squeeze(1)

        return waveform'''

#################################################################### signal processing layers ####################################################################

def preemphasis(x, coef= -0.85):
    
    return si.lfilter(np.array([1.0, coef]), np.array([1.0]), x).astype('float32')

def deemphasis(x, coef= -0.85):
    
    return si.lfilter(np.array([1.0]), np.array([1.0, coef]), x).astype('float32')

gamma = 0.92
weighting_vector = np.array([gamma**i for i in range(16,0,-1)])


def lpc_synthesis_one_frame(frame, filt, buffer, weighting_vector=np.ones(16)):
    
    out = np.zeros_like(frame)
    
    filt = np.flip(filt)
    
    inp = frame[:]
    
    
    for i in range(0, inp.shape[0]):
        
        s = inp[i] - np.dot(buffer*weighting_vector, filt)
        
        buffer[0] = s
        
        buffer = np.roll(buffer, -1)
        
        out[i] = s
        
    return out

def inverse_perceptual_weighting (pw_signal, filters, weighting_vector):
    
    #inverse perceptual weighting= H_preemph / W(z/gamma)
    
    pw_signal = preemphasis(pw_signal) 
    
    signal = np.zeros_like(pw_signal)
    buffer = np.zeros(16)
    num_frames = pw_signal.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = pw_signal[frame_idx*160: (frame_idx+1)*160][:]
        out_sig_frame = lpc_synthesis_one_frame(in_frame, filters[frame_idx, :], buffer, weighting_vector)
        signal[frame_idx*160: (frame_idx+1)*160] = out_sig_frame[:]
        buffer[:] = out_sig_frame[-16:]
    
    return signal

#################################################################### start generation ####################################################################
parser = argparse.ArgumentParser()
parser.add_argument('lpcnet_feat_in', type=str, help='input lpcnet feature path')  
gen= CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite() 
#device = torch.device('cuda:7')
#gen.to(device)

saved_gen= torch.load('./neural_vocoder_speech_uplift/trained_models_distill/20221003_2205/models/generator.pkl', map_location='cpu')
#saved_gen= torch.load('./neural_vocoder_speech_uplift/trained_models_distill/20221003_2205/models/generator.pkl')
gen.load_state_dict(saved_gen)
gen = gen.eval()

if __name__ == "__main__":
    args = parser.parse_args()
    sr = 16000

    sig_name = args.lpcnet_feat_in.split('.')[0]

    feat = np.memmap(args.lpcnet_feat_in, dtype='float32', mode='r') 

    num_feat_frames = len(feat) // 36
    feat = np.reshape(feat, (num_feat_frames, 36))

    bfcc = np.copy(feat[:, :18])
    corr = np.copy(feat[:, 19:20]) + 0.5  
    bfcc_with_corr =  torch.from_numpy(np.hstack((bfcc, corr))).type(torch.FloatTensor).unsqueeze(0)#.to(device)

    period = torch.from_numpy((0.1 + 50 * np.copy(feat[:, 18:19]) + 100)\
                            .astype('int32')).type(torch.long).view(1,-1)#.to(device)

    lpc_filters = np.copy(feat[:, -16:])

    start_time = time.time()
    x1 = gen(period, bfcc_with_corr)
    end_time = time.time()
    total_time = end_time - start_time
    x1 = x1.squeeze(1).squeeze(0).detach().numpy()
    gen_seconds = len(x1)/sr
    out = deemphasis(inverse_perceptual_weighting(x1, lpc_filters, weighting_vector))
    sf.write("./{}.wav".format(sig_name), out, samplerate=sr)
    