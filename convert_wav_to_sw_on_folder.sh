#!/bin/bash
for filename in input_wav_folder/*; do

    xbase=${filename##*/};
    xpref=${xbase%.*}
    echo "Processing ${xbase}";

    sox -c "1" -r "16000" ${filename} input_sw_folder/${xpref}.sw;

done