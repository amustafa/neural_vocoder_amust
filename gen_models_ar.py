from audioop import bias
from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
import math
from adaptive_conv1d import *

which_norm = weight_norm
    
class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.nl(self.conv(x))#self.conv(x) #
        return conv_out

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        self.padding_left = (kernel_size//2 ) * dilation
        self.padding_right = (kernel_size//2 ) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding_left, self.padding_right))
        
        conv_out = self.nl(self.conv(x))
        return conv_out
    
class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * torch.sigmoid(self.gate(x))
    
        return out 
    
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1, act='glu', parallel=False):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.parallel = parallel

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        if act=='glu':
            self.nl = GLU(self.hidden_size)

        if act=='tanh':
            self.nl = nn.Tanh()
        
        self.h0 = None

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def one_forward_pass(self, one_frame):
        
        if self.h0 is None:

            output, self.h0 = self.gru(one_frame)

        else:

            output, self.h0 = self.gru(one_frame, self.h0)
        self.h0 = self.nl(output).permute(1,0,2).contiguous() 
        return self.nl(output) 
    
    def forward(self, x):

        if self.parallel:

            self.gru.flatten_parameters()
            output, h0 = self.gru(x)
            return self.nl(output)

        else:

            return self.one_forward_pass(x) 

    def reset(self):
        self.h0 = None
        return
    
class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True, parallel=False):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        self.parallel = parallel

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )

        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
                
        self.buffer = None
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                 
    def one_forward_pass(self, one_frame):
        
        if self.buffer is None:
            self.buffer = torch.zeros(one_frame.size(0),1,self.required_pad_left,device=one_frame.device)
            
        one_frame_flat = one_frame.reshape(one_frame.size(0),1,-1)
        one_frame_flat_padded = torch.cat((self.buffer, one_frame_flat), dim=-1)
        out_frame_new = self.fc(one_frame_flat_padded)
        self.buffer[:,:,:(self.frame_kernel_size - 2)*self.frame_len] = self.buffer[:,:,self.frame_len:].clone()
        self.buffer[:,:,(self.frame_kernel_size - 2)*self.frame_len:] = one_frame_flat
        return out_frame_new

    def forward(self, x):

        if self.parallel:
            x_flat = x.reshape(x.size(0),1,-1)
            x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
            
            x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                        kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
            
            out = self.fc(x_flat_padded_unfolded)#self.activation(torch.matmul(x_flat_padded_unfolded, k.T))#
            return out  

        else:
            return self.one_forward_pass(x)
    
    def reset(self):
        self.buffer = None
        return
    
class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = nn.Linear(in_ch, out_ch * upsample_factor, bias=False)
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
                
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
        
def n(x):
    return torch.clamp(x + (1./127.)*(torch.rand_like(x)-.5), min=-1., max=1.)

class PSARGAN600M400Hz(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.feat_in_conv1 = CausalConv(19,40,kernel_size=3) 
        
        self.bfcc_with_corr_upsampler = UpsampleFC(40,40,4) 
        
        self.feat_in_conv2 = ConvLookahead(120,80,kernel_size=9) 
        self.feat_in_nl = GLU(80)
        
        self.cond_gain_dense = which_norm(nn.Linear(80, 1, bias=False))

        self.fwc = FramewiseConv(80 + 80, 192, frame_kernel_size=3)
        
        self.rnn1 = ForwardGRU(80+192,160)#FramewiseConv(80+192,160, frame_kernel_size=3) #
        self.rnn2 = ForwardGRU(80+160,128)#FramewiseConv(80+160,128, frame_kernel_size=3) #
        self.rnn3 = ForwardGRU(80+128,128)#FramewiseConv(80+128,128, frame_kernel_size=5) #
        

        self.skip_dense = nn.Sequential(which_norm(nn.Linear(80+192+160+128+128+80, 40, bias=False)), GLU(40))
        #self.out_layer = nn.Sequential(which_norm(nn.Linear(80, 40, bias=False)), nn.Tanh())
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_act = self.feat_in_conv1(bfcc_with_corr.permute(0,2,1).contiguous())
        
        envelope_up = self.bfcc_with_corr_upsampler(envelope_act)
        
        feat_in = torch.cat((p_embed , envelope_up), dim=1)
        
        feat_latent = self.feat_in_nl(self.feat_in_conv2(feat_in).permute(0,2,1).contiguous())
        
        gain = torch.exp(self.cond_gain_dense(feat_latent))
                
        period_repeat = torch.repeat_interleave(pitch_period, 4, dim=-1)
        batch_size = x0.size(0)
        device = x0.device
        # Pre-load the first 10 ms of x0 in the memory
        pitch_mem = torch.cat([torch.zeros((batch_size,256-160), device=device), x0[:,:160]], dim=-1)
        ar_wav = x0[:, 120:160].unsqueeze(1)
        out_waveform = x0

        # Start running the model on the last 10 ms of x0
        for i in range(4, feat_latent.size(1)):

            T = period_repeat[:, i:i+1]
            ind = 256 - T
            ind = ind + torch.arange(40, device=ind.device)
            mask = ind >= 256
            ind = ind - mask*T
            pred_wav = torch.gather(pitch_mem, dim=-1, index=ind).unsqueeze(1) 

            pred_wav = n(pred_wav/(1e-12 + gain[:,i:i+1,:]))
            ar_wav =   n(ar_wav/(1e-12 + gain[:,i:i+1,:]))
   
            phase_sig = torch.cat((pred_wav, ar_wav), dim=-1)
    
            fwc_out = self.fwc(torch.cat((feat_latent[:,i:i+1,:], phase_sig), dim=-1))
            
            rnn1_out = self.rnn1(torch.cat((fwc_out, phase_sig), dim=-1))
            rnn2_out = self.rnn2(torch.cat((rnn1_out, phase_sig), dim=-1))
            rnn3_out = self.rnn3(torch.cat((rnn2_out, phase_sig), dim=-1))
            
            skip = torch.cat((feat_latent[:,i:i+1,:], fwc_out, rnn1_out, rnn2_out, rnn3_out, phase_sig), dim=-1)
            out = self.skip_dense(skip)#self.out_layer(self.skip_dense(skip))

            if i<8:

                out_frame = x0[:,i*40:(i+1)*40]

            else:

                out_frame = out.squeeze(1) * gain[:,i:i+1,:].squeeze(1)
                out_waveform = torch.cat((out_waveform, out_frame), dim=-1)

            ar_wav = out_frame.unsqueeze(1)
            pitch_mem = torch.cat([pitch_mem[:,40:], out_frame], dim=-1)

        self.fwc.reset()
        self.rnn1.reset()
        self.rnn2.reset()
        self.rnn3.reset()

        return  out_waveform

class FARGAN(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.feat_in_conv1 = CausalConv(19,40,kernel_size=3) 
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,40,4) 
        
        self.feat_in_conv2 = ConvLookahead(120,80,kernel_size=9) 
        self.feat_in_nl = nn.LeakyReLU(0.2) #GLU(80)
        
        self.fwc = ForwardGRU(80, 192, parallel=True)
        
        self.rnn1 = FramewiseConv(192, 160, frame_kernel_size=3, parallel=True) 
        self.rnn2 = FramewiseConv(160, 128, frame_kernel_size=3, parallel=True) 
        self.rnn3 = FramewiseConv(128, 128, frame_kernel_size=5, parallel=True) 
        self.rnn4 = FramewiseConv(128, 40, frame_kernel_size=5, parallel=True)
        self.rnn5 = FramewiseConv(40, 40, frame_kernel_size=5, act= 'tanh', parallel=True)
        
        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 *torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_act = self.feat_in_conv1(bfcc_with_corr.permute(0,2,1).contiguous())
        
        envelope_up = self.bfcc_with_corr_upsampler(envelope_act)
        
        feat_in =  torch.cat((p_embed , envelope_up), dim=1)#p_embed[:,:40,:] + p_embed[:,40:,:] + envelope_up #
        
        feat_latent = self.feat_in_nl(self.feat_in_conv2(feat_in).permute(0,2,1).contiguous())
        
        fwc_out = self.fwc(feat_latent)

        rnn1_out = self.rnn1(fwc_out)
        rnn2_out = self.rnn2(rnn1_out)
        rnn3_out = self.rnn3(rnn2_out)
        rnn4_out = self.rnn4(rnn3_out)
        rnn5_out = self.rnn5(rnn4_out) 
        
        out = rnn5_out.reshape(rnn5_out.size(0),1,-1) 

        return  out.squeeze(1)
    
class FFTConv(nn.Module):
    def __init__(self):
        super(FFTConv, self).__init__()
        
        torch.manual_seed(5)

    def forward(self, kernel, x):

        kernel_fft = torch.fft.rfft(kernel)
        x_fft = torch.fft.rfft(x)
        out = torch.fft.irfft(kernel_fft * x_fft)

        return out
    
class FWCLite(nn.Module):
    def __init__(self, input_dim, out_dim, res=True, mod=True, act='glu'):
        super(FWCLite, self).__init__()
        
        torch.manual_seed(5)

        self.input_dim = input_dim
        self.out_dim = out_dim
        self.feature_mixing_layer = FramewiseConv(self.input_dim, self.out_dim, act='tanh',parallel=True)
        self.fft_conv = FFTConv()
        if act == 'glu':
            self.nl = GLU(out_dim)
        if act=='tanh':
            self.nl = nn.Tanh()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, kernel, signal):

        fc_out = self.feature_mixing_layer(kernel)
        out = self.nl(self.fft_conv(fc_out, signal))  
        return out, fc_out 
    
class IdentityModel(torch.nn.Module):
    
    def __init__(self):
        
        super(IdentityModel, self).__init__()

        self.fc1 = ForwardGRU(19+160+160,160, parallel=True) 

        self.fc2 = FWCLite(160,160)
        
        self.fc3 = FWCLite(160,160)
        
        self.fc4 = FWCLite(160,160)

        self.fc5 = FWCLite(160,160)
        
        self.fc6 = FWCLite(160,160)
        
        self.fc7 = FWCLite(160,160)

        self.fc8 = FWCLite(160,160)
        
        self.fc9 = FWCLite(160,160)
        
        self.fc10 = FWCLite(160,160, act='tanh')

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 *torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,160)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,160)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals
        
    def create_phase_signals1d(self, periods, frame_len=160, num_freq=1):
        
        phase_all_freq = []
        batch_size = periods.size(0)
        
        for freq_ind in range(1,num_freq+1):    
                
            progression = torch.arange(1, frame_len + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
            progression = torch.repeat_interleave(progression, batch_size, 0)

            phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
            chunks_per_freq = []
            
            for sframe in range(periods.size(1)):
                f = (2.0 * freq_ind * torch.pi / periods[:, sframe]).unsqueeze(-1)

                chunk_sin = torch.sin(f  * progression + phase0)
                chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,1)

                chunk_cos = torch.cos(f  * progression + phase0)
                chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,1)

                chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)

                phase0 = phase0 + frame_len * f

                chunks_per_freq.append(chunk)

            phase_signals = torch.cat(chunks_per_freq, dim=1)
            phase_all_freq.append(phase_signals)
        return torch.cat(phase_all_freq, -1)
                
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.create_phase_signals(pitch_period)

        p_sin = p_embed[:,:,:160]
        p_cos = p_embed[:,:,160:]
        p_noise = torch.randn_like(p_sin)
        signal = (p_sin + p_cos + p_noise )/3.0
        
        feat_in =  torch.cat((p_embed , bfcc_with_corr), dim=-1) 

        out_fc1 = self.fc1(feat_in) 
        out_fc2, signal = self.fc2(out_fc1, out_fc1)
        out_fc3, signal = self.fc3(out_fc2, out_fc2)
        out_fc4, signal = self.fc4(out_fc3, out_fc3)
        out_fc5, signal = self.fc5(out_fc4, out_fc4)
        out_fc6, signal = self.fc6(out_fc5, out_fc5)
        out_fc7, signal = self.fc7(out_fc6, out_fc6)
        out_fc8, signal = self.fc8(out_fc7, out_fc7)
        out_fc9, signal = self.fc9(out_fc8, out_fc8)
        out_fc10, signal = self.fc10(out_fc9, out_fc9)
        out = out_fc10.reshape(out_fc10.size(0),1,-1)

        return out.squeeze(1)
    
class PSARGAN500HzNew(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.p_embed = nn.Embedding(256,13)
        self.feat_in_conv1 = CausalConv(19+13,128,kernel_size=3) 
        self.feat_in_conv2 = ConvLookahead(128,128,kernel_size=3) 
        self.feat_in_nl = GLU(128)

        self.feat_upsampler = UpsampleFC(128,128,5) 
        
        self.cond_gain_dense = which_norm(nn.Linear(128, 1, bias=False))

        self.fwc0 = FramewiseConv(64+128,128, frame_kernel_size=6) 
        
        self.fwc1 = FramewiseConv(64+128,128, frame_kernel_size=3) 
        self.fwc2 = FramewiseConv(64+128,128, frame_kernel_size=3) 
        self.fwc3 = FramewiseConv(64+128,32, frame_kernel_size=6) 
        #self.skip = nn.Sequential(which_norm(nn.Linear(128+128+128+128+32, 64, bias=False)), GLU(64))
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,160)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,160)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    
    def forward(self, pitch_period, bfcc_with_corr, x0):

        p_embed = self.p_embed(pitch_period)
        
        feat_in = torch.cat((p_embed , bfcc_with_corr), dim=-1).permute(0, 2, 1).contiguous()
        
        feat_latent = self.feat_in_nl(self.feat_in_conv2(self.feat_in_conv1(feat_in)).permute(0,2,1).contiguous())
        
        feat_latent = self.feat_upsampler(feat_latent.permute(0,2,1).contiguous()).permute(0,2,1).contiguous()
        
        gain = torch.exp(self.cond_gain_dense(feat_latent))
                
        period_repeat = torch.repeat_interleave(pitch_period, 5, dim=-1)
        batch_size = x0.size(0)
        device = x0.device
        # Pre-load the first 10 ms of x0 in the memory
        pitch_mem =  torch.zeros_like(x0) #torch.cat([torch.zeros((batch_size,320-160), device=device), x0[:,:160]], dim=-1)
        ar_wav = torch.zeros_like(x0[:,:32].unsqueeze(1)) #x0[:, :160].unsqueeze(1)
        out_waveform = x0

        # Start running the model on the last 10 ms of x0
        for i in range(0, feat_latent.size(1)):
            
            T = period_repeat[:, i:i+1]
            ind = 320 - T
            ind = ind + torch.arange(32, device=ind.device)

            pred_wav = torch.gather(pitch_mem, dim=-1, index=ind).unsqueeze(1) 

            pred_wav = n(pred_wav/(1e-12 + gain[:,i:i+1,:]))
            ar_wav =   n(ar_wav/(1e-12 + gain[:,i:i+1,:]))
   
            phase_sig = torch.cat((pred_wav, ar_wav), dim=-1)
    
            fwc0_out = self.fwc0(torch.cat((feat_latent[:,i:i+1,:], phase_sig), dim=-1))
            fwc1_out = self.fwc1(torch.cat((fwc0_out, phase_sig), dim=-1))
            fwc2_out = self.fwc2(torch.cat((fwc1_out, phase_sig), dim=-1))
            fwc3_out = self.fwc3(torch.cat((fwc2_out, phase_sig), dim=-1))
            
            if i<10:

                out_frame = x0[:,i*32:(i+1)*32]

            else:

                out_frame = fwc3_out.squeeze(1) * gain[:,i:i+1,:].squeeze(1)
                out_waveform = torch.cat((out_waveform, out_frame), dim=-1)

            ar_wav = out_frame.unsqueeze(1)
            pitch_mem = torch.cat([pitch_mem[:,32:], out_frame], dim=-1)

        self.fwc0.reset()
        self.fwc1.reset()
        self.fwc2.reset()
        self.fwc3.reset()

        return  out_waveform