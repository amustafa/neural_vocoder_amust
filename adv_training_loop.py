import time
import torch
import os
import gc
from gen_models import *
from disc_models import *
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator_mel import AudioSampleGenerator
import soundfile as sf
import random
from feature_extractors import *
from stft_loss import *


################################# define some functions for processing ###################################

def fetch_data(batch, batch_size, device):
    
    wb_speech = batch['audio'].type(torch.FloatTensor).view(batch_size,16000).to(device)

    return wb_speech

def feature_loss(fmap_r, fmap_g, detach_first=False):
    loss = 0
    i = 0

    for dr, dg in zip(fmap_r, fmap_g):
        for rl, gl in zip(dr, dg):
            loss += torch.mean(torch.abs(rl - gl))
            i = i+1

    return loss/i

def discriminator_loss(disc_real_outputs, disc_generated_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dr, dg in zip(disc_real_outputs, disc_generated_outputs):
        r_loss =  torch.mean((1-dr)**2)
        g_loss =  torch.mean(dg**2) 
        loss += (r_loss + g_loss)
        hing_loss += ((torch.nn.ReLU()(1.0 - dr)).mean() + (torch.nn.ReLU()(1.0 + dg)).mean()).item() 
        i = i+1
    return loss/i, (hing_loss/i)


def generator_loss(disc_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dg in disc_outputs:
        l = torch.mean((1-dg)**2) 
        loss += l
        hing_loss += (-1.0 * dg.mean()).item()
        i = i+1

    return loss/i, (hing_loss/i)

################################# define multi-resolution spectrogram & correlogram discriminator architectures ###############

class DiscriminatorC(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorC, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 9), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.correlogram(x.squeeze(1), frame_size=self.resolution)

        x = x.unsqueeze(1)
    
        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def correlogram(self, x, frame_size=160, search_size=320):

        temp_dim = x.size(1)
        
        kernel = torch.cat([x[:,i:i+frame_size].clone().view(-1, 1, frame_size)\
                            for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
        
        kernel_splitted = torch.split(kernel, 1, dim=0)
        
        kernel = torch.cat(kernel_splitted, dim=1).permute(1,0,2)
        
        segment = torch.cat([x[:,i:i+frame_size+search_size].clone().view(-1, 1, frame_size + search_size) \
                            for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
        
        num_frames = segment.size(1)
        
        segment_splitted = torch.split(segment, 1, dim=0)
        
        segment = torch.cat(segment_splitted, dim=1)
        
        xcorr = F.conv1d(segment, kernel, groups=kernel.size(0))
        
        xcorr_splitted = torch.split(xcorr, num_frames, dim=1)
        
        xcorr = torch.cat(xcorr_splitted, dim=0)
        
        return xcorr.permute(0, 2, 1).contiguous()

class DiscriminatorR(torch.nn.Module):
    def __init__(self, resolution, use_spectral_norm=False):
        super(DiscriminatorR, self).__init__()
        norm_f = weight_norm if use_spectral_norm == False else spectral_norm
        self.resolution = resolution
        self.num_channels = 32

        self.convs = nn.ModuleList([
            norm_f(nn.Conv2d(1, self.num_channels, (3, 9), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 9), stride=(1, 2), padding=(1, 4))),
            norm_f(nn.Conv2d(self.num_channels, self.num_channels, (3, 3), padding=(1, 1))),
        ])
        self.conv_post = norm_f(nn.Conv2d(self.num_channels, 1, (3, 3), padding=(1, 1)))

        #self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")

    def forward(self, x):
        fmap = []

        x = self.spectrogram(x)
        
        x = x.unsqueeze(1)

        for l in self.convs:
            x = l(x)
            x = F.leaky_relu(x, 0.2)
            fmap.append(x)
        x = self.conv_post(x)
        
        fmap.append(x)
        x = torch.flatten(x, 1, -1)

        return x, fmap

    def spectrogram(self, x):
        n_fft, hop_length, win_length = self.resolution
        x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
        x = x.squeeze(1)
        with autocast(enabled=False):
            x = torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                           center=False, return_complex=False) #[B, F, TT, 2]
            mag = torch.norm(x, p=2, dim =-1) #[B, F, TT]

        return torch.sqrt(mag)

class TFDMultiResolutionDiscriminator(torch.nn.Module):
    def __init__(self, use_spectral_norm=False):
        super(TFDMultiResolutionDiscriminator, self).__init__()
        #periods = [2,3,5,7,11]
        #resolutions = [[1024, 120, 600], [2048, 240, 1200], [512, 50, 240]]
        #resolutions = [[2048, 256, 1024], [1024, 128, 512], [512, 64, 256], [256, 32, 128], [128, 16, 64], [64, 8, 32]]
        resolutions = [[2048, 256, 1024], [1024, 128, 512], [512, 64, 256]]
        corr_resolutions = [80,160,320]

        discs = [DiscriminatorR(resolutions[i], use_spectral_norm=use_spectral_norm) for i in range(len(resolutions))]
        discs = discs + [DiscriminatorC(c_res) for c_res in corr_resolutions] #[DiscriminatorP(i, use_spectral_norm=use_spectral_norm) for i in periods]
        self.discriminators = nn.ModuleList(discs)

    def forward(self, y, y_hat):
        y_d_rs = []
        y_d_gs = []
        fmap_rs = []
        fmap_gs = []
        for i, d in enumerate(self.discriminators):
            y_d_r, fmap_r = d(y)
            y_d_g, fmap_g = d(y_hat)
            y_d_rs.append(y_d_r)
            y_d_gs.append(y_d_g)
            fmap_rs.append(fmap_r)
            fmap_gs.append(fmap_g)

        return y_d_rs, y_d_gs, fmap_rs, fmap_gs

if __name__ == "__main__":

    torch.cuda.init()
    
    out_path = 'trained_models' 
    audio_data_file = '../datasets/total_audio_dataset_ms_challenge.sw' 

    model_fdr = 'models'  
    opt_fdr='optimizers'

    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 32

    sample_generator = AudioSampleGenerator(audio_data_file, batch_size)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0001#/2
    d_learning_rate = 0.0001#/2
    
    device = torch.device('cuda:5')
    generator =  Generator() #You should replace this class with the class definition of your speech uplift generator
    
    generator.to(device)
    
    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])
    
    msd =  TFDMultiResolutionDiscriminator().to(device) 
    
    msd_optimizer = optim.AdamW(msd.parameters(), d_learning_rate, betas=[0.8, 0.99])


    #load the saved generator model from pre-training (i.e., stft loss only) phase

    saved_model_path= 'trained_models/20220828_1715/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)
 
    saved_opt_path= 'trained_model/20220828_1715/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate

    spect_loss =  MultiResolutionSTFTLoss(device).to(device)

    feature_extractor_g = LogMel().to(device)
   
    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')
    
    #The training loop lasts for too many epochs, so feel free to interrupt the training when you find the quality is good enough

    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):

            gc.collect()
            torch.cuda.empty_cache()
            
            wb_speech = fetch_data(sample_batch, batch_size, device)

            feat = feature_extractor_g(wb_speech)[:,:,:100]

            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            ###################### Train D ######################

            #generate fake speech
            speech_generated = generator(feat.detach())  

            #wb_speech is the real speech sample [batch_size, segment_size] we use unsqueeze(1) to make it 3 dim 
            #speech_generated is the fake speech sample [batch_size, segment_size] we use unsqueeze(1) to make it 3 dim 
            #note that we call .detach() for generated speech as we only need its data for optimizing the discriminator model

            disc_ms_real, disc_ms_gen, _, _ = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            loss_ms_disc, loss_ms_disc_hinge = discriminator_loss(disc_ms_real, disc_ms_gen)

            total_d_loss = loss_ms_disc 

            #we use loss_ms_disc_hinge just for visualization of the adv loss in using a hinge metric, while total_d_loss is a least-square loss w.r.t 0 and 1 is the actual loss used for 
            #optimization (not to visualize the adversarial behavior, the hinge one is more elaborative)
            total_d_loss_hinge = loss_ms_disc_hinge 

            total_d_loss.backward()

            msd_optimizer.step()
            
            ###################### Train G ######################
            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            speech_generated = generator(feat.detach())  

            disc_ms_real, disc_ms_gen, fm_ms_real, fm_ms_gen = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1))  
            loss_ms_disc, loss_ms_disc_hinge = generator_loss(disc_ms_gen)   

    
            g_adv_loss = loss_ms_disc#

            g_adv_loss_hinge = loss_ms_disc_hinge 

            #g_fm_loss = fm_loss_ms 

            tf_loss = spect_loss(speech_generated, wb_speech.detach()) 
   
            
            total_loss = g_adv_loss + tf_loss # g_fm_loss #enable feature loss if you need it. I don't use it at the moment

            total_loss.backward()
            
            g_optimizer.step()
            

            if (i + 1) % 1 == 0:
            
                print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}\n')  

            if ((epoch % 1 == 0) and (i % 100 == 0)):

                model_path = os.path.join(models_path, 'generator.pkl')
                torch.save(generator.state_dict(), model_path)


                model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                torch.save(g_optimizer.state_dict(), model_opt_path) 

                model_path = os.path.join(models_path, 'msd.pkl')
                torch.save(msd.state_dict(), model_path)


                model_opt_path = os.path.join(optimizer_path, 'msd_opt.pkl')
                torch.save(msd_optimizer.state_dict(), model_opt_path) 


    print('Finished Training!')

