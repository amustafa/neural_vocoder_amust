
import torch

import numpy as np


# The LPC analysis is only implemented with numpy 
# The LPC synthesis is implemented in both numpy and torch

################################################# LPC analysis #################################################

def lpc_analysis (signal, filters):

    #signal: 1d numpy array of length that is divisible by frame length (160)
    #filters: 2d numpy array of shape: num_frames X 16
    
    residual = np.zeros_like(signal)
    buffer = np.zeros(16)
    num_frames = signal.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = signal[frame_idx*160: (frame_idx+1)*160][:]
        out_res_frame = lpc_analysis_one_frame(in_frame, filters[frame_idx, :], buffer)
        residual[frame_idx*160: (frame_idx+1)*160] = out_res_frame[:]
        buffer[:] = in_frame[-16:]
    
    return residual 
    
    
def lpc_analysis_one_frame(frame, filt, buffer):
    
    out = np.zeros_like(frame)
    
    filt = np.flip(filt)
    
    inp = np.concatenate((buffer, frame))
    j = 0
    
    for i in range(16, inp.shape[0]):
        
        out[j] = inp[i] + np.dot(inp[i-16:i], filt)
        
        j = j+1
    
    return out


################################################# LPC synthesis - numpy #################################################

def lpc_synthesis (residual, filters):

    #residual: 1d numpy array of length that is divisible by frame length (160)
    #filters: 2d numpy array of shape: num_frames X 16
    
    signal = np.zeros_like(residual)
    buffer = np.zeros(16)
    num_frames = residual.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = residual[frame_idx*160: (frame_idx+1)*160][:]
        out_sig_frame = lpc_synthesis_one_frame(in_frame, filters[frame_idx, :], buffer)
        signal[frame_idx*160: (frame_idx+1)*160] = out_sig_frame[:]
        buffer[:] = out_sig_frame[-16:]
    
    return signal 


def lpc_synthesis_one_frame(frame, filt, buffer):
    
    out = np.zeros_like(frame)
    
    filt = np.flip(filt)
    
    inp = frame[:]
    
    for i in range(0, inp.shape[0]):
        
        s = inp[i] - np.dot(buffer, filt)
        
        buffer[0] = s
        
        buffer = np.roll(buffer, -1)
        
        out[i] = s
        
    return out

################################################# LPC synthesis - torch #################################################

def lpc_synthesis_torch (residual, filters):
    
    signal = torch.zeros_like(residual)
    
    filters = torch.flip(filters, dims=[2])
    
    buffer = torch.zeros(signal.size(0),16, device=signal.device)
    
    num_frames = residual.size(-1)//160
    
    for frame_idx in range(0, num_frames):
        
        #torch.clone is used to ensure safe gradient flow between tensors 

        in_frame = torch.clone(residual[:,frame_idx*160: (frame_idx+1)*160])
        out_sig_frame = lpc_synthesis_one_frame_torch(in_frame, filters[:,frame_idx, :], buffer)
        signal[:,frame_idx*160: (frame_idx+1)*160] = torch.clone(out_sig_frame)
        buffer = torch.clone(out_sig_frame[:, -16:])
    
    return signal 

def lpc_synthesis_one_frame_torch(frame, filt, buffer):
    
    out = torch.zeros_like(frame)
    
    inp = torch.clone(frame)
    
    for i in range(0, inp.size(-1)):
        
        #here is the main bottleneck, I use  direct multiply-sum because the torch.dot & torch.inner functions don't work as expected for this operation
        s = inp[:,i:i+1] - torch.sum(buffer*filt, dim=-1, keepdim=True)
        
        buffer[:,:1] = torch.clone(s)
        
        buffer = torch.roll(buffer, -1, -1)
        
        out[:,i:i+1] = torch.clone(s)
        
    return out
