import struct
import numpy as np
import tqdm 
import librosa 

audio_file_samples = np.zeros(5760000000)
"""i=0
with open('../datasets/orig_speech_dataset_100hrs.f32', 'rb') as f:
    print('loading audio ...')
    for i in tqdm.tqdm(range(len(audio_file_samples))):
        wav_vector_bytes = f.read(4)
        if not wav_vector_bytes:
            print(f'number of read audio samples: {i}')
            print('EOF')
            break

        else:
            audio_file_samples[i] = struct.unpack('1f', wav_vector_bytes)[0]
            i = i+1"""


with open('../datasets/orig_speech_dataset_100hrs.f32', 'rb') as f:
    print('loading audio ...')
    
    wav_vector_bytes = f.read(4*5760000000)
    if not wav_vector_bytes:
        print('EOF')
    else:
        audio_file_samples[:] = struct.unpack('5760000000f', wav_vector_bytes)

f.close()

print('calculating melspectrogram ...')

mel_spectrogram = librosa.feature.melspectrogram(audio_file_samples, sr=16000,n_fft=1024, hop_length=160, n_mels=128)
print(mel_spectrogram.shape)
mel_spectrogram = 10.0*np.log10(mel_spectrogram)[:,:36000000]

print('saving melspectrogram ...')
np.save('../datasets/mel_orig_speech_dataset_100hrs.npy',mel_spectrogram)
print('Done!')

