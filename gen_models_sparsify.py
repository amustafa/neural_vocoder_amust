from audioop import bias
from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
import math


which_norm = weight_norm
kernel_size = 9

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, density=1):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        if density < 1:
            self.conv = nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation,bias= False)
        else:
            self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation,bias= False))

        self.density = density
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out

class Upsample(nn.Module):
    
    def __init__(self,ch1,ch2, rate1=0, rate2=0):
        
        super().__init__()
        torch.manual_seed(5)
        self.rate1 = rate1
        self.rate2 = rate2
        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x):
        
        x = self.nl(self.conv(F.interpolate(x, scale_factor= self.rate2 / self.rate1)))
        return x
          
class EnergyConv(nn.Module):
    
    def __init__(self,ch1,ch2):
        
        super().__init__()
        torch.manual_seed(5)

        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, energy):
        x = self.nl(self.conv(energy))
        
        return x
        
class TADE(nn.Module):
    
    def __init__(self,num_channels, k_siz, dilation_factor, learn_beta = True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.learn_beta = learn_beta
        self.mlp_shared = nn.Sequential(CausalConv(80,64,kernel_size=k_siz,dilation=dilation_factor),
                          nn.LeakyReLU(0.2))
       
        self.gamma_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
        if self.learn_beta:
            self.beta_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
     
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, cond, scale_factor):
           
        cond_interp = F.interpolate(cond,scale_factor= scale_factor)
        actv = self.mlp_shared(cond_interp)

        gamma = self.gamma_conv(actv)
        if self.learn_beta:
            beta = self.beta_conv(actv)
        else:
            beta = None
        
        return gamma, beta 
        
class Modulation(nn.Module):
    
    def __init__(self,num_channels, apply_norm= True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.apply_norm = apply_norm
        self.eps = 1e-05

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x, gamma, beta):
         
        if self.apply_norm:

            mu, var = torch.mean(x, dim=1, keepdim=True), torch.var(x, dim=1, keepdim=True)
            x_norm = (x - mu) * torch.rsqrt(var + self.eps)
            
        else:
            x_norm = x
            
        if beta is not None:
            out = (x_norm * gamma) + beta
        else:
            out = (x_norm * gamma)
        return out

class TADEResBlock(nn.Module):
    
    def __init__(self,in_channels, out_channels, k_siz=kernel_size, learn_beta = True, density=1):
        
        super().__init__()
        torch.manual_seed(5)
        self.in_channels= in_channels
        self.out_channels= out_channels
        dil = 1
        
        self.tade = TADE(in_channels,k_siz,1, learn_beta)
        
        self.mod1 = Modulation(self.out_channels)
        self.conv1_tanh = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1, density=density)
        self.conv1_gate = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1, density=density)

        self.mod2 = Modulation(self.out_channels)
        self.conv2_tanh = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2, density=density)
        self.conv2_gate = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2, density=density) 
                                    
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        
    def forward(self, x, cond):
        
        temp_res = x.shape[2]
        mel_res = cond.shape[2]
        scale_factor  = temp_res/mel_res
        
        gamma, beta = self.tade(cond, scale_factor)
        
        mod_out1 = self.mod1(x,gamma,beta)
        out1 = torch.tanh(self.conv1_tanh(mod_out1)) * F.softmax(self.conv1_gate(mod_out1),dim=1)
        
        mod_out2 = self.mod2(out1,gamma,beta) 
        out2 = torch.tanh(self.conv2_tanh(mod_out2)) * F.softmax(self.conv2_gate(mod_out2),dim=1)

        out = out2 + x
        return out

class SpadeGen(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.latent_dim = 64

        self.energy_conv = EnergyConv(1,self.latent_dim)
        
        self.act0 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler0 = Upsample(self.latent_dim,self.latent_dim,100,200)
        
        self.act1 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler1 = Upsample(self.latent_dim,self.latent_dim,200,500)

        self.act2 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler2 = Upsample(self.latent_dim,self.latent_dim,500,1000)

        self.act3 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler3 = Upsample(self.latent_dim,self.latent_dim,1000,2000)

        self.act4 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler4 = Upsample(self.latent_dim,self.latent_dim,2000,4000)

        self.act5 = TADEResBlock(self.latent_dim,self.latent_dim, density=0.5)

        self.act6 = TADEResBlock(self.latent_dim,self.latent_dim, density=0.5)

        self.act7 = TADEResBlock(self.latent_dim,self.latent_dim, density=0.5)
   
        self.anti_alias = TADEResBlock(self.latent_dim,self.latent_dim, density=0.5)

        self.dec_conv = CausalConv(self.latent_dim,4,kernel_size=kernel_size,dilation=1)
        self.dec_tanh = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, energy):
         
        latent = self.energy_conv(energy)
        latent  = self.upsampler0(self.act0(latent,x)) 
        stage125_250 = self.upsampler1(self.act1(latent, x))
        stage250_500 = self.upsampler2(self.act2(stage125_250 ,x))
        stage500_1000 = self.upsampler3(self.act3(stage250_500 ,x))
        stage1000_2000 = self.upsampler4(self.act4(stage500_1000 ,x))
        stage2000_4000 = self.act5(stage1000_2000 ,x)
        stage4000_8000 = self.act6(stage2000_4000 ,x)
        stage8000_16000 = self.act7(stage4000_8000 ,x)
        ant_alias = self.anti_alias(stage8000_16000 ,x)          
        d_conv= self.dec_conv(ant_alias)
        out = self.dec_tanh(d_conv)
   
        return out
        
class Generator(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.mel_conv = CausalConv(80,80,kernel_size=kernel_size,dilation=1)
        
        self.spade_generator = SpadeGen()    

        self.pqmf = PQMF()
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
                              
    def forward(self, mel):

        energy = torch.sum(mel, 1, keepdim=True)
    
        x = self.spade_generator(self.mel_conv(mel), energy)   
        x = self.pqmf.synthesis(x)
        return x.squeeze(1)
        
    def load_state_dict(self, state_dict: 'OrderedDict[str, Tensor]', strict: bool = True):

        # we need a recursive version of getattr
        def getattr_recursive(obj, attribute_key):
            keys = attribute_key.split('.')
            if len(keys) == 1:
                return getattr(obj, keys[0])
            else:
                next_key = keys[0]
                obj = getattr(obj, keys[0])
                return getattr_recursive(obj, attribute_key[len(next_key) + 1:])

        # make a copy of the state dict
        modified_state_dict = state_dict.copy()

        # get all conv layers with weight_v attribute...
        conv_dict = [key for key in state_dict.keys() if key.endswith('conv.weight_v')]
        
        # ... and modify those...
        for entry_v in conv_dict:
            attribute_key = entry_v[:-13] + 'density'
            try:
                density = getattr_recursive(self, attribute_key)
            except:
                continue

            # ... which have density smaller 1
            if density < 1:
                entry_g = entry_v[:-1] + 'g'
                entry = entry_v[:-2]

                modified_state_dict[entry] = state_dict[entry_v] * state_dict[entry_g]
                del modified_state_dict[entry_v]
                del modified_state_dict[entry_g]


        return super().load_state_dict(modified_state_dict, strict)


############################################################# Framewise Vocoder Layers #############################################################
        
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1, density=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size
        self.density= density

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.snake = Snake(self.hidden_size, self.density)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return self.snake(output) #output#

class Snake(nn.Module):
    def __init__(self, feat_size, density=1):
        super(Snake, self).__init__()
        
        torch.manual_seed(5)

        self.density = density

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * (1.0 + torch.sigmoid(self.gate(x)))

        return out 

class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, density=1):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.density = density
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                Snake(self.fc_out_dim, self.density)
                               )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out 

class CondFramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=2, apply_act=True, density=1):
        
        super(CondFramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.density = density
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        
        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim

        if apply_act:
        
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False)),
                                    Snake(self.fc_out_dim, self.density)
                                   )

        else:
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False))
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, cond):


        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()

        
        cond_x_flat_padded_unfolded = torch.cat([cond, x_flat_padded_unfolded], dim=2)
        out = self.fc(cond_x_flat_padded_unfolded)

        return out


############################################################### Vocoder definitions ###############################################################################
class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNet(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=9)
        
        self.feat_in_conv = CausalConv(256,320,kernel_size=9)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(320,320, density=0.15)
        
        self.rnn1 = ForwardGRU(320,320, density=0.15)
             
        self.rnn2 = ForwardGRU(320,320, density=0.15)
        
        self.rnn3 = ForwardGRU(320,320, density=0.15)
                 
        self.rnn4 = ForwardGRU(320,320, density=0.15)
        
        self.fc = which_norm(nn.Linear(1920, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512, density=0.15)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.15),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2, apply_act=False)]
                                            )
        self.out = nn.Tanh()

        self.density = 0.15

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)

        final = self.out(signal)
                               
        waveform = final.reshape(final.size(0),1,-1).squeeze(1)

        return waveform

class CondRNNFramewiseVocoderPerceptualDomainSnakeLPCNetLite(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=3)
        
        self.feat_in_conv = CausalConv(256,256,kernel_size=3)
        self.feat_in_snake = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(256,256, density=0.6)
        
        self.rnn1 = ForwardGRU(256,256, density=0.6)
             
        self.rnn2 = ForwardGRU(256,256, density=0.6)
        
        self.rnn3 = ForwardGRU(256,256, density=0.6)
                 
        self.rnn4 = ForwardGRU(256,256, density=0.6)
        
        self.fc = which_norm(nn.Linear(1536, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512, density=0.65)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2, density=0.65),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2)]
                                            )
        self.out = nn.Sequential(CausalConv(1,1,kernel_size= 320),
                                 nn.Tanh())

        self.density = 0.65

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_snake(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)
                 
        waveform = self.out(signal.reshape(signal.size(0),1,-1)).squeeze(1)

        return waveform