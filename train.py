import time
import torch
import os
import gc
from gen_models import *
from disc_models import *
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator import AudioSampleGenerator
import soundfile as sf
import random
from feature_extractors import *
from stft_loss import *

def fetch_data(batch, batch_size, device):
    
    #nb_speech = batch['feats'].type(torch.FloatTensor).view(batch_size,16000).to(device)
    wb_speech = batch['audio'].type(torch.FloatTensor).view(batch_size,16000).to(device)
    voiced_flags = batch['voiced'].type(torch.FloatTensor).view(batch_size,1,16000).to(device)
    unvoiced_flags = batch['unvoiced'].type(torch.FloatTensor).view(batch_size,1,16000).to(device)
    #wb_speech[:,1:] =  wb_speech[:,1:] - 0.85*wb_speech[:,:-1]
    
    return wb_speech, voiced_flags, unvoiced_flags #nb_speech, 

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True

def feature_loss(fmap_r, fmap_g):
    loss = 0
    i = 0
    for dr, dg in zip(fmap_r, fmap_g):
        for rl, gl in zip(dr, dg):
            loss += torch.mean(torch.abs(rl - gl))
            i = i+1
    return loss/i


def discriminator_loss(disc_real_outputs, disc_generated_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dr, dg in zip(disc_real_outputs, disc_generated_outputs):
        r_loss =  torch.mean((1-dr)**2) #(torch.nn.ReLU()(1.0 - dr)).mean() #
        g_loss =  torch.mean(dg**2) #(torch.nn.ReLU()(1.0 + dg)).mean() #
        loss += (r_loss + g_loss)
        hing_loss += ((torch.nn.ReLU()(1.0 - dr)).mean() + (torch.nn.ReLU()(1.0 + dg)).mean()).item() #(torch.mean((1-dr)**2) + torch.mean(dg**2)).item()
        i = i+1
    return loss/i, (hing_loss/i)


def generator_loss(disc_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dg in disc_outputs:
        l = torch.mean((1-dg)**2) #-1.0 * dg.mean() #
        loss += l
        hing_loss += (-1.0 * dg.mean()).item()
        i = i+1

    return loss/i, (hing_loss/i)

def get_autocorrelation(x):
    b,t = x.size()
    x = x.view(b, 1, -1) 
    x_permute = x.permute(1,0,2)
    pad_size = (t-1)//2
    x_as_input = F.pad(x_permute, (0 , pad_size))
    x_as_parameter = x
    out = F.conv1d(x_as_input, x_as_parameter, groups=b).squeeze(0)
    #maxima = out.max(dim=-1)[0].unsqueeze(1)
    #out = torch.div(out, maxima)
    return out

def get_crosscorrelation(x, y):
    b,t = x.size()
    x = x.view(b, 1, -1) 
    y = y.view(b, 1, -1) 
    x_permute = x.permute(1,0,2)
    pad_size = (t-1)//2
    x_as_input = F.pad(x_permute, (0 , pad_size))
    y_as_parameter = y
    out = F.conv1d(x_as_input, y_as_parameter, groups=b).squeeze(0)
    #maxima = out.max(dim=-1)[0].unsqueeze(1)
    #out = torch.div(out, maxima)
    return out

def get_short_term_correlation_hr(x):
    
    kernel = torch.cat([x[:,i:i+160].clone().view(-1, 1, 160)\
                        for i in range(0, 16000-320-80, 80)], dim=1)
    
    kernel_splitted = torch.split(kernel, 1, dim=0)
    
    kernel = torch.cat(kernel_splitted, dim=1).permute(1,0,2)
     
    segment = torch.cat([x[:,i+32:i+256+160-1].clone().view(-1, 1, 383) \
                         for i in range(0, 16000-320-80, 80)], dim=1)
    
    segment_splitted = torch.split(segment, 1, dim=0)
    
    segment = torch.cat(segment_splitted, dim=1)
    
    xcorr = F.conv1d(segment, kernel, groups=kernel.size(0))
    
    xcorr_splitted = torch.split(xcorr, 195, dim=1)
    
    xcorr = torch.cat(xcorr_splitted, dim=0)
    
    return xcorr

if __name__ == "__main__":

    torch.cuda.init()
    seed_everything(5)
    out_path = 'trained_models' 
    audio_data_file = '../datasets/total_audio_dataset_ms_challenge.sw'
    feat_data_file_dont_care = '../datasets/total_features_lpcnet_with_burg_ms_challenge.f32'
    feat_data_file_actual = '../datasets/pstn16000_total_audio_dataset_ms_challenge.sw'
    voicing_flag_file = '../datasets/voicing_info_total_audio_dataset_ms_challenge.sw'

    model_fdr = 'models'  
    opt_fdr='optimizers'
    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 32

    sample_generator = AudioSampleGenerator(audio_data_file, feat_data_file_dont_care, feat_data_file_actual, voicing_flag_file, batch_size)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0001/2
    d_learning_rate = 0.0002#/2
    
    device = torch.device('cuda:1')
    generator = Generator()
    
    generator.to(device)

    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])
    
    msd = MultiScaleDiscriminator().to(device) 
    mpd = MultiPeriodDiscriminator().to(device)

    msd_optimizer = optim.AdamW(msd.parameters(), d_learning_rate, betas=[0.8, 0.99])
    mpd_optimizer = optim.AdamW(mpd.parameters(), d_learning_rate, betas=[0.8, 0.99])

    saved_model_path= 'trained_models/20220429_0137/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models/20220429_0137/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    '''saved_model_path= 'trained_models/20220407_0428/models/mpd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    mpd.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models/20220407_0428/optimizers/mpd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    mpd_optimizer.load_state_dict(saved_model_opt)
    for param_group in mpd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    saved_model_path= 'trained_models/20220407_0428/models/msd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    msd.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models/20220407_0428/optimizers/msd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    msd_optimizer.load_state_dict(saved_model_opt)
    for param_group in msd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    #generator = nn.DataParallel(generator)'''

    spect_loss =  MultiResolutionSTFTLoss(device).to(device)

    feature_extractor_g = LogMel().to(device)
   
    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')
    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):

            gc.collect()
            torch.cuda.empty_cache()
            
            wb_speech, voiced_flags, unvoiced_flags = fetch_data(sample_batch, batch_size, device)
           
            feat = feature_extractor_g(wb_speech)[:,:,:100]

            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            #mpd.zero_grad()
            #mpd_optimizer.zero_grad()

            #generate fake speech
            speech_generated = generator(feat.detach())

            ###################### Train D ######################

            #disc_mp_real, disc_mp_gen, _, _ = mpd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            #loss_mp_disc, loss_mp_disc_hinge = discriminator_loss(disc_mp_real, disc_mp_gen)

            disc_ms_real, disc_ms_gen, _, _ = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach(), feat.detach(), voiced_flags, unvoiced_flags)
            loss_ms_disc, loss_ms_disc_hinge = discriminator_loss(disc_ms_real, disc_ms_gen)

            total_d_loss = loss_ms_disc #(loss_mp_disc + loss_ms_disc)/2.0 #

            total_d_loss_hinge = loss_ms_disc_hinge #(loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0

            total_d_loss.backward()

            msd_optimizer.step()
            #mpd_optimizer.step()'''

            ###################### Train G ######################
            generator.zero_grad()
            g_optimizer.zero_grad()

            msd.zero_grad()
            msd_optimizer.zero_grad()

            #mpd.zero_grad()
            #mpd_optimizer.zero_grad()     

            speech_generated = generator(feat.detach())

            #disc_mp_real, disc_mp_gen, fm_mp_real, fm_mp_gen = mpd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1))
            #loss_mp_disc, loss_mp_disc_hinge = generator_loss(disc_mp_gen)

            disc_ms_real, disc_ms_gen, fm_ms_real, fm_ms_gen = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1), feat.detach(), voiced_flags, unvoiced_flags)  
            loss_ms_disc, loss_ms_disc_hinge = generator_loss(disc_ms_gen)   

            g_adv_loss = loss_ms_disc#(loss_mp_disc + loss_ms_disc)/2.0 #

            g_adv_loss_hinge = loss_ms_disc_hinge #(loss_ms_disc_hinge + loss_mp_disc_hinge)/2.0

            #fm_loss_mp = feature_loss(fm_mp_real, fm_mp_gen)
            #fm_loss_ms = feature_loss(fm_ms_real, fm_ms_gen)

            #g_fm_loss = fm_loss_ms #(fm_loss_mp + fm_loss_ms)/2.0'''

            correlation_loss = F.l1_loss(get_short_term_correlation_hr(speech_generated), get_short_term_correlation_hr(wb_speech.detach())) 
            tf_loss = spect_loss(speech_generated, wb_speech.detach())

            total_loss =  g_adv_loss +  tf_loss + 10.0*correlation_loss ##g_adv_loss + 15.0*tf_loss + 7.0*g_fm_loss #

            total_loss.backward()
            
            g_optimizer.step()

            if (i + 1) % 1 == 0:
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, recon_loss {recon_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, recon_loss {acf_recon_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}\n') 
                print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}, corr_loss {correlation_loss.item()}\n')  

            if ((epoch % 1 == 0) and (i % 100 == 0)):

                model_path = os.path.join(models_path, 'generator.pkl')
                torch.save(generator.state_dict(), model_path)


                model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                torch.save(g_optimizer.state_dict(), model_opt_path) 

                model_path = os.path.join(models_path, 'mpd.pkl')
                torch.save(mpd.state_dict(), model_path)


                model_opt_path = os.path.join(optimizer_path, 'mpd_opt.pkl')
                torch.save(mpd_optimizer.state_dict(), model_opt_path) 

                model_path = os.path.join(models_path, 'msd.pkl')
                torch.save(msd.state_dict(), model_path)


                model_opt_path = os.path.join(optimizer_path, 'msd_opt.pkl')
                torch.save(msd_optimizer.state_dict(), model_opt_path) 


    print('Finished Training!')

