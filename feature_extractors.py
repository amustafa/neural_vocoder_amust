# -*- coding: utf-8 -*-

# Copyright 2020 Tomoki Hayashi
#  MIT License (https://opensource.org/licenses/MIT)

"""Pseudo QMF modules."""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import scipy.optimize as optimize
from scipy.signal import kaiser
import torchaudio
from torchaudio.transforms import MelSpectrogram

class LogMel(nn.Module):
    def __init__(self, sample_rate= 16000, 
                       n_fft= 512,
                       win_length=320,
                       hop_length=160,
                       n_mels=80,
                       norm='slaney',
                       mel_scale='slaney'):
        
        super(LogMel, self).__init__()
        
        self.mel = MelSpectrogram(sample_rate=sample_rate,
                                  n_fft= n_fft,
                                  win_length= win_length,
                                  hop_length= hop_length,
                                  n_mels= n_mels,
                                  norm=norm,
                                  mel_scale=mel_scale)

    def forward(self, x):

        mel_feat = self.mel(x)
        log_mel = torch.log10(torch.clamp(mel_feat, min=1e-5))
        return log_mel


class LogMelD(nn.Module):
    def __init__(self, sample_rate= 16000, 
                       n_fft= 1024,
                       win_length=1024,
                       hop_length=160,
                       n_mels=80,
                       norm='slaney',
                       mel_scale='slaney'):
        
        super(LogMelD, self).__init__()
        
        self.mel = MelSpectrogram(sample_rate=sample_rate,
                                  n_fft= n_fft,
                                  win_length= win_length,
                                  hop_length= hop_length,
                                  n_mels= n_mels,
                                  norm=norm,
                                  mel_scale=mel_scale)

    def forward(self, x):

        mel_feat = self.mel(x)
        log_mel = torch.log10(1. + 100000. * mel_feat)
        return log_mel
