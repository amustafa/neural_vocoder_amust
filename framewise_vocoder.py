import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np


which_norm = weight_norm

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out

class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * (1.0 + torch.sigmoid(self.gate(x)))

        return out 

class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        self.gru.flatten_parameters()

        output, h0 = self.gru(x)
        
        return self.nl(output)

class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                GLU(self.fc_out_dim)
                               )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        if self.frame_kernel_size == 1:
            return self.fc(x)

        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
        
        out = self.fc(x_flat_padded_unfolded)
        return out 

class CondFramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=2, apply_act=True):
        
        super(CondFramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        
        if self.frame_kernel_size == 2:

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:
        
            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim

        if apply_act:
        
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False)),
                                    GLU(self.fc_out_dim)
                                   )

        else:
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim + self.frame_len, self.fc_out_dim, bias=False))
                                   )
        
        self.init_weights()
        
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, cond):


        x_flat = x.reshape(x.size(0),1,-1)
        x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
        
        x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                    kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()

        
        cond_x_flat_padded_unfolded = torch.cat([cond, x_flat_padded_unfolded], dim=2)
        out = self.fc(cond_x_flat_padded_unfolded)

        return out

class CondRNNFramewiseVocoderPerceptualDomainLPCNet(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pitch_embed = nn.Embedding(256,128)

        self.bfcc_with_corr_conv = CausalConv(19,128,kernel_size=9)
        
        self.feat_in_conv = CausalConv(256,320,kernel_size=9)
        self.feat_in_nl = nn.LeakyReLU(0.2)

        self.rnn0 = ForwardGRU(320,320)
        
        self.rnn1 = ForwardGRU(320,320)
             
        self.rnn2 = ForwardGRU(320,320)
        
        self.rnn3 = ForwardGRU(320,320)
                 
        self.rnn4 = ForwardGRU(320,320)
        
        self.fc = which_norm(nn.Linear(1920, 512, bias=False))

        self.lookahead_conv = FramewiseConv(512, 512)

        self.framewise_conv = nn.ModuleList([
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 512, frame_kernel_size=2),
                                             CondFramewiseConv(512, 160, frame_kernel_size=2, apply_act=False)]
                                            )
        self.out = nn.Tanh()

        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def ulaw2lin(self, x):

        return torch.sign(x) * ((256.0**torch.abs(x) - 1.0)/255.0)
                   
    def forward(self, pitch_period, bfcc_with_corr):

        p_embed = self.pitch_embed(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope_embed = self.bfcc_with_corr_conv(bfcc_with_corr.permute(0,2,1).contiguous())\
            .permute(0,2,1).contiguous().permute(0, 2, 1).contiguous()

        wav_latent = self.feat_in_nl(self.feat_in_conv(torch.cat((p_embed , envelope_embed), dim=1)).permute(0,2,1).contiguous())

        stage0_out = self.rnn0(wav_latent)
        
        stage1_out = self.rnn1(stage0_out)
        
        stage2_out = self.rnn2(stage1_out)
        
        stage3_out = self.rnn3(stage2_out)
        
        stage4_out = self.rnn4(stage3_out)
        
        res = torch.cat([wav_latent, stage0_out, stage1_out, stage2_out, stage3_out, stage4_out], dim=2)

        cond = self.fc(res)
        signal = self.lookahead_conv(cond)

        for layer in self.framewise_conv:

            signal = layer(signal, cond)

        final = self.out(signal)
                               
        waveform = final.reshape(final.size(0),1,-1).squeeze(1)

        return waveform