#!/bin/bash
for filename in input_lpcnet_folder/*; do

    xbase=${filename##*/};
    xpref=${xbase%.*}
    echo "Processing ${xbase}";

    ./lpcnet_demo-50Eft "-synthesis" ${filename} output_lpcnet_waveform_folder/${xpref}.sw;

done