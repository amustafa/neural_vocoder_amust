from audioop import bias
from pickle import NONE
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import random
from pqmf import *
import math as m
from adaptive_conv1d import *

which_norm = weight_norm
    
class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.nl(self.conv(x))#self.conv(x) #
        return conv_out

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        self.padding_left = (kernel_size//2 ) * dilation
        self.padding_right = (kernel_size//2 ) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding_left, self.padding_right))
        
        conv_out = self.nl(self.conv(x))
        return conv_out
    
class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        out = x * torch.sigmoid(self.gate(x))
    
        return out 
    
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1, act='glu', parallel=False):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.parallel = parallel

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        if act=='glu':
            self.nl = GLU(self.hidden_size)

        if act=='tanh':
            self.nl = nn.Tanh()
        
        self.h0 = None

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def one_forward_pass(self, one_frame):
        
        if self.h0 is None:

            output, self.h0 = self.gru(one_frame)

        else:

            output, self.h0 = self.gru(one_frame, self.h0)
        self.h0 = self.nl(output).permute(1,0,2).contiguous() 
        return self.nl(output) 
    
    def forward(self, x):

        if self.parallel:

            self.gru.flatten_parameters()
            output, h0 = self.gru(x)
            return self.nl(output)

        else:

            return self.one_forward_pass(x) 

    def reset(self):
        self.h0 = None
        return
    
class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True, parallel=False):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len
        self.parallel = parallel

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )

        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
                
        self.buffer = None
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                 
    def one_forward_pass(self, one_frame):
        
        if self.buffer is None:
            self.buffer = torch.zeros(one_frame.size(0),1,self.required_pad_left,device=one_frame.device)
            
        one_frame_flat = one_frame.reshape(one_frame.size(0),1,-1)
        one_frame_flat_padded = torch.cat((self.buffer, one_frame_flat), dim=-1)
        out_frame_new = self.fc(one_frame_flat_padded)
        self.buffer[:,:,:(self.frame_kernel_size - 2)*self.frame_len] = self.buffer[:,:,self.frame_len:].clone()
        self.buffer[:,:,(self.frame_kernel_size - 2)*self.frame_len:] = one_frame_flat
        return out_frame_new

    def forward(self, x):

        if self.parallel:
            x_flat = x.reshape(x.size(0),1,-1)
            x_flat_padded = F.pad(x_flat, (self.required_pad_left, self.required_pad_right)).unsqueeze(2)
            
            x_flat_padded_unfolded = F.unfold(x_flat_padded,\
                        kernel_size= (1,self.fc_input_dim), stride=self.frame_len).permute(0,2,1).contiguous()
            
            out = self.fc(x_flat_padded_unfolded)#self.activation(torch.matmul(x_flat_padded_unfolded, k.T))#
            return out  

        else:
            return self.one_forward_pass(x)
    
    def reset(self):
        self.buffer = None
        return
    
class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = nn.Linear(in_ch, out_ch * upsample_factor, bias=False)
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
                
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
        
def n(x):
    return torch.clamp(x + (1./127.)*(torch.rand_like(x)-.5), min=-1., max=1.)

class ScaleEmbedding(nn.Module):
    def __init__(self,
                 dim = 8,
                 min_val = 50,
                 max_val = 650,
                 logscale=True):

        super().__init__()

        if min_val >= max_val:
            raise ValueError('min_val must be smaller than max_val')

        if min_val <= 0 and logscale:
            raise ValueError('min_val must be positive when logscale is true')

        self.dim = dim
        self.logscale = logscale
        self.min_val = min_val
        self.max_val = max_val

        if logscale:
            self.min_val = m.log(self.min_val)
            self.max_val = m.log(self.max_val)


        self.offset = (self.min_val + self.max_val) / 2
        self.scale_factors = nn.Parameter(
            torch.arange(1, dim+1, dtype=torch.float32) * torch.pi / (self.max_val - self.min_val)
        )

    def forward(self, x):
        if self.logscale: x = torch.log(x)
        x = torch.clip(x, self.min_val, self.max_val) - self.offset
        return torch.sin(x.unsqueeze(-1) * self.scale_factors - 0.5).flatten(2)

class SilkEnhancerAR(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.num_bits_embedding = ScaleEmbedding()
   
        self.feat_in_conv1 = CausalConv(93+16,128,kernel_size=3) 

        self.bfcc_with_corr_upsampler = UpsampleFC(128,80,2) 
        
        self.feat_in_conv2 = ConvLookahead(80+80,80,kernel_size=5) 
        self.feat_in_nl = GLU(80)
        
        self.cond_gain_dense = which_norm(nn.Linear(80, 1, bias=False))

        self.fwc = FramewiseConv(160 + 80, 192, frame_kernel_size=3)
        
        self.rnn1 = ForwardGRU(160+192,160)
        self.rnn2 = ForwardGRU(160+160,128)
        self.rnn3 = ForwardGRU(160+128,128)
        

        self.skip_dense = nn.Sequential(which_norm(nn.Linear(80+192+160+128+128+160, 40, bias=False)), GLU(40))
        #self.out_layer = nn.Sequential(which_norm(nn.Linear(80, 40, bias=False)), nn.Tanh())
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 80 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 *torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 80 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def forward(self, pitch_period, silk_feats, num_bits, coded_signal):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous() 
        num_bits_embed = self.num_bits_embedding(num_bits)
        envelope_act = self.feat_in_conv1(torch.cat((silk_feats, num_bits_embed), dim=-1).permute(0,2,1).contiguous())
        envelope_up = self.bfcc_with_corr_upsampler(envelope_act)
        feat_in = torch.cat((p_embed , envelope_up), dim=1)
        feat_latent = self.feat_in_nl(self.feat_in_conv2(feat_in).permute(0,2,1).contiguous())

        gain = torch.exp(self.cond_gain_dense(feat_latent))
        
        period_repeat = torch.repeat_interleave(pitch_period, 2, dim=-1)
        
        x0 = torch.zeros_like(coded_signal[:,:320])
        batch_size = x0.size(0)
        device = x0.device
        # Pre-load the first 10 ms of x0 in the memory
        pitch_mem = x0
        ar_wav = x0[:, 320-40:320].unsqueeze(1)
        coded_pitch_mem = x0
        out_waveform = coded_signal[:,:320]
        

        # Start running the model on the last 10 ms of x0
        for i in range(0, feat_latent.size(1)):

            T = period_repeat[:, i:i+1]
            ind = 320 - T
            ind = ind + torch.arange(40, device=ind.device)
            mask = ind >= 320
            ind = ind - mask*T
            
            pred_wav = torch.gather(pitch_mem, dim=-1, index=ind).unsqueeze(1) 
            
            coded_wav = coded_signal[:,i*40:(i+1)*40].unsqueeze(1)
            coded_pred_wav = torch.gather(coded_pitch_mem, dim=-1, index=ind).unsqueeze(1) 

            pred_wav = n(pred_wav/(1e-12 + gain[:,i:i+1,:]))
            ar_wav =   n(ar_wav/(1e-12 + gain[:,i:i+1,:]))
            
            coded_wav_scaled = n(coded_wav/(1e-12 + gain[:,i:i+1,:]))
            coded_pred_wav_scaled = n(coded_pred_wav/(1e-12 + gain[:,i:i+1,:]))
   
            phase_sig = torch.cat((pred_wav, ar_wav, coded_pred_wav_scaled, coded_wav_scaled), dim=-1)
    
            fwc_out = self.fwc(torch.cat((feat_latent[:,i:i+1,:], phase_sig), dim=-1))
            
            rnn1_out = self.rnn1(torch.cat((fwc_out, phase_sig), dim=-1))
            rnn2_out = self.rnn2(torch.cat((rnn1_out, phase_sig), dim=-1))
            rnn3_out = self.rnn3(torch.cat((rnn2_out, phase_sig), dim=-1))
            
            skip = torch.cat((feat_latent[:,i:i+1,:], fwc_out, rnn1_out, rnn2_out, rnn3_out, phase_sig), dim=-1)
            out = self.skip_dense(skip)#self.out_layer(self.skip_dense(skip))

            if i<8:

                out_frame = coded_wav.squeeze(1)

            else:

                out_frame = out.squeeze(1) * gain[:,i:i+1,:].squeeze(1)
                out_waveform = torch.cat((out_waveform, out_frame), dim=-1)

            ar_wav = out_frame.unsqueeze(1)
            pitch_mem = torch.cat([pitch_mem[:,40:], out_frame], dim=-1)
            coded_pitch_mem = torch.cat([coded_pitch_mem[:,40:], coded_wav.squeeze(1)], dim=-1)

        self.fwc.reset()
        self.rnn1.reset()
        self.rnn2.reset()
        self.rnn3.reset()
        
        return  out_waveform
    
class SilkEnhancerAROLA(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.num_bits_embedding = ScaleEmbedding()
   
        self.feat_in_conv1 = CausalConv(93+16,128,kernel_size=3) 

        self.bfcc_with_corr_upsampler = UpsampleFC(128,80,2) 
        
        self.feat_in_conv2 = ConvLookahead(80+80,80,kernel_size=5) 
        self.feat_in_nl = GLU(80)
        
        self.cond_gain_dense = which_norm(nn.Linear(80, 1, bias=False))

        self.fwc = FramewiseConv(160 + 80, 192, frame_kernel_size=3)
        
        self.rnn1 = ForwardGRU(160+192,160)
        self.rnn2 = ForwardGRU(160+160,128)
        self.rnn3 = ForwardGRU(160+128,128)
        

        self.skip_dense = nn.Sequential(which_norm(nn.Linear(80+192+160+128+128+160, 128, bias=False)), GLU(128))
        #self.out_layer = nn.Sequential(which_norm(nn.Linear(80, 40, bias=False)), nn.Tanh())
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 80 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 *torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,40)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,40)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 80 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def forward(self, pitch_period, silk_feats, num_bits, coded_signal, window):

        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous() 
        num_bits_embed = self.num_bits_embedding(num_bits)
        envelope_act = self.feat_in_conv1(torch.cat((silk_feats, num_bits_embed), dim=-1).permute(0,2,1).contiguous())
        envelope_up = self.bfcc_with_corr_upsampler(envelope_act)
        feat_in = torch.cat((p_embed , envelope_up), dim=1)
        feat_latent = self.feat_in_nl(self.feat_in_conv2(feat_in).permute(0,2,1).contiguous())

        gain = torch.exp(self.cond_gain_dense(feat_latent))
        
        period_repeat = torch.repeat_interleave(pitch_period, 2, dim=-1)
        
        x0 = torch.zeros_like(coded_signal[:,:320])
        batch_size = x0.size(0)
        device = x0.device
        # Pre-load the first 10 ms of x0 in the memory
        pitch_mem = x0
        ar_wav = x0[:, 320-40:320].unsqueeze(1)
        coded_pitch_mem = x0
        out_waveform = coded_signal[:,:320]
        prev_win = torch.zeros_like(out_waveform[:,320-128:]) * window
        
        # Start running the model on the last 10 ms of x0
        for i in range(0, feat_latent.size(1)):

            T = period_repeat[:, i:i+1]
            ind = 320 - T
            ind = ind + torch.arange(40, device=ind.device)
            mask = ind >= 320
            ind = ind - mask*T
            
            pred_wav = torch.gather(pitch_mem, dim=-1, index=ind).unsqueeze(1) 
            
            coded_wav = coded_signal[:,i*40:(i+1)*40].unsqueeze(1)
            coded_pred_wav = torch.gather(coded_pitch_mem, dim=-1, index=ind).unsqueeze(1) 

            pred_wav = n(pred_wav/(1e-12 + gain[:,i:i+1,:]))
            ar_wav =   n(ar_wav/(1e-12 + gain[:,i:i+1,:]))
            
            coded_wav_scaled = n(coded_wav/(1e-12 + gain[:,i:i+1,:]))
            coded_pred_wav_scaled = n(coded_pred_wav/(1e-12 + gain[:,i:i+1,:]))
   
            phase_sig = torch.cat((pred_wav, ar_wav, coded_pred_wav_scaled, coded_wav_scaled), dim=-1)
    
            fwc_out = self.fwc(torch.cat((feat_latent[:,i:i+1,:], phase_sig), dim=-1))
            
            rnn1_out = self.rnn1(torch.cat((fwc_out, phase_sig), dim=-1))
            rnn2_out = self.rnn2(torch.cat((rnn1_out, phase_sig), dim=-1))
            rnn3_out = self.rnn3(torch.cat((rnn2_out, phase_sig), dim=-1))
            
            skip = torch.cat((feat_latent[:,i:i+1,:], fwc_out, rnn1_out, rnn2_out, rnn3_out, phase_sig), dim=-1)
            out = self.skip_dense(skip)#self.out_layer(self.skip_dense(skip))

            if i<8:

                out_frame = coded_wav.squeeze(1)

            else:

                next_win = out.squeeze(1) * gain[:,i:i+1,:].squeeze(1) * window
                out_frame = ((next_win[:, 40:80] + prev_win[:,80:120]) + (pred_wav.squeeze(1) * gain[:,i:i+1,:].squeeze(1)))/2.0
                out_waveform = torch.cat((out_waveform, out_frame), dim=-1)
                prev_win = next_win

            ar_wav = out_frame.unsqueeze(1)
            pitch_mem = torch.cat([pitch_mem[:,40:], out_frame], dim=-1)
            coded_pitch_mem = torch.cat([coded_pitch_mem[:,40:], coded_wav.squeeze(1)], dim=-1)

        self.fwc.reset()
        self.rnn1.reset()
        self.rnn2.reset()
        self.rnn3.reset()
        
        return  out_waveform