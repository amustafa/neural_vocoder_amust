
import os
import time
import numpy as np
import soundfile as sf
import amfm_decompy.pYAAPT as pYAAPT
import amfm_decompy.basic_tools as basic


def get_voicing_info(x, sr=16000):

    signal = basic.SignalObj(x, sr)
    pitch = pYAAPT.yaapt(signal, **{'frame_length' : 20.0, 'tda_frame_length' : 20.0})

    pitch_values = pitch.samp_values
    voiced_flags = pitch.vuv.astype('float')

    return pitch_values, voiced_flags

    
audio_dataset = os.listdir("./original/")

lpcnet3g_pitch, lpcnet3g_voiced =  list(), list() #"./lpcnet3g"
lpcnet1p2g_pitch, lpcnet1p2g_voiced = list(), list() #"./lpcnet1p2g"
fwgan1p2g_pitch, fwgan1p2g_voiced = list(), list() #"./fwgan1p2g"
fwgan1p5g_pitch, fwgan1p5g_voiced = list(), list() #"./fwgan1p5g"
fwgan3g_pitch, fwgan3g_voiced = list(), list() #"./fwgan3g"

for sig_name in audio_dataset:

    print("Processing {} ...".format(sig_name))

    x_original, sr = sf.read("./original/{}".format(sig_name),dtype='float32') 
    x_lpcnet3g, sr = sf.read("./lpcnet3g/{}".format(sig_name),dtype='float32') 
    x_lpcnet1p2g, sr = sf.read("./lpcnet1p2g/{}".format(sig_name),dtype='float32') 
    x_fwgan1p2g, sr = sf.read("./fwgan1p2g/{}".format(sig_name),dtype='float32') 
    x_fwgan1p5g, sr = sf.read("./fwgan1p5g/{}".format(sig_name),dtype='float32') 
    x_fwgan3g, sr = sf.read("./fwgan3g/{}".format(sig_name),dtype='float32') 

    wave_length = min([len(x_original), len(x_lpcnet3g), len(x_lpcnet1p2g), len(x_fwgan1p2g), len(x_fwgan1p5g), len(x_fwgan3g)])

    x_original = x_original[:wave_length]
    x_lpcnet3g = x_lpcnet3g[:wave_length]
    x_lpcnet1p2g = x_lpcnet1p2g[:wave_length]
    x_fwgan1p2g = x_fwgan1p2g[:wave_length]
    x_fwgan1p5g = x_fwgan1p5g[:wave_length]
    x_fwgan3g = x_fwgan3g[:wave_length]

    pitch_contour_orig, voicing_original = get_voicing_info(x_original)

    pitch_contour_lpcnet3g, voicing_lpcnet3g = get_voicing_info(x_lpcnet3g)
    pitch_contour_lpcnet1p2g, voicing_lpcnet1p2g = get_voicing_info(x_lpcnet1p2g)
    pitch_contour_fwgan1p2g, voicing_fwgan1p2g = get_voicing_info(x_fwgan1p2g)
    pitch_contour_fwgan1p5g, voicing_fwgan1p5g = get_voicing_info(x_fwgan1p5g)
    pitch_contour_fwgan3g, voicing_fwgan3g = get_voicing_info(x_fwgan3g)

    lpcnet3g_pitch.append(np.mean(np.abs(pitch_contour_orig - pitch_contour_lpcnet3g)))
    lpcnet3g_voiced.append(np.sum(np.abs(voicing_original - voicing_lpcnet3g)) / len(voicing_original))

    lpcnet1p2g_pitch.append(np.mean(np.abs(pitch_contour_orig - pitch_contour_lpcnet1p2g)))
    lpcnet1p2g_voiced.append(np.sum(np.abs(voicing_original - voicing_lpcnet1p2g)) / len(voicing_original))

    fwgan1p2g_pitch.append(np.mean(np.abs(pitch_contour_orig - pitch_contour_fwgan1p2g)))
    fwgan1p2g_voiced.append(np.sum(np.abs(voicing_original - voicing_fwgan1p2g)) / len(voicing_original))

    fwgan1p5g_pitch.append(np.mean(np.abs(pitch_contour_orig - pitch_contour_fwgan1p5g)))
    fwgan1p5g_voiced.append(np.sum(np.abs(voicing_original - voicing_fwgan1p5g)) / len(voicing_original))

    fwgan3g_pitch.append(np.mean(np.abs(pitch_contour_orig - pitch_contour_fwgan3g)))
    fwgan3g_voiced.append(np.sum(np.abs(voicing_original - voicing_fwgan3g)) / len(voicing_original))


print(f"model lpcnet3g: PMAE= {np.mean(lpcnet3g_pitch)}, VDE= {np.mean(lpcnet3g_voiced)}")
print(f"model lpcnet1p2g: PMAE= {np.mean(lpcnet1p2g_pitch)}, VDE= {np.mean(lpcnet1p2g_voiced)}")
print(f"model fwgan1p2g: PMAE= {np.mean(fwgan1p2g_pitch)}, VDE= {np.mean(fwgan1p2g_voiced)}")
print(f"model fwgan1p5g: PMAE= {np.mean(fwgan1p5g_pitch)}, VDE= {np.mean(fwgan1p5g_voiced)}")
print(f"model fwgan3g: PMAE= {np.mean(fwgan3g_pitch)}, VDE= {np.mean(fwgan3g_voiced)}")