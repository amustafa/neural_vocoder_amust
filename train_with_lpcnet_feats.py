import time
import torch
import os
import gc
#from gen_models import *
from gen_models_ar import *
from disc_models import *
from fd_discriminator_jan import *
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator_lpcnet import AudioSampleGenerator
import soundfile as sf
import random
from feature_extractors import *
#from stft_loss import *
from stft_loss_phase import *
#from stft_loss_jan import MRSTFTLoss, MRLogMelLoss

def fetch_data(batch, batch_size, device):
    
    wb_speech = batch['audio'].type(torch.FloatTensor).view(batch_size,8000).to(device)
    #lpc_feat = batch['lpc_feat'].type(torch.FloatTensor).to(device)
    pw_speech = batch['pw_audio'].type(torch.FloatTensor).view(batch_size,8000).to(device)
    periods = batch['periods'].type(torch.long).view(batch_size,50).to(device)
    bfcc_with_corr = batch['bfcc_with_corr'].type(torch.FloatTensor).to(device)

    return wb_speech, pw_speech, periods, bfcc_with_corr
    #return wb_speech, pw_speech, vuv_pulse_train, bfcc_with_corr

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True

def feature_loss(fmap_r, fmap_g, detach_first=False):
    loss = 0
    i = 0
    if detach_first:

        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                loss += torch.norm(rl.detach() - gl, p=1) / (torch.norm(rl.detach(), p=1))
                #loss += torch.norm(rl.detach() - gl, p="fro") / (torch.norm(rl.detach(), p="fro")) #torch.mean(torch.abs(rl.detach() - gl))
                i = i+1

    else:
        for dr, dg in zip(fmap_r, fmap_g):
            for rl, gl in zip(dr, dg):
                #loss += torch.norm(rl - gl, p="fro") / (torch.norm(rl, p="fro")) #torch.mean(torch.abs(rl - gl))
                loss += torch.norm(rl - gl, p=1) / (torch.norm(rl, p=1))
                i = i+1

    return loss/i


def discriminator_loss(disc_real_outputs, disc_generated_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dr, dg in zip(disc_real_outputs, disc_generated_outputs):
        r_loss =  torch.mean((1-dr)**2) #(torch.nn.ReLU()(1.0 - dr)).mean() #
        g_loss =  torch.mean(dg**2) #(torch.nn.ReLU()(1.0 + dg)).mean() #
        loss += (r_loss + g_loss)
        hing_loss += ((torch.nn.ReLU()(1.0 - dr)).mean() + (torch.nn.ReLU()(1.0 + dg)).mean()).item() #(torch.mean((1-dr)**2) + torch.mean(dg**2)).item()
        i = i+1
    return loss/i, (hing_loss/i)


def generator_loss(disc_outputs):
    loss = 0
    i = 0
    hing_loss = 0
    for dg in disc_outputs:
        l = torch.mean((1-dg)**2) #-1.0 * dg.mean() #
        loss += l
        hing_loss += (-1.0 * dg.mean()).item()
        i = i+1

    return loss/i, (hing_loss/i)


def correlogram(x, frame_size=160, search_size=320):

    temp_dim = x.size(1)
    
    kernel = torch.cat([x[:,i:i+frame_size].clone().view(-1, 1, frame_size)\
                        for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
    
    kernel_splitted = torch.split(kernel, 1, dim=0)
    
    kernel = torch.cat(kernel_splitted, dim=1).permute(1,0,2)
    
    segment = torch.cat([x[:,i:i+frame_size+search_size].clone().view(-1, 1, frame_size + search_size) \
                        for i in range(0, temp_dim - frame_size - search_size, frame_size)], dim=1)
    
    num_frames = segment.size(1)
    
    segment_splitted = torch.split(segment, 1, dim=0)
    
    segment = torch.cat(segment_splitted, dim=1)
    
    xcorr = F.conv1d(segment, kernel, groups=kernel.size(0))
    
    xcorr_splitted = torch.split(xcorr, num_frames, dim=1)
    
    xcorr = torch.cat(xcorr_splitted, dim=0)
    
    return xcorr.permute(0, 2, 1).contiguous()


def phase_difference(x, n_fft=320, hop_length=160, win_length=320):
    
    #x = F.pad(x, (int((n_fft - hop_length) / 2), int((n_fft - hop_length) / 2)), mode='reflect')
    window = getattr(torch, 'hann_window')(win_length).to(x.device)
    
    with autocast(enabled=False):

        phase = torch.sgn(torch.stft(x.float(), n_fft=n_fft, hop_length=hop_length, win_length=win_length,\
                          window=window, return_complex=True))

        phase_diff_time = phase[:,:,:-1] * torch.conj(phase)[:,:,1:]
      
    return phase_diff_time

def delta_delta(phase_diff):

    delta_delta = phase_diff[:,:,1:] * torch.conj(phase_diff)[:,:,:-1]

    return torch.abs(torch.angle(delta_delta))


def phase_difference_loss(phase_diff_gen, phase_diff_orig):

    #Ls = [2]

    phase_diff_mag = torch.abs(phase_diff_gen - phase_diff_orig) #/ 2.0 
    total_loss = F.l1_loss(phase_diff_mag, torch.zeros_like(phase_diff_mag).to(device))  
    '''for l in Ls:

        pd_avg_gen = torch.complex(F.avg_pool1d(phase_diff_gen.real, kernel_size=l, stride=1), F.avg_pool1d(phase_diff_gen.imag, kernel_size=l, stride=1)) 
        pd_avg_orig = torch.complex(F.avg_pool1d(phase_diff_orig.real, kernel_size=l, stride=1), F.avg_pool1d(phase_diff_orig.imag, kernel_size=l, stride=1)) 

        phase_diff_mag = torch.abs(pd_avg_gen - pd_avg_orig)/2.0

        total_loss = total_loss + F.l1_loss(phase_diff_mag, torch.zeros_like(phase_diff_mag).to(device))  '''
       
    return total_loss

def short_term_energy(signal):

    signal = signal.unsqueeze(1).unsqueeze(2)

    signal_frames = F.unfold(signal, kernel_size= (1,160), stride=80).permute(0,2,1).contiguous()

    signal_energy = torch.sum(signal_frames**2, dim=-1)

    return torch.log(signal_energy + 1e-9)

def ccc_loss(x,y):

    x = x.reshape(x.size(0),-1,320).permute(0,2,1).contiguous()
    y = y.reshape(y.size(0),-1,320).permute(0,2,1).contiguous()

    mean_x = torch.mean(x, dim=1, keepdim=True)
    mean_y = torch.mean(y, dim=1, keepdim=True)


    var_x = torch.var(x, dim=1, keepdim=True)
    var_y = torch.var(y, dim=1, keepdim=True)

    std_x = torch.std(x, dim=1, keepdim=True)
    std_y = torch.std(y, dim=1, keepdim=True)

    x_minus_mean = x - mean_x
    y_minus_mean = y - mean_y

    pearson_corr = torch.sum(x_minus_mean * y_minus_mean, dim=1, keepdim=True) / \
                (torch.sqrt(torch.sum(x_minus_mean ** 2, dim=1, keepdim=True) + 1e-7) * \
                torch.sqrt(torch.sum(y_minus_mean ** 2, dim=1, keepdim=True) + 1e-7))

    numerator = 2.0 * pearson_corr * std_x * std_y
    denominator = var_x + var_y + (mean_y - mean_x)**2

    ccc = numerator/denominator

    loss = F.l1_loss(1.0 - ccc, torch.zeros_like(ccc))

    return loss

def _remove_weight_norm(m):
    try:
        torch.nn.utils.remove_weight_norm(m)
    except ValueError:  # this module didn't have weight norm
        return

if __name__ == "__main__":

    torch.cuda.init()
    seed_everything(5)
    out_path = 'trained_models_distill_np' 
    audio_data_file =  '../datasets/data_npgan1.s16' #'../datasets/data_gan2.s16' #
    feat_data_file_actual = '../datasets/features_npgan1.f32'  # '../datasets/features_gan2.f32' #
    feat_data_file_dont_care =  '../datasets/data_npgan1.s16' #'../datasets/data_gan2.s16' #
    voicing_flag_file =  '../datasets/data_npgan1.s16' #'../datasets/data_gan2.s16' #

    model_fdr = 'models'  
    opt_fdr='optimizers'
    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 1024
    sample_generator = AudioSampleGenerator(audio_data_file, feat_data_file_actual, feat_data_file_dont_care, voicing_flag_file, batch_size)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0005 #/2
    d_learning_rate = 0.0002 #/2
    
    device = torch.device('cuda:6')
    generator = PSARGAN600M400Hz() # IdentityModel() # FARGAN() # PSARGAN600M400Hz() #  #  #PSARGAN1G() #PSARGAN() #PSARGAN1G() # 
    generator.to(device)
    #window = getattr(torch, 'hann_window')(160).to(device)

    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])
    lr_decay = 1e-4
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=g_optimizer, lr_lambda=lambda x : 1 / (1 + lr_decay * x))

    msd = FWGAN_disc_wrapper(TFDMultiResolutionDiscriminator(architecture='free', design='f_down', freq_roi=[0, 7400], max_channels=256, noise_gain=0.0)).to(device) 
    
    #msd = MelGANDiscriminator().to(device) #BigTFDMultiResolutionDiscriminator().to(device) # 

    msd_optimizer = optim.AdamW(msd.parameters(), d_learning_rate, betas=[0.8, 0.99])

    '''saved_model_path= 'trained_models_distill_np/20240101_0356/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models_distill_np/20240101_0356/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    saved_model_path= 'trained_models_distill_np/20231212_0857/models/msd.pkl'
    saved_model_generator= torch.load(saved_model_path)
    msd.load_state_dict(saved_model_generator)
 
    #generator = nn.DataParallel(generator)

    saved_opt_path= 'trained_models_distill_np/20231212_0857/optimizers/msd_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    msd_optimizer.load_state_dict(saved_model_opt)
    for param_group in msd_optimizer.param_groups:
        param_group['lr'] = d_learning_rate
        #param_group['betas'] = betas
        #param_group['weight_decay'] = g_wight_decay

    #generator = nn.DataParallel(generator)'''

    spect_loss = MultiResolutionSTFTLoss(device).to(device)# MRSTFTLoss().to(device)#
    #mel_loss =  MRLogMelLoss().to(device)

    feature_extractor_g = LogMel().to(device)
   
    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')

    
    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):
            
            wb_speech, pw_speech, periods, bfcc_with_corr = fetch_data(sample_batch, batch_size, device)

            x0 = wb_speech[:,:320]
  
            generator.zero_grad()
            g_optimizer.zero_grad()

            #msd.zero_grad()
            #msd_optimizer.zero_grad()

            #generate fake speech

            speech_generated = generator(periods.detach(), bfcc_with_corr.detach(), x0.detach()) #generator(wb_speech) #

            ###################### Train D ######################

            '''disc_ms_real, disc_ms_gen, _, _ = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1).detach())
            loss_ms_disc, loss_ms_disc_hinge = discriminator_loss(disc_ms_real, disc_ms_gen)

            total_d_loss = loss_ms_disc

            total_d_loss_hinge = loss_ms_disc_hinge 

            total_d_loss.backward()

            msd_optimizer.step()
      
            ###################### Train G ######################

            msd.zero_grad()
            msd_optimizer.zero_grad()

            ########################### adv training without distillation ###########################
            
            disc_ms_real, disc_ms_gen, fm_ms_real, fm_ms_gen = msd(wb_speech.unsqueeze(1), speech_generated.unsqueeze(1))  
            loss_ms_disc, loss_ms_disc_hinge = generator_loss(disc_ms_gen)   

            g_adv_loss = loss_ms_disc

            g_adv_loss_hinge = loss_ms_disc_hinge 

            fm_loss_ms = feature_loss(fm_ms_real, fm_ms_gen)
            g_fm_loss = fm_loss_ms #'''

            tf_loss = spect_loss(speech_generated, wb_speech.detach()) 
                        
            cos_dist_loss =  torch.mean(1.0 - F.cosine_similarity(speech_generated[:,320:480], wb_speech[:,320:480].detach()))
            total_loss = tf_loss + 0.01*cos_dist_loss #+ 0.01*cos_dist_loss  #g_adv_loss + 10.0*(tf_loss + 0.01*cos_dist_loss) + 1.0*g_fm_loss   ###
          
            #with torch.autograd.set_detect_anomaly(True):
            total_loss.backward()
            
            g_optimizer.step()
            scheduler.step()

            if (i + 1) % 1 == 0:
                print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, cos_dist {cos_dist_loss.item()}\n') 
                #print(f'Epoch {epoch + 1}, Step {i + 1}, d_loss {total_d_loss_hinge}, g_adv_loss {g_adv_loss_hinge},  spect_loss {tf_loss.item()}, cos_dist {cos_dist_loss.item()}\n')
                
            if ((epoch % 1 == 0) and (i % 100 == 0)):

                model_path = os.path.join(models_path, 'generator.pkl')
                torch.save(generator.state_dict(), model_path)

                model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                torch.save(g_optimizer.state_dict(), model_opt_path) 

                model_path = os.path.join(models_path, 'msd.pkl')
                torch.save(msd.state_dict(), model_path)

                model_opt_path = os.path.join(optimizer_path, 'msd_opt.pkl')
                torch.save(msd_optimizer.state_dict(), model_opt_path) 


    print('Finished Training!')

