
import torch
import struct
import sys
import torch.nn as nn
import torch.nn.functional as F
from torch.utils import data
import numpy as np
import os
import math
from torch.utils.data import DataLoader
import amfm_decompy.pYAAPT as pYAAPT
import amfm_decompy.basic_tools as basic
from scipy import signal as si


def get_voicing_info(x, sr=16000, upsample_rate=160):

    signal = basic.SignalObj(x, sr)
    pitch = pYAAPT.yaapt(signal, **{'frame_length' : 20.0, 'tda_frame_length' : 20.0} )

    voiced_flags = np.repeat(pitch.vuv.astype('int'), upsample_rate)
    unvoiced_flags = np.repeat(np.logical_not(pitch.vuv).astype('int'), upsample_rate)
    
    return voiced_flags, unvoiced_flags

def ulaw2lin(u):
    
    s = np.sign(u)
    u = np.abs(u)
    return s* (1 / 255)*((1 + 255)**u - 1) 

def lin2ulaw(x):
    #x = x*scale
    s = np.sign(x)
    x = np.abs(x)
    u = s*(np.log(1+255*x)/math.log(1 + 255))
    return u.astype('float32')

def deemphasis(x, coef= -0.85):
    
    return si.lfilter(np.array([1.0]), np.array([1.0, coef]), x).astype('float32')

def preemphasis(x, coef= -0.85):
    
    return si.lfilter(np.array([1.0, coef]), np.array([1.0]), x).astype('float32')
    
def lpc_analysis_one_frame(frame, filt, buffer, weighting_vector=np.ones(16)):
    
    out = np.zeros_like(frame)
    
    filt = np.flip(filt)
    
    inp = np.concatenate((buffer, frame))
    j = 0
    
    for i in range(16, inp.shape[0]):
        
        out[j] = inp[i] + np.dot(inp[i-16:i]*weighting_vector, filt)
        
        j = j+1
    
    return out


def lpc_analysis (signal, filters):
    
    residual = np.zeros_like(signal)
    buffer = np.zeros(16)
    num_frames = signal.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = signal[frame_idx*160: (frame_idx+1)*160][:]
        out_res_frame = lpc_analysis_one_frame(in_frame, filters[frame_idx, :], buffer)
        residual[frame_idx*160: (frame_idx+1)*160] = out_res_frame[:]
        buffer[:] = in_frame[-16:]
    
    return residual 


def perceptual_weighting (signal, filters, weighting_vector):
    
    weighted_residual = np.zeros_like(signal)
    buffer = np.zeros(16)
    num_frames = signal.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = signal[frame_idx*160: (frame_idx+1)*160][:]
        out_wres_frame = lpc_analysis_one_frame(in_frame, filters[frame_idx, :], buffer, weighting_vector)
        weighted_residual[frame_idx*160: (frame_idx+1)*160] = out_wres_frame[:]
        buffer[:] = in_frame[-16:]
    
    #perceptual weighting= W(z/gamma) * H_deemph
    pw_signal = deemphasis(weighted_residual) 
    
    return pw_signal 


def lpc_synthesis_one_frame(frame, filt, buffer, weighting_vector=np.ones(16)):
    
    out = np.zeros_like(frame)
    
    filt = np.flip(filt)
    
    inp = frame[:]
    
    
    for i in range(0, inp.shape[0]):
        
        s = inp[i] - np.dot(buffer*weighting_vector, filt)
        
        buffer[0] = s
        
        buffer = np.roll(buffer, -1)
        
        out[i] = s
        
    return out


def lpc_synthesis (residual, filters):
    
    signal = np.zeros_like(residual)
    buffer = np.zeros(16)
    num_frames = residual.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = residual[frame_idx*160: (frame_idx+1)*160][:]
        out_sig_frame = lpc_synthesis_one_frame(in_frame, filters[frame_idx, :], buffer)
        signal[frame_idx*160: (frame_idx+1)*160] = out_sig_frame[:]
        buffer[:] = out_sig_frame[-16:]
    
    return signal 


def inverse_perceptual_weighting (pw_signal, filters, weighting_vector):
    
    #inverse perceptual weighting= H_preemph / W(z/gamma)
    
    pw_signal = preemphasis(pw_signal) 
    
    signal = np.zeros_like(pw_signal)
    buffer = np.zeros(16)
    num_frames = pw_signal.shape[0] //160
    assert num_frames == filters.shape[0]
    
    for frame_idx in range(0, num_frames):
        
        in_frame = pw_signal[frame_idx*160: (frame_idx+1)*160][:]
        out_sig_frame = lpc_synthesis_one_frame(in_frame, filters[frame_idx, :], buffer, weighting_vector)
        signal[frame_idx*160: (frame_idx+1)*160] = out_sig_frame[:]
        buffer[:] = out_sig_frame[-16:]
    
    return signal


class AudioSampleGenerator(data.Dataset):
    """
    Audio sample reader.
    Used alongside with DataLoader class to generate batches.
    see: http://pytorch.org/docs/master/data.html#torch.utils.data.Dataset
    """
    def __init__(self, audio_file_path, feat_file_path, nb_audio_file_path, voicing_flag_file_path, batch_size):

        if not os.path.exists(audio_file_path):
            raise Error('The audio data file  does not exist!')
        else:
            self.audio_file_path = audio_file_path

        if not os.path.exists(feat_file_path):
            raise Error('The feat feature file  does not exist!')

        else:
            self.feat_file_path = feat_file_path

        if not os.path.exists(nb_audio_file_path):
            raise Error('The narrowband audio file  does not exist!')

        else:

            self.nb_audio_file_path = nb_audio_file_path

        if not os.path.exists(voicing_flag_file_path):
            raise Error('The voicing flag file  does not exist!')

        else:

            self.voicing_flag_file_path = voicing_flag_file_path
        
        self.seg_len = 100
        self.frame_len = 160
        self.frame_len_nb = 160
        self.nb_features = 72 #36
        self.batch_size = batch_size
        gamma = 0.92
        self.perceptual_weighting_vector = np.array([gamma**i for i in range(16,0,-1)])
        
        print(f'Start loading dataset files: {self.audio_file_path} & {self.feat_file_path} & {self.nb_audio_file_path}  ... ')

        self.feat = np.memmap(self.feat_file_path, dtype='float32', mode='r') 
        self.audio = np.memmap(self.audio_file_path, dtype=np.short, mode='r') 

        self.num_frames = len(self.feat) // self.nb_features
        self.feat = np.reshape(self.feat, (self.num_frames, self.nb_features))
        #self.feat = self.feat[:, -36:]   
       
        self.audio = self.audio[: self.num_frames * self.frame_len]

        print(f'Done! ... number of frames: {self.num_frames}')
        
    def __getitem__(self, idx):
        # get item for specified index
                      
        lc_index = np.random.randint(0, self.num_frames - self.seg_len)
        '''feat_sample = np.copy(self.feat[lc_index : lc_index + self.seg_len, :])
        bfcc = feat_sample[:, :18] 
        period = (0.1 + 50 * feat_sample[:, 18:19]  + 100).astype('int32')
        corr = feat_sample[:, 19:20] + 0.5
        lpc_filter_sample = feat_sample[:, -16:]   
        bfcc_with_corr = np.hstack((bfcc, corr))'''
  
        audio_sample = np.copy(self.audio[lc_index * self.frame_len : (lc_index + self.seg_len) * self.frame_len]/32768.0)
        audio_sample = preemphasis(audio_sample)
        
        #perceptual_weighted_sample = perceptual_weighting(audio_sample, lpc_filter_sample, self.perceptual_weighting_vector)

        #return {'audio': torch.from_numpy(audio_sample), 'periods':torch.from_numpy(period), 'bfcc_with_corr': torch.from_numpy(bfcc_with_corr),\
        #        'pw_audio': torch.from_numpy(perceptual_weighted_sample)}

        return {'audio': torch.from_numpy(audio_sample)}

    def __len__(self):
        return self.num_frames // self.seg_len 


