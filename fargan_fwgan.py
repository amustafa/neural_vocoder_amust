
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np


which_norm = weight_norm
kernel_size = 9

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, externel_padding= None):

        #if externel_padding is not None:
        #    x = torch.cat((externel_padding, x), dim=-1)
        #else:
        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        #just 10 look-ahead frames
       
        self.padding_left = (kernel_size - 5) * dilation
        self.padding_right = 4 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, init_state=0):


        x = F.pad(x,(self.padding_left, self.padding_right))
        
        conv_out = self.nl(self.conv(x))
        return conv_out
    
class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        out = x * torch.sigmoid(self.gate(x)) 
    
        return out

class FWConv(nn.Module):
    def __init__(self, in_size, out_size, kernel_size=3):
        super(FWConv, self).__init__()

        torch.manual_seed(5)

        self.in_size = in_size
        self.kernel_size = kernel_size
        self.conv = which_norm(nn.Linear(in_size*self.kernel_size, out_size, bias=False))
        self.glu = GLU(out_size)

        self.init_weights()

    def init_weights(self):

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, state):
        xcat = torch.cat((state, x), -1)
        #print(x.shape, state.shape, xcat.shape, self.in_size, self.kernel_size)
        out = self.glu(torch.tanh(self.conv(xcat)))
        return out, xcat[:,:,self.in_size:]

class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size


        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)
        
        self.h0 = None

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def one_forward_pass(self, one_frame):
        
        if self.h0 is None:

            output, self.h0 = self.gru(one_frame)

        else:

            output, self.h0 = self.gru(one_frame, self.h0)

        return self.nl(output) 
    
    def forward(self, x_frame):
        
        return self.one_forward_pass(x_frame) #out 

    def reset(self):
        self.h0 = None
        return
    
class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

            #self.cont_fc = nn.Sequential(which_norm(nn.Linear(64, self.required_pad_left, bias=False)),
            #                        nn.Tanh())

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )
        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
        
        self.buffer = None
        
        self.init_weights()
                
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def one_forward_pass(self, one_frame):
        
        if self.buffer is None:
            self.buffer = torch.zeros(one_frame.size(0),1,self.required_pad_left,device=one_frame.device)#self.cont_fc(x0).view(x0.size(0),1,-1)#.clone()
            
        one_frame_flat = one_frame.reshape(one_frame.size(0),1,-1)
        one_frame_flat_padded = torch.cat((self.buffer, one_frame_flat), dim=-1)
        out_frame_new = self.fc(one_frame_flat_padded)
        self.buffer[:,:,:self.frame_len] = self.buffer[:,:,self.frame_len:]#.clone()
        self.buffer[:,:,self.frame_len:] = one_frame_flat#.clone()
        return out_frame_new

    def forward(self, x_frame):
        
        return self.one_forward_pass(x_frame)#out 
    
    def reset(self):
        self.buffer = None
        return
    
class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = nn.Linear(in_ch, out_ch * upsample_factor, bias=False)
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
                
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
    
class FWGAN400ContLargeNewAR(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.bfcc_with_corr_upsampler = UpsampleFC(19,80,5) 
        
        self.feat_in_conv1 = ConvLookahead(144,256,kernel_size=9) 
        self.feat_in_nl1 = GLU(256)

        self.gru = nn.GRUCell(320, 256, bias=False)
        self.gru_glu = GLU(256)
        self.fwc1 = FWConv(256, 256)
        self.fwc2 = FWConv(256, 128)
        self.fwc3 = FWConv(128, 128)
        self.fwc4 = FWConv(128, 64)
        self.fwc5 = FWConv(64, 64)
        self.fwc6 = FWConv(64, 32)
        self.fwc7 = FWConv(32, 32)

        self.gain_dense_out = which_norm(nn.Linear(256, 1, bias=False))
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,32)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,32)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals

    def gain_multiply(self, x, c0):

        gain = 0.3*10**(0.5*c0/np.sqrt(18.0))
        gain = torch.repeat_interleave(gain, 160, dim=-1)
        gain  = gain.reshape(gain.size(0),1,-1).squeeze(1)

        return x * gain
    
    def forward(self, pitch_period, bfcc_with_corr, x0):
        batch_size = x0.size(0)
        device = x0.device
        p_embed = self.create_phase_signals(pitch_period).permute(0, 2, 1).contiguous()
        
        envelope = self.bfcc_with_corr_upsampler(bfcc_with_corr.permute(0,2,1).contiguous())
        
        feat_in = torch.cat((p_embed , envelope), dim=1)
        
        feat_latent = self.feat_in_nl1(self.feat_in_conv1(feat_in).permute(0,2,1).contiguous())
 
        out_waveform = x0
        period_repeat = torch.repeat_interleave(pitch_period, 5, dim=-1)

        fwc1_state = torch.zeros(batch_size, 1, 256*2, device=device)
        fwc2_state = torch.zeros(batch_size, 1, 256*2, device=device)
        fwc3_state = torch.zeros(batch_size, 1, 128*2, device=device)
        fwc4_state = torch.zeros(batch_size, 1, 128*2, device=device)
        fwc5_state = torch.zeros(batch_size, 1, 64*2, device=device)
        fwc6_state = torch.zeros(batch_size, 1, 64*2, device=device)
        fwc7_state = torch.zeros(batch_size, 1, 32*2, device=device)
        gru_state = torch.zeros(batch_size, 256, device=device)
        for i in range(feat_latent.size(1)):
            
            T = period_repeat[:, i:i+1]
            ind = out_waveform.size(-1) - T
            ind = ind + torch.arange(32, device=ind.device)
            pred_wav = torch.gather(out_waveform, dim=-1, index=ind).unsqueeze(1) 

            ar_wav = out_waveform[:,-32:].unsqueeze(1) 
        
            latent_frame = torch.cat((feat_latent[:,i:i+1,:], pred_wav, ar_wav), dim=-1) 
            gru_state = self.gru(latent_frame[:,0,:], gru_state)
            rnn_out = self.gru_glu(gru_state[:,None,:])

            fwc1_out, fwc1_state = self.fwc1(rnn_out, fwc1_state)
            fwc2_out, fwc2_state = self.fwc2(fwc1_out, fwc2_state)
            fwc3_out, fwc3_state = self.fwc3(fwc2_out, fwc3_state)
            fwc4_out, fwc4_state = self.fwc4(fwc3_out, fwc4_state)
            fwc5_out, fwc5_state = self.fwc5(fwc4_out, fwc5_state)    
            fwc6_out, fwc6_state = self.fwc6(fwc5_out, fwc6_state)
            fwc7_out, fwc7_state = self.fwc7(fwc6_out, fwc7_state)

            pitch_gain = torch.exp(self.gain_dense_out(rnn_out))
            out_frame = (pitch_gain * pred_wav).squeeze(1) + fwc7_out.squeeze(1)
        
            out_waveform = torch.cat((out_waveform, out_frame), dim=-1)

        return  out_waveform[:,320:]
