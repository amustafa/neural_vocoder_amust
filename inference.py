import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np
import argparse
import time
import soundfile as sf
from torchaudio.transforms import MelSpectrogram
from scipy.signal import kaiser

torch.cuda.init()
torch.manual_seed(5)


###################################### Define Generator Model Components ######################################
                     
which_norm = weight_norm
kernel_size = 9

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation,bias= False))
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.conv(x)
        return conv_out


class Upsample(nn.Module):
    
    def __init__(self,ch1,ch2, rate1=0, rate2=0):
        
        super().__init__()
        torch.manual_seed(5)
        self.rate1 = rate1
        self.rate2 = rate2
        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        
    def forward(self, x):
        
        x = self.nl(self.conv(F.interpolate(x, scale_factor= self.rate2 / self.rate1)))
        return x
        
class EnergyConv(nn.Module):
    
    def __init__(self,ch1,ch2):
        
        super().__init__()
        torch.manual_seed(5)

        self.nl = nn.LeakyReLU(0.2)
        self.conv = CausalConv(ch1,ch2,kernel_size=kernel_size,dilation=1)
      
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, energy):
        x = self.nl(self.conv(energy))
        
        return x
        
class TADE(nn.Module):
    
    def __init__(self,num_channels, k_siz, dilation_factor, learn_beta = True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.learn_beta = learn_beta
        self.mlp_shared = nn.Sequential(CausalConv(80,64,kernel_size=k_siz,dilation=dilation_factor),
                          nn.LeakyReLU(0.2))
       
        self.gamma_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
        if self.learn_beta:
            self.beta_conv = CausalConv(64,64,kernel_size=k_siz,dilation=1)
     
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, cond, scale_factor):
           
        cond_interp = F.interpolate(cond,scale_factor= scale_factor)
        actv = self.mlp_shared(cond_interp)

        gamma = self.gamma_conv(actv)
        if self.learn_beta:
            beta = self.beta_conv(actv)
        else:
            beta = None
        
        return gamma, beta 
        
class Modulation(nn.Module):
    
    def __init__(self,num_channels, apply_norm= True):
        
        super().__init__()
        torch.manual_seed(5)
        self.num_channels= num_channels
        self.apply_norm = apply_norm
        self.eps = 1e-05

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        

    def forward(self, x, gamma, beta):
         
        if self.apply_norm:

            mu, var = torch.mean(x, dim=1, keepdim=True), torch.var(x, dim=1, keepdim=True)
            x_norm = (x - mu) * torch.rsqrt(var + self.eps)
            
        else:
            x_norm = x
            
        if beta is not None:
            out = (x_norm * gamma) + beta
        else:
            out = (x_norm * gamma)
        return out

class TADEResBlock(nn.Module):
    
    def __init__(self,in_channels, out_channels, k_siz=kernel_size, learn_beta = True):
        
        super().__init__()
        torch.manual_seed(5)
        self.in_channels= in_channels
        self.out_channels= out_channels
        dil = 1
        
        self.tade = TADE(in_channels,k_siz,1, learn_beta)
        
        self.mod1 = Modulation(self.out_channels)
        self.conv1_tanh = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1)
        self.conv1_gate = CausalConv(self.out_channels,self.out_channels,kernel_size=k_siz,dilation=1)

        self.mod2 = Modulation(self.out_channels)
        self.conv2_tanh = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2)
        self.conv2_gate = CausalConv(self.in_channels,self.out_channels,kernel_size=k_siz,dilation=2) 
                                    
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
        
    def forward(self, x, cond):
        
        temp_res = x.shape[2]
        mel_res = cond.shape[2]
        scale_factor  = temp_res/mel_res
        
        gamma, beta = self.tade(cond, scale_factor)
        
        mod_out1 = self.mod1(x,gamma,beta)
        out1 = torch.tanh(self.conv1_tanh(mod_out1)) * F.softmax(self.conv1_gate(mod_out1),dim=1)
        
        mod_out2 = self.mod2(out1,gamma,beta) 
        out2 = torch.tanh(self.conv2_tanh(mod_out2)) * F.softmax(self.conv2_gate(mod_out2),dim=1)

        out = out2 + x
        return out

class SpadeGen(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.latent_dim = 64

        self.energy_conv = EnergyConv(1,self.latent_dim)
        
        self.act0 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler0 = Upsample(self.latent_dim,self.latent_dim,100,200)
        
        self.act1 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler1 = Upsample(self.latent_dim,self.latent_dim,200,500)

        self.act2 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler2 = Upsample(self.latent_dim,self.latent_dim,500,1000)

        self.act3 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler3 = Upsample(self.latent_dim,self.latent_dim,1000,2000)

        self.act4 = TADEResBlock(self.latent_dim,self.latent_dim)
        self.upsampler4 = Upsample(self.latent_dim,self.latent_dim,2000,4000)

        self.act5 = TADEResBlock(self.latent_dim,self.latent_dim)

        self.act6 = TADEResBlock(self.latent_dim,self.latent_dim)

        self.act7 = TADEResBlock(self.latent_dim,self.latent_dim)
   
        self.anti_alias = TADEResBlock(self.latent_dim,self.latent_dim)

        self.dec_conv = CausalConv(self.latent_dim,4,kernel_size=kernel_size,dilation=1)
        self.dec_tanh = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x, energy):
         
        latent = self.energy_conv(energy)
        latent  = self.upsampler0(self.act0(latent,x)) 
        stage125_250 = self.upsampler1(self.act1(latent, x))
        stage250_500 = self.upsampler2(self.act2(stage125_250 ,x))
        stage500_1000 = self.upsampler3(self.act3(stage250_500 ,x))
        stage1000_2000 = self.upsampler4(self.act4(stage500_1000 ,x))
        stage2000_4000 = self.act5(stage1000_2000 ,x)
        stage4000_8000 = self.act6(stage2000_4000 ,x)
        stage8000_16000 = self.act7(stage4000_8000 ,x)
        ant_alias = self.anti_alias(stage8000_16000 ,x)          
        d_conv= self.dec_conv(ant_alias)
        out = self.dec_tanh(d_conv)
   
        return out
        
class Generator(nn.Module):
    
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)
        
        self.mel_conv = CausalConv(80,80,kernel_size=kernel_size,dilation=1)
        
        self.spade_generator = SpadeGen()    

        self.pqmf = PQMF()
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
                              
    def forward(self, mel):

        energy = torch.sum(mel, 1, keepdim=True)
    
        x = self.spade_generator(self.mel_conv(mel), energy)   
        x = self.pqmf.synthesis(x)
        return x.squeeze(1)
        
############################################## Define PQMF Analysis/Synthesis Filters #################################################

def design_prototype_filter(taps=100, cutoff_ratio=0.13553663, beta=9.0):
    """
    ahd: I haved edited this according to the following issue:
    https://github.com/kan-bayashi/ParallelWaveGAN/issues/195
    This gives reconstruction with significantly lower error than the original implementation. 
    
    Design prototype filter for PQMF.
    This method is based on `A Kaiser window approach for the design of prototype
    filters of cosine modulated filterbanks`_.
    Args:
        taps (int): The number of filter taps.
        cutoff_ratio (float): Cut-off frequency ratio.
        beta (float): Beta coefficient for kaiser window.
    Returns:
        ndarray: Impluse response of prototype filter (taps + 1,).
    .. _`A Kaiser window approach for the design of prototype filters of cosine modulated filterbanks`:
        https://ieeexplore.ieee.org/abstract/document/681427
    """
    # check the arguments are valid
    assert taps % 2 == 0, "The number of taps mush be even number."
    assert 0.0 < cutoff_ratio < 1.0, "Cutoff ratio must be > 0.0 and < 1.0."

    # make initial filter
    omega_c = np.pi * cutoff_ratio
    with np.errstate(invalid='ignore'):
        h_i = np.sin(omega_c * (np.arange(taps + 1) - 0.5 * taps)) \
            / (np.pi * (np.arange(taps + 1) - 0.5 * taps))
    h_i[taps // 2] = np.cos(0) * cutoff_ratio  # fix nan due to indeterminate form

    # apply kaiser window
    w = kaiser(taps + 1, beta)
    h = h_i * w

    return h

class PQMF(torch.nn.Module):
    """
    ahd: I haved edited this according to the following issue:
    https://github.com/kan-bayashi/ParallelWaveGAN/issues/195
    This gives reconstruction with significantly lower error than the original implementation.
    
    PQMF module.
    This module is based on `Near-perfect-reconstruction pseudo-QMF banks`_.
    .. _`Near-perfect-reconstruction pseudo-QMF banks`:
        https://ieeexplore.ieee.org/document/258122
    """

    def __init__(self, subbands=4, taps=100, cutoff_ratio=0.13553663, beta=9.0):
        """Initilize PQMF module.
        Args:
            subbands (int): The number of subbands.
            taps (int): The number of filter taps.
            cutoff_ratio (float): Cut-off frequency ratio.
            beta (float): Beta coefficient for kaiser window.
        """
        super(PQMF, self).__init__()

        # build analysis & synthesis filter coefficients
        h_proto = design_prototype_filter(taps, cutoff_ratio, beta)
        self.h_proto = h_proto
        h_analysis = np.zeros((subbands, len(h_proto)))
        h_synthesis = np.zeros((subbands, len(h_proto)))
        for k in range(subbands):
            h_analysis[k] = 2 * h_proto * np.cos(
                (2 * k + 1) * (np.pi / (2 * subbands)) *
                (np.arange(taps + 1) - (taps / 2)) +
                (-1) ** k * np.pi / 4)
            h_synthesis[k] = 2 * h_proto * np.cos(
                (2 * k + 1) * (np.pi / (2 * subbands)) *
                (np.arange(taps + 1) - (taps / 2)) -
                (-1) ** k * np.pi / 4)

        # convert to tensor
        analysis_filter = torch.from_numpy(h_analysis).float().unsqueeze(1)
        synthesis_filter = torch.from_numpy(h_synthesis).float().unsqueeze(0)

        # register coefficients as beffer
        self.register_buffer("analysis_filter", analysis_filter)
        self.register_buffer("synthesis_filter", synthesis_filter)

        # filter for downsampling & upsampling
        updown_filter = torch.zeros((subbands, subbands, subbands)).float()
        for k in range(subbands):
            updown_filter[k, k, 0] = 1.0
        self.register_buffer("updown_filter", updown_filter)
        self.subbands = subbands
        self.odd_subbands = [i for i in range(self.subbands) if i % 2 != 0]

        # keep padding info
        self.pad_fn = torch.nn.ConstantPad1d(taps // 2, 0.0)

    def analysis(self, x):
        """Analysis with PQMF.
        Args:
            x (Tensor): Input tensor (B, 1, T).
        Returns:
            Tensor: Output tensor (B, subbands, T // subbands).
        """

        if self.subbands == 1:
            return x

        else:

            x = F.conv1d(self.pad_fn(x), self.analysis_filter)
            x = F.conv1d(x, self.updown_filter, stride=self.subbands)
            #invert mirrored subbands 
            #ind = torch.arange(0,x.shape[-1], device=x.device)
            #for i in self.odd_subbands:
            #    x[:,i,:] = x[:,i,:]*torch.pow(-1,ind)
            return x

    def synthesis(self, x):
        """Synthesis with PQMF.
        Args:
            x (Tensor): Input tensor (B, subbands, T // subbands).
        Returns:
            Tensor: Output tensor (B, 1, T).
        """
        # NOTE(kan-bayashi): Power will be dreased so here multipy by # subbands.
        #   Not sure this is the correct way, it is better to check again.
        # TODO(kan-bayashi): Understand the reconstruction procedure

        if self.subbands == 1:
            return x

        else:

            x = F.conv_transpose1d(x, self.updown_filter * self.subbands, stride=self.subbands)
            return F.conv1d(self.pad_fn(x), self.synthesis_filter) 

############################################## Define Some Pre/Post-processing Functions #################################################

class LogMel(nn.Module):
    def __init__(self, sample_rate= 16000, 
                       n_fft= 512,
                       win_length=320,
                       hop_length=160,
                       n_mels=80,
                       norm='slaney',
                       mel_scale='slaney'):
        
        super(LogMel, self).__init__()
        
        self.mel = MelSpectrogram(sample_rate=sample_rate,
                                  n_fft= n_fft,
                                  win_length= win_length,
                                  hop_length= hop_length,
                                  n_mels= n_mels,
                                  norm=norm,
                                  mel_scale=mel_scale)

    def forward(self, x):

        mel_feat = self.mel(x)
        log_mel = torch.log10(torch.clamp(mel_feat, min=1e-5))
        return log_mel


def _remove_weight_norm(m):
    try:
        torch.nn.utils.remove_weight_norm(m)
    except ValueError:  # this module didn't have weight norm
        return
    
def deemphasis(x, coef=0.85):
    out = np.zeros_like(x)
    out[0] = x[0]
    for i in range(1,len(x)):
        out[i] = out[i-1]*coef + x[i]
    return out


###################################### Start the Infrence Process ######################################

def init_parser():
    
    parser = argparse.ArgumentParser( usage='%(prog)s [OPTION]', description='')

    parser.add_argument( '--g_path',       type=str, default='./generator.pkl')
    parser.add_argument( '--inp_mel',      type=str)
    parser.add_argument( '--inp_wav',      type=str)
    parser.add_argument( '--out_wav_path', type=str, default='./out_wav.wav')

    return parser.parse_args()


if __name__ == "__main__":

    options = vars(init_parser())
    
    genenerator= Generator()
    saved_genenerator= torch.load(options['g_path'], map_location='cpu')
    genenerator.load_state_dict(saved_genenerator)
    genenerator.apply(_remove_weight_norm)
    genenerator = genenerator.eval()

    if options['inp_wav'] is not None:
        
        mel_feat_extractor = LogMel()
        wav_in , sr = sf.read(options['inp_wav'],dtype='float32')

        #pre-emphasis
        wav_in[1:] = wav_in[1:] - 0.85*wav_in[:-1]

        wav_in = torch.from_numpy(wav_in).unsqueeze(0)
        mel_feat = mel_feat_extractor(wav_in)

    if options['inp_mel'] is not None:
        
        sr = 16000
        mel_feat = np.load(options['inp_mel'])
        mel_feat = torch.from_numpy(mel_feat).unsqueeze(0) if len(mel_feat.shape)==2 else torch.from_numpy(mel_feat)
        
    #inference
    with torch.no_grad():  
        start_time = time.time()
        wav_out = genenerator(mel_feat)
        end_time = time.time() 
    wav_out = wav_out.squeeze(1).squeeze(0).detach().numpy()
    
    #de-emphasis
    wav_out = deemphasis(wav_out)     

    #save audio file
    sf.write(options['out_wav_path'], wav_out, sr)

    total_time = end_time - start_time
    gen_seconds = len(wav_out)/sr
    print(f"Took {total_time:.3f}s to generate {len(wav_out)}  samples ({gen_seconds}s) -> {gen_seconds/total_time:.2f}x real time")  

    print('Done!')